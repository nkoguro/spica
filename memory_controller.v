`timescale 1ns / 1ps

// 4M word SDRAM memory controller
module memory_controller(/*AUTOARG*/
  // Outputs
  exec_n, SD_A, SD_BA, SD_RAS, SD_CAS, SD_WE, SD_CS, SD_UDM, SD_LDM,
  // Inouts
  data, SD_DQ, SD_LDQS_P, SD_UDQS_P,
  // Inputs
  clk100, rst, cs_n, wr_n, rd_n, mem_addr, dqs_p
  );
  input clk100;
  input rst;
  input cs_n;
  input wr_n;
  input rd_n;
  output reg exec_n = 1;
  input [21:0] mem_addr;
  inout [31:0] data;
  
  input             dqs_p;
  output reg [15:0] SD_A = 0;
  inout [15:0]      SD_DQ;
  output reg [2:0]  SD_BA = 0;
  output reg        SD_RAS;
  output reg        SD_CAS;
  output reg        SD_WE;
  output            SD_CS;
  output            SD_UDM;
  output            SD_LDM;
  inout             SD_LDQS_P;
  inout             SD_UDQS_P;

  // バンクアドレスの計算
  function [2:0] addr2bank;
    input [21:0] addr;
    addr2bank = {1'b0, addr[21:20]};
  endfunction
  
  // ローの計算
  function [15:0] addr2row;
    input [21:0] addr;
    addr2row = {3'b0, addr[19:7]};
  endfunction
  
  // カラムの計算
  function [9:0] addr2column;
    input [21:0] addr;
    addr2column = {addr[6:0], 3'b0};
  endfunction

  reg [31:0] read_data;
  reg [15:0] dq;
  reg dq_out;
  
  // DDR2コマンド (RAS#, CAS#, WE#)
  parameter 
    DDR2_LOAD_MODE = 3'b000,
    DDR2_REFRESH   = 3'b001,
    DDR2_PRECHARGE = 3'b010,
    DDR2_ACTIVATE  = 3'b011,
    DDR2_WRITE     = 3'b100,
    DDR2_READ      = 3'b101,
    DDR2_NOP       = 3'b111;

  parameter
    // アイドル状態
    IDLE         = 5'h0,
      
    // 初期化シーケンスで取りうる状態
    INIT_PRE     = 5'h01,
    INIT_LM5     = 5'h02,
    INIT_LM6     = 5'h03,
    INIT_LM7     = 5'h04,
    INIT_LM8     = 5'h05,
    INIT_PRE9    = 5'h06,
    INIT_REF10_1 = 5'h07,
    INIT_REF10_2 = 5'h08,
    INIT_LM11    = 5'h09,
    INIT_LM12    = 5'h0A,
    INIT_LM13    = 5'h0B,

    // データ書き込みシーケンスで取りうる状態
    WRITE_DATA_L = 5'h0C,
    WRITE_H      = 5'h0D,
    WRITE_DATA_H = 5'h0E,
    WRITE_WAIT   = 5'h0F,

    // データ読み込みシーケンスで取りうる状態
    READ_H       = 5'h10,
    READ_DATA_L  = 5'h11,
    READ_DATA_H  = 5'h12,

    // common status of read and write
    WAIT_IDLE    = 5'h13;
  
  reg [4:0]         status = INIT_PRE;

  // リフレッシュ指示
  wire refresh;
  reg refresh_clear = 0;
  refresh_counter refcnt(.clk100(clk100), .refresh(refresh), .refresh_clear(refresh_clear)); 

  // Initialization
  parameter INIT_WAIT = 15'd20040;  // 200,000ns + 400ns
  
  // 待ち時間を取るために使用するカウンタ
  reg [14:0]         wait_cnt = INIT_WAIT;
  
  // 現在有効なバンクとロー (最上位ビットが立っている場合は、有効なものがない状態)
  parameter NO_ACTIVE = 19'h7FFFF;
  reg [18:0] current_active = NO_ACTIVE;
  
`define DDR2_CMD {SD_RAS, SD_CAS, SD_WE}

  always @(posedge clk100) begin
    if (rst) begin
      status         <= INIT_PRE;
      exec_n         <= 1;
      refresh_clear  <= 0;
      wait_cnt       <= 15'd20040;  // 200,000ns + 400ns
      current_active <= NO_ACTIVE;
      dq_out         <= 0;
    end
    else if (wait_cnt != 0) begin
      // waitのあいだはNOPを送り続ける
      `DDR2_CMD     <= DDR2_NOP;
      refresh_clear <= 0;
      wait_cnt      <= wait_cnt-1;
    end
    else begin
      case (status)
        IDLE: begin
          if (refresh) begin
            if (current_active[15:15]) begin
              // REFRESH
              `DDR2_CMD     <= DDR2_REFRESH;

              refresh_clear <= 1;
              wait_cnt      <= 10; // tRFC = 105ns
            end
            else begin
              // All banks PRECHARGE
              `DDR2_CMD      <= DDR2_PRECHARGE;
              SD_A[10]       <= 1;

              current_active <= NO_ACTIVE;
              wait_cnt       <= 1;  // tRPA = 15ns
            end
          end
          else if (!cs_n && (!rd_n || !wr_n)) begin
            if (current_active[15:15]) begin
              // ACTIVATE
              `DDR2_CMD      <= DDR2_ACTIVATE;
              SD_BA          <= addr2bank(mem_addr);
              SD_A           <= addr2row(mem_addr);

              current_active <= {addr2bank(mem_addr), addr2row(mem_addr)};
              wait_cnt       <= 1;  // tRCD = 15ns (2clk - 1)
            end
            else if (current_active != {addr2bank(mem_addr), addr2row(mem_addr)}) begin
              // All banks PRECHARGE
              `DDR2_CMD      <= DDR2_PRECHARGE;
              SD_A[10]       <= 1;

              current_active <= NO_ACTIVE;
              wait_cnt       <= 1;  // tRPA = 15ns
            end
            else begin
              exec_n <= 0;
              if (!rd_n) begin
                // READ without auto precharge
                `DDR2_CMD  <= DDR2_READ;
                SD_BA      <= addr2bank(mem_addr);
                SD_A[10:0] <= {1'b0, addr2column(mem_addr)};

                status     <= READ_H;
                wait_cnt   <= 1;
              end
              else begin
                // WRITE without auto precharge
                `DDR2_CMD  <= DDR2_WRITE;
                SD_BA      <= addr2bank(mem_addr);
                SD_A[10:0] <= {1'b0, addr2column(mem_addr)};

                status     <= WRITE_H;
                wait_cnt   <= 2;
              end
            end
          end
        end
        
        // 初期化シーケンス
        INIT_PRE: begin // All banks PRECHARGE
          `DDR2_CMD <= DDR2_PRECHARGE;
          SD_A[10]  <= 1;

          status    <= INIT_LM5;
          wait_cnt  <= 1;  // tRP = 15ns
        end

        INIT_LM5: begin // LOAD MODE (EMR2)
          `DDR2_CMD <= DDR2_LOAD_MODE;
          SD_BA     <= 3'b010;  // EMR2
          SD_A      <= 16'b0000000000000000; // SRT=0 (1X refresh rate)

          status    <= INIT_LM6;
          wait_cnt  <= 1;  // tMRD = 2clk (wait_cnt=0 だと次のクロックで実行されるので、待ち時間 1clkとなる)
        end
        
        INIT_LM6: begin // LOAD MODE (EMR3)
          `DDR2_CMD <= DDR2_LOAD_MODE;
          SD_BA     <= 3'b011; // EMR3
          SD_A      <= 16'b0000000000000000;

          status    <= INIT_LM7;
          wait_cnt  <= 1;  // tMRD = 2clk
        end
        
        INIT_LM7: begin // LOAD MODE (EMR)
          `DDR2_CMD <= DDR2_LOAD_MODE;
          SD_BA     <= 3'b001; // EMR
          SD_A      <= 16'b0000000000000000;

          status    <= INIT_LM8;
          wait_cnt  <= 1;  // tMRD = 2clk
        end
        
        INIT_LM8: begin // LOAD MODE (MR with DLL RESET)
          `DDR2_CMD <= DDR2_LOAD_MODE;
          SD_BA     <= 3'b000; // MR
          SD_A      <= 16'b0000000100000000;

          status    <= INIT_PRE9;
          wait_cnt  <= 1;  // tMRD = 2clk
        end
        
        INIT_PRE9: begin // All banks PRECHARGE
          `DDR2_CMD <= DDR2_PRECHARGE;
          SD_A[10]  <= 1;

          status    <= INIT_REF10_1;
          wait_cnt  <= 1;  // tRPA = 15ns
        end
        
        INIT_REF10_1: begin // REFRESH
          `DDR2_CMD <= DDR2_REFRESH;

          status    <= INIT_REF10_2;
          wait_cnt  <= 10; // tRFC = 105ns
        end
        
        INIT_REF10_2: begin // REFRESH
          `DDR2_CMD <= DDR2_REFRESH;

          status    <= INIT_LM11;
          wait_cnt  <= 10; // tRFC = 105ns
        end
        
        INIT_LM11: begin // LOAD MODE (MR without DLL RESET)
          `DDR2_CMD <= DDR2_LOAD_MODE;
          SD_BA     <= 3'b000; // MR
          SD_A      <= 16'b0000001001010010;
          // Burst Length = 4
          // Burst Type = Sequential
          // CAS Latency (CL) = 5
          // Operating Mode (TM) = Normal
          // DLL Reset = No
          // WR = 2 (tWR = 15ns / tCK = 10ns (100MHz))
          // Power-Down Mode = Fast exit (normal)

          status    <= INIT_LM12;
          wait_cnt  <= 1;  // tMRD = 2clk
        end
        
        INIT_LM12: begin // LOAD MODE (EMR with OCD default)
          `DDR2_CMD <= DDR2_LOAD_MODE;
          SD_BA     <= 3'b001; // EMR
          SD_A      <= 16'b0000011110000000;
          // DLL Enable = enable
          // Output Drive Strength = Full
          // On-Die Termination = disable
          // Posted CAS# Additive Latency = 0
          // OCD Operation = Enable OCD defaults
          // DQS# Enable = disable
          // RDQS Enable = disable
          // Output = enable

          status    <= INIT_LM13;
          wait_cnt  <= 1;  // tMRD = 2clk
        end
        
        INIT_LM13: begin // LOAD MODE (EMR with OCD exit)
          `DDR2_CMD <= DDR2_LOAD_MODE;
          SD_BA     <= 3'b001; // EMR
          SD_A      <= 16'b0000010000000000;
          // DLL Enable = enable
          // Output Drive Strength = Full
          // On-Die Termination = disable
          // Posted CAS# Additive Latency = 0
          // OCD Operation = OCD exit
          // DQS# Enable = disable
          // RDQS Enable = disable
          // Output = enable

          status    <= IDLE;
          wait_cnt  <= 169;  // 200 cycles of CK are required before a READ command can be issued (INIT_LM8から数えて200cycles)
        end
        
        // 読み込みシーケンス
        READ_H: begin   // READ without auto precharge
          `DDR2_CMD  <= DDR2_READ;
          SD_A[10:0] <= SD_A[10:0] | 3'b100;

          status     <= READ_DATA_L;
          wait_cnt   <= 4; 
        end

        READ_DATA_L: begin   // Read low data
          read_data[15:0] <= SD_DQ;
          
          `DDR2_CMD       <= DDR2_NOP;

          status          <= READ_DATA_H;
          wait_cnt        <= 1; 
        end
        
        READ_DATA_H: begin // Read high data
          read_data[31:16] <= SD_DQ;

          `DDR2_CMD        <= DDR2_NOP;

          status           <= (rd_n & wr_n) ? IDLE : WAIT_IDLE;
          exec_n           <= 1;
          wait_cnt         <= 0;
        end
        
        // 書き込みシーケンス
        WRITE_H: begin   // WRITE without auto precharge
          `DDR2_CMD  <= DDR2_WRITE;
          SD_A[10:0] <= SD_A[10:0] | 3'b100;

          status     <= WRITE_DATA_L;
          wait_cnt   <= 0;
        end
        WRITE_DATA_L: begin
          `DDR2_CMD <= DDR2_NOP;
          
          status    <= WRITE_DATA_H;
          dq_out    <= 1;
          dq        <= data[15:0];
          wait_cnt  <= 2;
        end
        WRITE_DATA_H: begin
          `DDR2_CMD <= DDR2_NOP;

          status    <= WRITE_WAIT;
          dq        <= data[31:16];
          exec_n      <= 1;
          wait_cnt  <= 2;
        end
        WRITE_WAIT: begin      // wait write recovery time
          `DDR2_CMD <= DDR2_NOP;

          status    <= (rd_n & wr_n) ? IDLE : WAIT_IDLE;
          dq_out    <= 0;
          wait_cnt  <= 1; // tWR = 15 ns
        end

        WAIT_IDLE: begin
          `DDR2_CMD <= DDR2_NOP;

          status    <= (rd_n & wr_n) ? IDLE : WAIT_IDLE;
        end
      endcase
    end
  end
  
  assign SD_CS = 0;
  assign SD_DQ = dq_out ? dq : 16'bz;
  assign SD_LDQS_P = dq_out ? dqs_p : 1'bz;
  assign SD_UDQS_P = dq_out ? dqs_p : 1'bz;
  
  assign SD_UDM = 1'b0;
  assign SD_LDM = 1'b0;

  assign data = (!cs_n && !rd_n) ? read_data : 32'bz;
endmodule

module refresh_counter(
                       input      clk100,
                       output reg refresh = 0,
                       input      refresh_clear
                       );

  parameter REFRESH_CYCLE = 10'd700;  // clocks (= 7000 ns)
  reg [9:0] refresh_cnt = REFRESH_CYCLE;
  
  always @(posedge clk100) begin
    if (refresh_clear) begin
      refresh_cnt <= REFRESH_CYCLE;
      refresh <= 0;
    end
    else begin
      if (refresh_cnt == 9'h0) begin
        refresh <= 1;
      end
      else begin
        refresh_cnt <= refresh_cnt - 1;
        refresh <= 0;
      end
    end
  end

endmodule
