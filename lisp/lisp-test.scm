;; -*- coding: utf-8; mode: scheme -*-
;;
;; lisp-test.scm - 
;;
;;   Copyright (c) 2011 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(use gauche.test)

(test-record-file (list-ref *argv* 0))
(test-start "compiler")

(load "spica-lisp.scm")

(test "compile-lookup: lookup global."
    '(global foo)
  (lambda ()
    (compile-lookup 'foo (top-env)
                    (lambda (x)
                      `(local ,x))
                    (lambda (x)
                      `(free ,x))
                    (lambda (x)
                      `(global ,x)))))

(test "compile-lookup: lookup free."
    '(free 1)
  (lambda ()
    (compile-lookup 'quux (make-env '(foo bar) '(baz quux))
                    (lambda (x)
                      `(local ,x))
                    (lambda (x)
                      `(free ,x))
                    (lambda (x)
                      `(global ,x)))))

(test "compile-lookup: lookup local."
    '(local 0)
  (lambda ()
    (compile-lookup 'foo (make-env '(foo bar) '(baz quux))
                    (lambda (x)
                      `(local ,x))
                    (lambda (x)
                      `(free ,x))
                    (lambda (x)
                      `(global ,x)))))

(test "make-boxes: sets found."
    (bytecodes-pack! ($begin! ($asm 'box 0)
                              ($asm 'box 2)
                              ($asm 'halt)))
  (lambda ()
    (bytecodes-pack! (make-boxes '(a c) '(a b c d) ($begin! ($asm 'halt))))))

(test "make-boxes: no sets found."
    (bytecodes-pack! ($begin! ($asm 'halt)))
  (lambda ()
    (bytecodes-pack! (make-boxes '() '(a b c d) ($begin! ($asm 'halt))))))

(test "set-cons: add new var."
    '(foo bar baz)
  (lambda ()
    (set-cons 'foo '(bar baz))))

(test "set-cons: var is already exists."
    '(foo bar baz)
  (lambda ()
    (set-cons 'foo '(foo bar baz))))

(test "set-union"
    '(foo bar baz)
  (lambda ()
    (set-union '(foo) '(bar baz))))

(test "set-minus"
    '(foo baz)
  (lambda ()
    (set-minus '(foo bar baz) '(bar))))

(test "set-intersect"
    '(foo baz)
  (lambda ()
    (set-intersect '(foo bar baz) '(baz quux foo quuux))))

(test "collect-free"
    (bytecodes-pack!
     ($begin! ($asm 'refer-local 1)
              ($asm 'argument)
              ($asm 'refer-free 0)
              ($asm 'argument)
              ($asm 'halt)))
  (lambda ()
    (bytecodes-pack!
     (collect-free '(bar foo)
                   (make-env '(quux foo) '(bar baz))
                   ($begin! ($asm 'halt))))))

(test "compile-refer: refer-local"
    ($begin! ($asm 'refer-local 0)
             ($asm 'halt))
  (lambda ()
    (compile-refer 'foo (make-env '(foo bar) '(baz quux)) ($asm 'halt))))

(test "compile-refer: refer-free"
    ($begin! ($asm 'refer-free 1)
             ($asm 'halt))
  (lambda ()
    (compile-refer 'quux (make-env '(foo bar) '(baz quux)) ($asm 'halt))))

(test "compile-refer: refer-global"
    ($begin! ($asm 'refer-global 'abc)
             ($asm 'halt))
  (lambda ()
    (compile-refer 'abc (make-env '(foo bar) '(baz quux)) ($asm 'halt))))

(test "compile-arguments: no argument"
    ($begin! ($asm 'halt))
  (lambda ()
    (compile-arguments '() (make-env '(foo bar) '(baz quux)) '() ($asm 'halt))))

(test "compile-arguments: one argument"
    ($begin! ($asm 'constant 1)
             ($asm 'halt))
  (lambda ()
    (compile-arguments '(1) (make-env '(foo bar) '(baz quux)) '() ($asm 'halt))))

(test "compile-arguments: two arguments"
    ($begin! ($asm 'constant 1)
             ($asm 'argument)
             ($asm 'refer-local 1)
             ($asm 'halt))
  (lambda ()
    (compile-arguments '(1 bar) (make-env '(foo bar) '(baz quux)) '() ($asm 'halt))))

;;
(define (bytecodes-equal bc0 bc1)
  (equal? bc0 bc1))

(test "bytecodes-pack!: already packed"
    (let ((bc (make-bytecodes)))
        (bytecodes-set-packed! bc #t)
        (bytecodes-set-reverse-code-list! bc '(1 2 3 4))
        (bytecodes-set-previous! bc #f)
        bc)
  (lambda ()
    (let ((bc (make-bytecodes)))
      (bytecodes-set-packed! bc #t)
      (bytecodes-set-reverse-code-list! bc '(1 2 3 4))
      (bytecodes-set-previous! bc #f)      
      (bytecodes-pack! bc)))
  bytecodes-equal)

(test "bytecodes-pack!: empty bytecodes"
    (let ((bc (make-bytecodes)))
        (bytecodes-set-packed! bc #t)
        (bytecodes-set-reverse-code-list! bc '())
        (bytecodes-set-previous! bc #f)
        bc)
  (lambda ()
    (let ((bc (make-bytecodes)))
      (bytecodes-set-packed! bc #f)
      (bytecodes-set-reverse-code-list! bc '())
      (bytecodes-set-previous! bc #f)      
      (bytecodes-pack! bc))))

(test "bytecodes-pack!: pack 4 instructions"
    (let ((bc (make-bytecodes)))
        (bytecodes-set-packed! bc #t)
        (bytecodes-set-reverse-code-list! bc '(arg4-1 arg4-0 arg3 arg1 1060993))
        (bytecodes-set-previous! bc #f)
        bc)
  (lambda ()
    (let ((bc (make-bytecodes)))
      (bytecodes-set-packed! bc #f)
      (bytecodes-set-reverse-code-list! bc '((4 arg4-0 arg4-1) (3 arg3) (2) (1 arg1)))
      (bytecodes-set-previous! bc #f)      
      (bytecodes-pack! bc))))

(test "bytecodes-pack!: pack 5 instructions"
    (let ((bc (make-bytecodes)))
        (bytecodes-set-packed! bc #t)
        (bytecodes-set-reverse-code-list! bc '(arg5 arg4-1 arg4-0 arg3 arg1 84947073))
        (bytecodes-set-previous! bc #f)
        bc)
  (lambda ()
    (let ((bc (make-bytecodes)))
      (bytecodes-set-packed! bc #f)
      (bytecodes-set-reverse-code-list! bc '((5 arg5) (4 arg4-0 arg4-1) (3 arg3) (2) (1 arg1)))
      (bytecodes-set-previous! bc #f)      
      (bytecodes-pack! bc))))

(test "bytecodes-pack!: pack 6 instructions"
    (let ((bc (make-bytecodes)))
        (bytecodes-set-packed! bc #t)
        (bytecodes-set-reverse-code-list! bc '(arg6 6 arg5 arg4-1 arg4-0 arg3 arg1 84947073))
        (bytecodes-set-previous! bc #f)
        bc)
  (lambda ()
    (let ((bc (make-bytecodes)))
      (bytecodes-set-packed! bc #f)
      (bytecodes-set-reverse-code-list! bc '((6 arg6) (5 arg5) (4 arg4-0 arg4-1) (3 arg3) (2) (1 arg1)))
      (bytecodes-set-previous! bc #f)      
      (bytecodes-pack! bc))))

(test "$begin!: no list"
    (make-bytecodes)
  (lambda ()
    ($begin!)))

(test "$begin!: only one element"
    (make-bytecodes #t '(arg4-1 arg4-0 arg3 arg1 1060993) #f)
  (lambda ()
    ($begin! (make-bytecodes #t '(arg4-1 arg4-0 arg3 arg1 1060993) #f))))

(test "$begin!: combine"
    (make-bytecodes #f '((1) (7 foo)) #f)
  (lambda ()
    ($begin! ($asm 'constant 'foo)
             ($asm 'halt))))

(test "$begin!: combine with previous"
    (let ((prev (make-bytecodes #t '(arg4-1 arg4-0 arg3 arg1 1060993) #f)))
      (make-bytecodes #f '((1) (7 foo)) prev))
  (lambda ()
    (let ((prev (make-bytecodes #t '(arg4-1 arg4-0 arg3 arg1 1060993) #f)))
      ($begin! prev
               ($asm 'constant 'foo)
               ($asm 'halt)))))

(test "$begin!: combine with previous2"
    (make-bytecodes #f '((1))
                    (make-bytecodes #t '(arg4-1 arg4-0 arg3 arg1 1060993)
                                    (make-bytecodes #t '(foo 7) #f)))
  (lambda ()
    (let ((prev (make-bytecodes #t '(arg4-1 arg4-0 arg3 arg1 1060993) #f)))
      ($begin! ($asm 'constant 'foo)
               prev
               ($asm 'halt)))))

(test "bytecodes-serialize"
    #(1060993 arg1 arg3 arg4-0 arg4-1 71 foo)
  (lambda ()
    (bytecodes-serialize ($begin!
                          (make-bytecodes #t '(arg4-1 arg4-0 arg3 arg1 1060993) #f)
                          ($asm 'constant 'foo)
                          ($asm 'halt)))))

(test "bytecodes-packed-length"
    7
  (lambda ()
    (bytecodes-packed-length! ($begin!
                               (make-bytecodes #t '(arg4-1 arg4-0 arg3 arg1 1060993) #f)
                               ($asm 'constant 'foo)
                               ($asm 'halt)))))

;;;
(test "compile: quote"
    ($begin! ($asm 'constant 'foo)
             ($asm 'halt))
  (lambda ()
    (compile '(quote foo) (make-env '() '()) '() ($asm 'halt))))

(test "compile: lambda (no args, no free, no sets)"
    ($begin! ($asm 'close 0
                   ($begin! ($asm 'check-args 0)
                            ($asm 'constant #t)
                            ($asm 'return 0)))
             ($asm 'halt))
  (lambda ()
    (compile '(lambda () #t) (make-env '() '()) '() ($asm 'halt))))

(test "compile: lambda (1 arg, no free, no sets)"
    ($begin! ($asm 'close 0
                   ($begin! ($asm 'check-args 1)
                            ($asm 'refer-local 0)
                            ($asm 'return 1)))
             ($asm 'halt))
  (lambda ()
    (compile '(lambda (a) a) (make-env '(a) '()) '() ($asm 'halt))))

(test "compile: lambda (1 arg, free exists, no sets)"
    ($begin! ($asm 'refer-local 1)
             ($asm 'argument)
             ($asm 'close 1
                   ($begin! ($asm 'check-args 1)
                            ($asm 'refer-free 0)
                            ($asm 'return 1)))
             ($asm 'halt))
  (lambda ()
    (compile '(lambda (a) b) (make-env '(a b) '()) '() ($asm 'halt))))

(test "compile: lambda (1 arg, free exists, local variable is modified.)"
    ($begin! ($asm 'refer-local 1)
             ($asm 'argument)
             ($asm 'close 1
                   ($begin! ($asm 'check-args 1)
                            ($asm 'box 0)
                            ($asm 'refer-free 0)
                            ($asm 'assign-local 0)
                            ($asm 'return 1)))
             ($asm 'halt))
  (lambda ()
    (compile '(lambda (a) (set! a b)) (make-env '(a b) '()) '() ($asm 'halt))))

(test "compile: lambda (1 arg, free exists, free variable is modified.)"
    ($begin! ($asm 'refer-local 1)
             ($asm 'argument)
             ($asm 'close 1
                   ($begin! ($asm 'check-args 1)
                            ($asm 'refer-local 0)
                            ($asm 'assign-free 0)
                            ($asm 'return 1)))
             ($asm 'halt))
  (lambda ()
    (compile '(lambda (a) (set! b a)) (make-env '(a b) '()) '() ($asm 'halt))))

(test "compile: lambda (1 arg, no free, global variable is modified.)"
    ($begin! ($asm 'close 0
                   ($begin! ($asm 'check-args 1)
                            ($asm 'refer-local 0)
                            ($asm 'assign-global 'foo)
                            ($asm 'return 1)))
             ($asm 'halt))
  (lambda ()
    (compile '(lambda (a) (set! foo a)) (make-env '(a b) '()) '() ($asm 'halt))))

(test "compile: lambda (variable args)"
    ($begin! ($asm 'close 0
                   ($begin! ($asm 'check-args -2)
                            ($asm 'list 1)
                            ($asm 'adjust-vaargs -2)
                            ($asm 'refer-local 1)
                            ($asm 'assign-global 'foo)
                            ($asm 'return 2)))
             ($asm 'halt))
  (lambda ()
    (compile '(lambda (a . b) (set! foo b)) (make-env '(a b) '()) '() ($asm 'halt))))

(test-end)

;; end of file
