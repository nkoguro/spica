;; -*- coding: utf-8; mode: scheme -*-
;;
;; spica-lisp.scm - Spica Lisp
;;
;;   Copyright (c) 2010 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(use srfi-1)
(use gauche.uvector)
(use srfi-11)
(use gauche.parseopt)

(define (destructure-expander pat v body next)
  (if (null? pat)
      `(if (null? ,v)
           ,body
           ,next)
      (if (pair? pat)
          (let ((t (gensym)))
            `(let ((,t ,v))
               (if (pair? ,t)
                   ,(destructure-expander (car pat) `(car ,t)
                                          (destructure-expander (cdr pat) `(cdr ,t) body next)
                                          next)
                   ,next)))
          `(let ((,pat ,v))
             ,body))))

;; compiler
(define (make-bytecodes . args)
  (let-optionals* args ((packed? #f)
                        (reversed-code-list '())
                        (previous #f)
                        (return-arg #f))
    (let ((bc (make-vector 4)))
      ;; packed?
      (vector-set! bc 0 packed?)
      ;; reversed code-list
      (vector-set! bc 1 reversed-code-list)
      ;; previous bytecodes
      (vector-set! bc 2 previous)
      ;; return arg value. if the last code isn't return, set #f.
      (vector-set! bc 3 return-arg)
      bc)))

(define (bytecodes-packed? bc)
  (vector-ref bc 0))

(define (bytecodes-set-packed! bc packed?)
  (vector-set! bc 0 packed?))

(define (bytecodes-reverse-code-list bc)
  (vector-ref bc 1))

(define (bytecodes-set-reverse-code-list! bc code-list)
  (vector-set! bc 1 code-list))

(define (bytecodes-previous bc)
  (vector-ref bc 2))

(define (bytecodes-set-previous! bc prev)
  (vector-set! bc 2 prev))

(define (bytecodes-return-arg bc)
  (vector-ref bc 3))

(define (bytecodes-reverse-code-length bc)
  (length (bytecodes-reverse-code-list bc)))

(define (bytecodes-pack! bc)
  (cond
    ((bytecodes-packed? bc)
     bc)
    ((= (bytecodes-reverse-code-length bc) 0)
     (bytecodes-set-packed! bc #t)
     bc)
    (else
     (call-with-values
         (lambda ()
           (let traverse ((rev-unpacked-list (bytecodes-reverse-code-list bc)))
             (if (null? rev-unpacked-list)
                 (let ((op-code (list 0)))
                   (values op-code op-code))
                 (call-with-values (lambda ()
                                     (traverse (cdr rev-unpacked-list)))
                   (lambda (op-code rev-packed-list)
                     (let ((pack-code
                            (lambda (s)
                              (set-car! op-code
                                        (logior (car op-code)
                                                (ash (caar rev-unpacked-list) s)))
                              (values op-code
                                      (append (reverse (cdar rev-unpacked-list))
                                              rev-packed-list)))))
                       (cond
                         ((= (car op-code) 0)
                          (pack-code 0))
                         ((< (car op-code) 64)
                          (pack-code 6))
                         ((< (car op-code) 4096)
                          (pack-code 12))
                         ((< (car op-code) 262144)
                          (pack-code 18))
                         ((< (car op-code) 16777216)
                          (pack-code 24))
                         (else
                          (let ((op-code (cons (caar rev-unpacked-list)
                                               rev-packed-list)))
                            (values op-code
                                    (append (reverse (cdar rev-unpacked-list))
                                            op-code)))))))))))
       (lambda (op-code rev-packed-list)
         (bytecodes-set-packed! bc #t)
         (bytecodes-set-reverse-code-list! bc rev-packed-list)
         bc)))))

(define (bytecodes-serialize bc)
  (call-with-values
      (lambda ()
        (let traverse ((len 0)
                       (bc bc))
          (if bc
              (let ((bc-len (bytecodes-reverse-code-length (bytecodes-pack! bc))))
                (call-with-values (lambda ()
                                    (traverse (+ len bc-len)
                                              (bytecodes-previous bc)))
                  (lambda (vec i)
                    (let loop ((rev-list (bytecodes-reverse-code-list bc))
                               (j (- bc-len 1)))
                      (if (null? rev-list)
                          (values vec (+ i bc-len))
                          (begin
                            (vector-set! vec (+ i j) (car rev-list))
                            (loop (cdr rev-list) (- j 1))))))))
              (values (make-vector len) 0))))
    (lambda (vec i)
      vec)))

(define (bytecodes-packed-length! bc)
  (let loop ((len 0)
             (bc bc))
    (if bc
        (loop (+ len (bytecodes-reverse-code-length (bytecodes-pack! bc)))
              (bytecodes-previous bc))
        len)))
    
(define ($begin! . bc-list)
  (cond
    ((null? bc-list)
     (make-bytecodes))
    ((null? (cdr bc-list))
     (car bc-list))
    (else
     (apply $begin! (let ((bc0 (car bc-list))
                          (bc1 (cadr bc-list)))
                      (cond
                        ((not (or (bytecodes-packed? bc0)
                                  (bytecodes-packed? bc1)
                                  (bytecodes-previous bc1)))
                         (bytecodes-set-reverse-code-list!
                          bc0
                          (append (bytecodes-reverse-code-list bc1)
                                  (bytecodes-reverse-code-list bc0)))
                         bc0)
                        (else
                         (bytecodes-pack! bc0)
                         (let loop ((bc bc1))
                           (let ((prev (bytecodes-previous bc)))
                             (if prev
                                 (loop prev)
                                 (begin
                                   (bytecodes-set-previous! bc bc0)
                                   bc)))))))
            (cddr bc-list)))))

(define ($asm mnemonic . args)
  (let ((code (mnemonic->code mnemonic)))
    (make-bytecodes #f (list (cons code args)) #f (if (eq? mnemonic 'return)
                                                      (car args)
                                                      #f))))

(define (tail? next-bc)
  (bytecodes-return-arg next-bc))

;;
(define (top-env)
  '())

(define (make-env bounds frees)
  (cons bounds frees))

(define (env-bounds env)
  (car env))

(define (env-frees env)
  (cdr env))

(define (env-top? env)
  (null? env))

(define (env-bound? env var)
  (if (null? env)
      #f
      (memq var (env-bounds env))))

(define (env-free? env var)
  (if (null? env)
      #f
      (memq var (env-frees env))))

;;
(define (compile-lookup var env return-local return-free return-global)
  (if (env-top? env)
      (return-global var)
      (let nxtlocal ((locals (env-bounds env))
                     (n 0))
        (if (null? locals)
            (let nxtfree ((free (env-frees env))
                          (n 0))
              (cond
               ((null? free)
                (return-global var))
               ((eq? (car free) var)
                (return-free n))
               (else
                (nxtfree (cdr free) (+ n 1)))))
            (if (eq? (car locals) var)
                (return-local n)
                (nxtlocal (cdr locals) (+ n 1)))))))

(define (make-boxes sets vars next-bc)
  (let loop ((vars vars)
             (n 0))
    (if (null? vars)
        next-bc
        (if (memq (car vars) sets)
            ($begin! ($asm 'box n)
                     (loop (cdr vars) (+ n 1)))
            (loop (cdr vars) (+ n 1))))))

(define (set-cons var s)
  (if (memq var s)
      s
      (cons var s)))

(define (set-union s1 s2)
  (if (null? s1)
      s2
      (set-union (cdr s1) (set-cons (car s1) s2))))

(define (set-minus s1 s2)
  (if (null? s1)
      '()
      (if (memq (car s1) s2)
          (set-minus (cdr s1) s2)
          (cons (car s1) (set-minus (cdr s1) s2)))))

(define (set-intersect s1 s2)
  (if (null? s1)
      '()
      (if (memq (car s1) s2)
          (cons (car s1) (set-intersect (cdr s1) s2))
          (set-intersect (cdr s1) s2))))

;; Push values into stack for close instruction.
;; close will copy these values into procedure object (display closure).
(define (collect-free vars env next-bc)
  (if (null? vars)
      next-bc
      (if (env-top? env)
          (error "[bug] collect-free can't work in toplevel.")
          (collect-free (cdr vars) env
                        (compile-refer (car vars) env
                                       ($begin! ($asm 'argument) next-bc))))))

;; TODO: We have to add module support.
(define (compile-refer var env next-bc)
  (compile-lookup var env
                  (lambda (n)
                    ($begin! ($asm 'refer-local n) next-bc))
                  (lambda (n)
                    ($begin! ($asm 'refer-free n) next-bc))
                  (lambda (sym)
                    ($begin! ($asm 'refer-global sym) next-bc))))

(define (compile-arguments args env sets next-bc)
  (let loop ((args (reverse args))
             (first? #t)
             (bc next-bc))
    (if (null? args)
        bc
        (loop (cdr args) #f
              (compile (car args) env sets
                       (if first?
                           bc
                           ($begin! ($asm 'argument) bc)))))))

(define *pass1-procedure*
  (lambda (expr bounds)
    (let loop ((expr expr)
               (eexpr '())
               (frees '())
               (sets '()))
      (if (null? expr)
          (values (reverse eexpr) frees sets)
          (call-with-values (lambda ()
                              (compile/pass1 (car expr) bounds))
            (lambda (x f s)
              (loop (cdr expr) (cons x eexpr) (set-union f frees) (set-union s sets))))))))

(define *pass2-procedure*
  (lambda (x e s next)
    (let loop ((args (cdr x))
               (c ($begin!)))
      (if (null? args)
          (if (tail? next)
              ($begin! ($asm 'reset-$ap)
                       c
                       (compile (car x) e s ($begin! ($asm 'shift (code->arg (compiled-code-first next)))
                                                     ($asm 'apply))))
              (let ((cc ($begin! c
                                 (compile (car x) e s ($asm 'apply)))))
                ($begin! ($asm 'frame (bytecode-packed-length! cc))
                         cc
                         next)))
          (loop (cdr args)
                ($begin! c
                         (compile (car args) e s ($asm 'argument))))))))

(define (spica-macroexpand expr)
  (if (pair? expr)
      (let ((head (car expr)))
        (if (symbol? head)
            (let ((expander (symbol-macro (obj->code head))))
              (if (code->obj expander)
                  (spica-macroexpand (spica-apply expander `(quote ,(cdr expr))))
                  expr))
            expr))
      expr))

(define-macro (define-special name arg-var . bodies-alist)
  (let ((next-proc (gensym))
        (cbodies (cdr (assq 'pass2 bodies-alist)))
        (abodies (cdr (or (assq 'pass1 bodies-alist)
                          '(#f . #f)))))
    `(begin
       (set! *pass2-procedure*
             (let ((,next-proc *pass2-procedure*))
               (lambda ($expr $env $sets $next)
                 (if (eq? ',name (car $expr))
                     ,(destructure-expander arg-var '(cdr $expr)
                                            `(begin ,@cbodies)
                                            `(,next-proc $expr $env $sets $next))
                     (,next-proc $expr $env $sets $next)))))
       ,(if abodies
            `(set! *pass1-procedure*
                   (let ((,next-proc *pass1-procedure*))
                     (lambda ($expr $bounds)
                       (if (eq? ',name (car $expr))
                           ,(destructure-expander arg-var '(cdr $expr)
                                                  `(begin ,@abodies)
                                                  `(,next-proc $expr $bounds))
                           (,next-proc $expr $bounds)))))
            '()))))

(define-macro (define-primitive name arg-var . bodies)
  (let ((next-proc (gensym))
        (sym (gensym)))
    `(set! *pass2-procedure*
           (let ((,next-proc *pass2-procedure*))
             (lambda ($expr $env $sets $next)
               (let ((,sym (car $expr)))
                 (if (and (eq? ',name ,sym)
                          (not (memq ,sym (env-bounds $env)))
                          (not (memq ,sym (env-frees $env))))
                     ,(destructure-expander arg-var '(cdr $expr)
                                            `(begin ,@bodies)
                                            `(,next-proc $expr $env $sets $next))
                     (,next-proc $expr $env $sets $next))))))))

(define-special quote (obj)
  (pass2
   ($begin! ($asm 'constant obj) $next))
  (pass1
   (values $expr '() '())))

(define-special lambda (vars . bodies)
  (pass2
   (call-with-values (lambda ()
                       (compile/pass1 $expr '()))
     (lambda (x f s)
       (compile x $env $sets $next))))
  (pass1
   (let loop ((vars vars)
              (arity 0)
              (nvars '()))
     (cond
      ((symbol? vars)
       (loop '() (- (+ arity 1)) (cons vars nvars)))
      ((null? vars)
       (let loop ((exprs bodies)
                  (eexprs '())
                  (frees '())
                  (sets '()))
         (if (null? exprs)
             (values `(%lambda ,(reverse nvars) ,arity ,(reverse eexprs) ,frees ,(set-intersect sets nvars))
                     (set-minus frees $bounds)
                     (set-minus sets nvars))
             (call-with-values (lambda ()
                                 (compile/pass1 (car exprs) nvars))
               (lambda (e f s)
                 (loop (cdr exprs) (cons e eexprs) (set-union f frees) (set-union s sets)))))))
      ((pair? vars)
       (loop (cdr vars) (+ arity 1) (cons (car vars) nvars)))
      (else
       (error "invalid lambda vars"))))))

(define-special %lambda (vars arity bodies free sets)
  (pass2
   ;; freeにはグローバル変数も含まれるため、それを取り除く
   (let ((%free (if (env-top? $env)
                    '()
                    (set-union (set-intersect free (car $env))
                               (set-intersect free (cdr $env))))))
     (let ((body-env (make-env vars %free))
           (body-sets (set-union sets (set-intersect $sets %free))))
       (collect-free %free $env
                     ($begin! ($asm 'close (length %free)
                                    ($begin! ($asm 'check-args arity)
                                             (if (< arity 0)
                                                 ($begin! ($asm 'list (- (- arity) 1))
                                                          ($asm 'adjust-vaargs arity))
                                                 ($begin!))
                                             (make-boxes sets
                                                         vars
                                                         (let loop ((bodies bodies)
                                                                    (cc ($asm 'return (length vars))))
                                                           (if (null? bodies)
                                                               cc
                                                               (compile (car bodies)
                                                                        body-env
                                                                        body-sets
                                                                        (loop (cdr bodies) cc)))))))
                              $next)))))
  (pass1
   (values $expr (list-ref $expr 4) (list-ref $expr 5))))

(define-special if (test then else)
  (pass2
   (let* ((elsec (compile else $env $sets (if (tail? $next)
                                              $next
                                              ($begin!))))
          (thenc (compile then $env $sets (if (tail? $next)
                                              $next
                                              ($asm 'jump (bytecodes-packed-length! elsec))))))
     (compile test $env $sets
              ($begin! ($asm 'branch-false (bytecodes-packed-length! thenc))
                      thenc
                      elsec
                      (if (tail? $next)
                          ($begin!)
                          $next)))))
  (pass1
   (call-with-values (lambda ()
                       (compile/pass1 test $bounds))
     (lambda (test-x test-f test-s)
       (call-with-values (lambda ()
                           (compile/pass1 then $bounds))
         (lambda (then-x then-f then-s)
           (call-with-values (lambda ()
                               (compile/pass1 else $bounds))
             (lambda (else-x else-f else-s)
               (values `(if ,test-x ,then-x ,else-x)
                       (set-union test-f (set-union then-f else-f))
                       (set-union test-s (set-union then-s else-s)))))))))))

(define-special set! (var x)
  (pass2
   (compile-lookup var $env
                   (lambda (n)
                     (compile x $env $sets ($begin! ($asm 'assign-local n)
                                                    $next)))
                   (lambda (n)
                     (compile x $env $sets ($begin! ($asm 'assign-free n)
                                                    $next)))
                   (lambda (sym)
                     ;; TODO: need to use gref object.
                     (compile x $env $sets ($begin! ($asm 'assign-global sym)
                                                    $next)))))
  (pass1
   (call-with-values (lambda ()
                       (compile/pass1 x $bounds))
     (lambda (x f s)
       (values `(set! ,var ,x)
               (if (memq var $bounds)
                   f
                   (set-cons var f))
               (set-cons var s))))))


(define-special define (var . x)
  (pass2
   (if (pair? var)
       (compile `(define ,(car var) (lambda ,(cdr var) ,@x)) $env $sets $next)
       (compile `(set! ,var ,(car x)) $env $sets $next)))
  (pass1
   ;; compile/pass1中にdefineが見つかるということは、lambdaの中でdefineがあるということ。これはエラーとする。
   (error "can't define in lambda")))

(define-special define-macro ((name . args) . bodies)
  (pass2
   (compile `(lambda ,args ,@bodies) $env $sets
            ($begin! ($asm 'argument)
                     ($asm 'constant name)
                     ($asm 'assign-object 2)
                     $next)))
  (pass1
   ;; compile/pass1中にdefine-macroが見つかるということは、lambdaの中でdefine-macroがあるということ。これはエラーとする。
   (error "can't define-macro in lambda")))

(define-special begin xs
  (pass2
   (let loop ((ys (reverse xs))
              (cc $next))
     (if (null? ys)
         cc
         (loop (cdr ys) (compile (car ys) $env $sets cc)))))
  (pass1
   (let loop ((expr xs)
              (eexpr '())
              (frees '())
              (sets '()))
     (if (null? expr)
         (values `(begin ,@(reverse eexpr)) frees sets)
         (call-with-values (lambda ()
                             (compile/pass1 (car expr) $bounds))
           (lambda (x f s)
             (loop (cdr expr) (cons x eexpr) (set-union f frees) (set-union s sets))))))))

;; primitives
(define-primitive call/cc (x)
  (let ((c ($begin! ($asm 'conti)
                    ($asm 'argument)
                    (compile x $env $sets
                             (if (tail? $next)
                                 ($begin! ($asm 'shift (bytecodes-return-arg $next))
                                          ($asm 'apply))
                                 ($asm 'apply))))))
    (if (tail? $next)
        ($begin! ($asm 'reset-$ap)
                 c)
        ($begin! ($asm 'frame (bytecodes-packed-length! c))
                 c
                 $next))))

(define-primitive + (a b)
  (compile-arguments (list a b) $env $sets ($begin! ($asm 'add) $next)))

(define-primitive - (a b)
  (compile-arguments (list a b) $env $sets ($begin! ($asm 'sub) $next)))

(define-primitive * (a b)
  (compile-arguments (list a b) $env $sets ($begin! ($asm 'mul) $next)))

(define-primitive eq? (x0 x1)
  (compile-arguments (list x0 x1) $env $sets ($begin! ($asm 'eq) $next)))

(define-primitive object-type (x0)
  (compile-arguments (list x0) $env $sets ($begin! ($asm 'type) $next)))

(define-primitive alloc-object (type word)
  (if (fixnum? word)
      (compile-arguments (list type) $env $sets
                         ($begin! ($asm 'alloc word)
                                 $next))
      (compile-arguments (list word type) $env $sets
                         ($begin! ($asm 'alloc-n)
                                 $next))))

(define-primitive object-ref (obj index)
  (if (fixnum? index)
      (compile-arguments (list obj) $env $sets
                         ($begin! ($asm 'refer-object index)
                                  $next))
      (compile-arguments (list index obj) $env $sets
                         ($begin! ($asm 'refer-object-n)
                                  $next))))

(define-primitive object-set! (obj index v)
  (if (fixnum? index)
      (compile-arguments (list v obj) $env $sets
                         ($begin! ($asm 'assign-object index)
                                  $next))
      (compile-arguments (list index v obj) $env $sets
                         ($begin! ($asm 'assign-object-n)
                                  $next))))

(define-primitive object-size (obj)
  (compile-arguments (list obj) $env $sets
                     ($begin! ($asm 'size)
                              $next)))

;; (define-primitive byte-ref (obj index)
;;   (compile-arguments (list index obj) $env $sets
;;                      ($begin (asm 'refer-byte-n)
;;                              $next)))

;; (define-primitive byte-set! (obj index v)
;;   (compile-arguments (list index v obj) $env $sets
;;                      ($begin (asm 'assign-byte-n)
;;                              $next)))

(define-primitive values args
  ($begin! ($asm 'save-$ap)
           (compile-arguments args $env $sets
                              ($begin! ($asm 'argument)
                                       ($asm 'values)
                                       $next))))

(define-primitive apply-values (proc)
  (if (tail? $next)
      ($begin! ($asm 'reset-$ap)
               ($asm 'values-argument)
               (compile-arguments (list proc) $env $sets
                                  ($begin! ($asm 'shift (bytecodes-return-arg $next))
                                           ($asm 'apply))))
      (let ((cc ($begin! ($asm 'values-argument)
                         (compile-arguments (list proc) $env $sets
                                            ($asm 'apply)))))
        ($begin! ($asm 'frame (bytecodes-packed-length! cc))
                 cc
                 $next))))
  
(define-primitive apply (proc . args)
  (when (null? args)
    (error "apply args is null"))
  (if (tail? $next)
      ($begin! ($asm 'reset-$ap)
               (compile-arguments args $env $sets
                                  ($begin! ($asm 'list-argument)
                                           (compile-arguments (list proc) $env $sets
                                                              ($begin! ($asm 'shift (bytecodes-return-arg $next))
                                                                       ($asm 'apply))))))
      (let ((cc (compile-arguments args $env $sets
                                   ($begin! ($asm 'list-argument)
                                            (compile-arguments (list proc) $env $sets ($asm 'apply))))))
        ($begin! ($asm 'frame (bytecodes-packed-length! cc))
                 cc
                 $next))))

(define-primitive fixnum->char (n)
  (compile-arguments (list n) $env $sets
                     ($begin! ($asm 'fixnum->char)
                              $next)))

(define-primitive char->fixnum (c)
  (compile-arguments (list c) $env $sets
                     ($begin! ($asm 'char->fixnum)
                              $next)))

;;
(define (compile expr env sets next-cc)
  (cond
    ((symbol? expr)
     (if (env-top? env)
         ($begin! ($asm 'refer-global expr)
                  next-cc)
         (compile-refer expr env (if (memq expr sets)
                                     ($begin! ($asm 'indirect)
                                              next-cc)
                                     next-cc))))
    ((pair? expr)
     (*pass2-procedure* (spica-macroexpand expr) env sets next-cc))
    (else
     ($begin! ($asm 'constant expr) next-cc))))

(define (compile/pass1 expr bounds)
  (cond
   ((symbol? expr)
    (values expr
            (if (memq expr bounds)
                '()
                (list expr))
            '()))
   ((pair? expr)
    (*pass1-procedure* (spica-macroexpand expr) bounds))
   (else
    (values expr '() '()))))

;;;
(define (spica-eval x)
  (let ((code (bytecode-serialize (compile x '() '() ($asm 'halt)))))
    (set! *pc* code)
    (vm)
    (code->obj *value0*)))

(define (spica-apply proc . args)
  (when (null? args)
    (error "apply args is null"))
  (let ((cc (compile-arguments args '() '()
                               ($begin! ($asm 'list-argument)
                                        ($asm 'constant proc)
                                        ($asm 'apply)))))
    (let ((code (bytecode-serialize ($begin! ($asm 'frame (bytecodes-packed-length! cc))
                                             cc
                                             ($asm 'halt)))))
      (print-asm code)
      (set! *pc* code)
      (vm)
      (code->obj *value0*))))
       
(define (spica-symbol? obj)
  (= (mem-type obj) @symbol))

(define (symbol-macro sym)
  (if (spica-symbol? sym)
      (object-ref sym 2)
      (error "[bug] not symbol")))

;;;

(define memory (make-u32vector (/ 16777216 4)))

(define *heap-pointer* 0)

(define allocated-objects '())

(define (clear-heap)
  (u32vector-fill! memory 0)
  (set! allocated-objects '())
  (set! *heap-pointer* 4)
  (set! *stack-pointer* *stack-base*)
  (set! *frame-pointer* *stack-base*)
  #f)

(define-macro (define-enum . consts)
  (let loop ((i 0)
             (consts consts)
             (bodies '()))
    (if (null? consts)
        `(begin ,@bodies)
        (loop (+ i 1)
              (cdr consts)
              (cons `(define ,(car consts) ,i) bodies)))))

(define-enum
  @fixnum
  @char
  @false
  @true
  @nil
  @eof
  @undefined
  
  @bytearray
  @box
  @bignum
  @string
  @symbol
  @pair
  @vector
  @procedure
  @record
  @object
  @wordarray
  @port
  )
  
(define (alloc-object type word-size)
  (mem-write-word *heap-pointer* word-size)
  (inc! *heap-pointer* 1)
  (begin0
    (logior *heap-pointer* (ash type 24))
    (inc! *heap-pointer* word-size)))

(define (object-ref obj n)
  (mem-read-word (+ obj n)))

(define (object-set! obj n v)
  (mem-write-word (+ obj n) v))

(define (mem-read-word addr)
  (u32vector-ref memory (logand addr #x00ffffff)))

(define (mem-read-bytes addr)
  (let ((bytes (make-vector 4))
        (w (mem-read-word addr)))
    (vector-set! bytes 0 (logand w #xff))
    (vector-set! bytes 1 (logand (ash w -8) #xff))
    (vector-set! bytes 2 (logand (ash w -16) #xff))
    (vector-set! bytes 3 (logand (ash w -24) #xff))
    bytes))

(define (mem-write-word addr w)
  (u32vector-set! memory (logand addr #x00ffffff) w))

(define (mem-write-bytes addr bytes)
  (mem-write-word addr (logior (ash (vector-ref bytes 3) 24)
                               (ash (vector-ref bytes 2) 16)
                               (ash (vector-ref bytes 1) 8)
                               (vector-ref bytes 0))))
(define (sizeof-word addr)
  (logand (mem-read-word (- addr 1)) #x3fffff))

(define (mem-type addr)
  (logand (ash addr -24) #x7f))

(define (byte->word b)
  (+ (ash (- b 1) -2) 1))

(define (serialize-string str)
  (let* ((len (string-length str))
         (addr (alloc-object @string 2))
         (data (alloc-object @bytearray (byte->word len)))
         (bytes (make-vector 4 0)))
    (mem-write-word addr (obj->code len))
    (mem-write-word (+ addr 1) data)
    (dotimes (i len)
      (let ((j (logand i #x3)))
        (vector-set! bytes j (char->ucs (string-ref str i)))
        (when (= j 3)
          (mem-write-bytes (+ addr (ash i -2)) bytes)
          (vector-set! bytes 0 0)
          (vector-set! bytes 1 0)
          (vector-set! bytes 2 0)
          (vector-set! bytes 3 0))))
    (unless (= (logand len #x3) 0)
      (mem-write-bytes (+ addr (ash len -2)) bytes))
    addr))

(define (deserialize-string addr)
  (let ((len (code->obj (mem-read-word addr)))
        (data (mem-read-word (+ addr 1))))
    (let loop ((i 0)
               (char-list '())
               (w (mem-read-word data)))
      (if (< i len)
          (loop (+ i 1)
                (cons (ucs->char (logiand w #xff)) char-list)
                (if (= (logiand i 3) 3)
                    (mem-read-word (+ data (ash (+ i 1) -2)))
                    (ash w -8)))
          (list->string (reverse char-list))))))

(define (intern str)
  ;; 0: 印字形式
  ;; 1: 値
  ;; 2: マクロ
  (let ((addr (alloc-object @symbol 3))
        (str-addr (obj->code str)))
    (mem-write-word addr str-addr)
    (mem-write-word (+ addr 1) #b00000000000000000000010100001011)
    (mem-write-word (+ addr 2) (obj->code #f))
    addr))
  
(define (serialize-symbol sym)
  (intern (symbol->string sym)))

(define (deserialize-symbol addr)
  (string->symbol (deserialize-string (mem-read-word addr))))

(define (serialize-u32vector vec)
  (let* ((len (u32vector-length vec))
         (addr (alloc-object @bytearray len)))
    (dotimes (i len)
      (mem-write-word (+ addr i) (u32vector-ref vec i)))
    addr))

(define (deserialize-u32vector addr)
  (let* ((len (sizeof-word addr))
         (vec (make-u32vector len)))
    (dotimes (i len)
      (u32vector-set! vec i (mem-read-word (+ addr i))))
    vec))

(define (serialize-compiled-code cc)
  (serialize-u32vector (list->u32vector (compiled-code-code-list cc))))

(define (obj->code obj)
  (or (and-let* ((p (assq obj allocated-objects)))
        (cdr p))
      (cond
       ((fixnum? obj)
        obj)
       ((char? obj)
        (logior #b01000000000000000000000000000000 (char->ucs obj)))
       ((eq? obj #f)
        #b01010000000000000000000000000000)
       ((eq? obj #t)
        #b01011000000000000000000000000000)
       ((eq? obj '())
        #b01010100000000000000000000000000)
       ((eq? obj (eof-object))
        #b01011100000000000000000000000000)
       ((eq? obj (undefined))
        #b01010010000000000000000000000000)
       ((is-a? obj <string>)
        (let ((code (serialize-string obj)))
          (set! allocated-objects (cons (cons obj code) allocated-objects))
          code))
       ((is-a? obj <symbol>)
        (let ((code (serialize-symbol obj)))
          (set! allocated-objects (cons (cons obj code) allocated-objects))
          code))
       ((is-a? obj <u32vector>)
        (let ((code (serialize-u32vector obj)))
          (set! allocated-objects (cons (cons obj code) allocated-objects))
          code))
       ((pair? obj)
        (let ((code (alloc-object @pair 2)))
          (mem-write-word code (obj->code (car obj)))
          (mem-write-word (+ code 1) (obj->code (cdr obj)))
          code))
       (else
        (errorf "Unsupported ~s" obj)))))

(define (code->obj code)
  (cond
   ((not (integer? code))
    code)
   ((= (logand code #b11000000000000000000000000000000) 0)
    ;; fixnum
    code)
   ((= (ash code -24) #b01000000)
    (ucs->char (logand code #x00ffffff)))
   ((= code #b01010000000000000000000000000000)
    #f)
   ((= code #b01011000000000000000000000000000)
    #t)
   ((= code #b01010100000000000000000000000000)
    '())
   ((= code #b01011100000000000000000000000000)
    (eof-object))
   ((= code #b01010010000000000000000000000000)
    (undefined))
   ((= (mem-type code) @symbol)
    (deserialize-symbol code))
   ((= (mem-type code) @pair)
    (cons (code->obj (mem-read-word code)) (code->obj (mem-read-word (+ code 1)))))
   ((= (mem-type code) @procedure)
    (lambda args (spica-apply code args)))
   (else
    (errorf "object ~x ~d" code (mem-type code))
    (list 'object code (mem-type code)))))
  
;; code0: O-type operand
(define (mnemonic->code0 code)
  (list code))

(define (mnemonic->code0+obj code obj)
  (list code (obj->code obj)))

;; code1: I-type operand
(define (mnemonic->code1 code arg0)
  (list (logand #xffffffff (logior (ash arg0 8) code))))

(define (mnemonic->code1+obj code arg0 obj)
  (append (mnemonic->code1 code arg0)
          (list (obj->code obj))))

(define mnemonic-table
  (let loop ((syms '(fetch
                     halt
                     set-$free
                     set-$pc
                     refer-local
                     refer-free
                     indirect
                     constant
                     close
                     box
                     branch-false
                     jump
                     assign-local
                     assign-free
                     conti
                     nuate
                     frame
                     argument
                     shift
                     apply
                     return
                     add
                     sub
                     mul
                     eq
                     alloc
                     alloc-n
                     refer-object
                     assign-object
                     refer-object-n
                     assign-object-n
                     type
                     reset-$ap
                     check-args
                     adjust-vaargs
                     list
                     list-argument
                     save-$ap
                     values
                     values-argument
                     fixnum->char
                     char->fixnum
                     size
                     refer-global
                     assign-global))
             (i 0)
             (alist '()))
    (if (null? syms)
        alist
        (loop (cdr syms) (+ i 1) (cons (cons (car syms) i) alist)))))

(define (mnemonic->code sym)
  (cdr (assq sym mnemonic-table)))

(define (code->mnemonic code)
  (let loop ((rest mnemonic-table))
    (cond
     ((null? rest)
      (errorf "unknown code: ~a" code))
     ((= (cdar rest) (logand code #xff))
      (caar rest))
     (else
      (loop (cdr rest))))))

(define (code->arg code)
  (ash (logior (if (logbit? 31 code) (ash -1 32) 0)
               code)
       -8))

(define (print-asm addr)
  (print "")
  (%print-asm addr 0))

(define (%print-asm addr indent)
  (let ((code-vector (deserialize-u32vector addr)))
    (let loop ((code-list (u32vector->list code-vector))
               (i 0))
      (if (null? code-list)
          #f
          (let ((mnemonic (code->mnemonic (car code-list))))
            (format #t "~a~8,'0x: " (make-string indent #\space) (+ addr i))
            (case mnemonic
              ((halt indirect conti argument apply add sub mul eq type reset-$ap list-argument
                     save-$ap values values-argument refer-object-n assign-object-n alloc-n
                     fixnum->char char->fixnum size)
               (format #t "~s~%" (list mnemonic))
               (loop (cdr code-list) (+ i 1)))
              ((refer-local refer-free box assign-local assign-free return set-$pc set-$free
                            alloc mem-ref check-args adjust-vaargs list shift refer-object assign-object)
               (format #t "~s~%" (list mnemonic (code->arg (car code-list))))
               (loop (cdr code-list) (+ i 1)))
              ((branch-false frame jump)
               (format #t "(~s #x~8,'0x)~%" mnemonic (+ addr (+ i (code->arg (car code-list)) 1)))
               (loop (cdr code-list) (+ i 1)))
              ((nuate constant)
               (format #t "~s~%" (list mnemonic (code->obj (cadr code-list))))
               (loop (cddr code-list) (+ i 2)))
              ((close)
               (format #t "(~s ~s #x~8,'0x)~%" mnemonic (code->arg (car code-list)) (cadr code-list))
               (%print-asm (cadr code-list) (+ indent 2))
               (loop (cddr code-list) (+ i 2)))))))))

(define (dump-heap filename)
  (call-with-output-file filename
    (lambda (out)
      (dotimes (i *heap-pointer*)
        (let ((w (mem-read-word i)))
          (write-byte (logiand w #xff) out)
          (write-byte (logiand (ash w -8) #xff) out)
          (write-byte (logiand (ash w -16) #xff) out)
          (write-byte (logiand (ash w -24) #xff) out))))))

(define (dump-heap-as-data filename)
  (call-with-output-file filename
    (lambda (out)
      (let ((word-len (byte->word *heap-pointer*)))
        (format out "  :bootcode-size~%")
        (format out "  (@data #x~8,'0x)~%" word-len)
        (format out "  :bootcode~%")
        (dotimes (i word-len)
          (format out "  (@data #x~8,'0x)~%" (mem-read-word i)))))))

;; VM

(define (index sp i)
  (mem-read-word (+ sp 1 i)))

(define (index-set! sp i v)
  (mem-write-word (+ sp 1 i) v))

(define (save-stack)
  (let* ((len (- *stack-base* *stack-pointer*))
         (addr (alloc-object @wordarray (byte->word len))))
    (let copy ((i 0))
      (unless (= i len)
        (mem-write-word (+ addr i) (mem-read-word (+ *stack-pointer* 1 i)))
        (copy (+ i 1))))
    addr))

(define (restore-stack addr)
  (let ((s (sizeof-byte addr)))
    (let copy ((i 0))
      (unless (= i s)
        (mem-write-word (- *stack-base* i) (mem-read-word (- (+ addr (- s 1)) i)))
        (copy (+ i 1))))
    (set! *stack-pointer* (- *stack-base* s))))

(define (continuation)
  (let* ((addr (save-stack))
         (cc ($begin! ($asm 'refer-local 0)
                      ($asm 'nuate addr)
                      ($asm 'return 0))))
    (closure (bytecodes-serialize cc))))

(define (box v)
  (let ((addr (alloc-object @box 1)))
    (mem-write-word addr v)
    addr))

(define (unbox addr)
  (mem-read-word addr))

(define (set-box! addr v)
  (mem-write-word addr v))

(define (shift-args n m)
  (let nxtarg ((i (- n 1)))
    (unless (< i 0)
      (index-set! *stack-pointer* (+ i m) (index *stack-pointer* i))
      (nxtarg (- i 1))))
  (set! *stack-pointer* (+ *stack-pointer* m))
  (set! *arg-pointer* (+ *stack-pointer* n)))

(define (closure body n)
  (let ((addr (alloc-object @procedure (+ n 1))))
    (mem-write-word addr body)
    (let f ((i 0))
      (unless (= i n)
        (mem-write-word (+ addr 1 i) (index *stack-pointer* i))
        (f (+ i 1))))
    addr))

(define (closure-body addr)
  (mem-read-word addr))

(define (index-closure addr n)
  (mem-read-word #?=(+ addr 1 n)))

#|

 The stack structure when a procedure is called.
 
#x0000000 |                  |
          |                  |
          ~~~~~~~~~~~~~~~~~~~~
          ~~~~~~~~~~~~~~~~~~~~
          |                  | <- $sp
          |     argN         |
          |     ...          |
          |     arg1         |
          |     arg0         | <- $fp
          |previous $pc      | <- return address
 growth   |previous $ap      |
   ^      |previous $fp      |
   |      |previous $closure |
   |      |                  |
          |                  |
#xffffff  |                  | 




 The stack structure when (frame xxx) is executed.
 

#x0000000 |                  |
          |                  |
          ~~~~~~~~~~~~~~~~~~~~
          ~~~~~~~~~~~~~~~~~~~~
          |                  | <- $sp
          |     arg1         |
          |     arg0         | <- $ap
          | return address   |
          | current $ap      |
          | current $fp      |
          | current $closure |
          |     argN         |
          |     ...          |
          |     arg1         |
          |     arg0         | <- $fp
          |previous $pc      |
 growth   |previous $ap      |
   ^      |previous $fp      |
   |      |previous $closure |
   |      |                  |
          |                  |
#xffffff  |                  | 


|#



;; a: accumulator
;; x: next
;; f: current frame
;; c: current closure
;; s: stack

(define *pc* 0)
(define *value0* 0)
(define *valnum* 0)
(define *values* (make-vector 16))
(define *closure* 0)
(define *stack-base* (/ (- 16777216 4) 4))
(define *stack-pointer* *stack-base*)
(define *frame-pointer* *stack-base*)
(define *arg-pointer* 0)

(define (push-stack v)
  (mem-write-word *stack-pointer* v)
  (dec! *stack-pointer* 1))

(define (pop-stack)
  (inc! *stack-pointer* 1)
  (mem-read-word *stack-pointer*))

(define (vm)
  #?=(format "~8,'0x" *pc*)
  (let loop ((code (mem-read-word *pc*)))
    (inc! *pc* 1)
    (case #?=(code->mnemonic code)
      ((halt)
       (print "Halted")
       (format #t "value0: ~8,'0x~%" *value0*)
       (format #t "~s~%" (code->obj *value0*))
       *value0*)
      ((set-$free)
       (let ((heap-pointer (code->arg code)))
         (set! *heap-pointer* heap-pointer)
         (vm)))
      ((refer-local)
       (let ((n (code->arg code)))
         (set! *value0* #?=(mem-read-word #?=(- *frame-pointer* n)))
         (set! *valnum* 1)
         (vm)))
      ((refer-free)
       (let ((n (code->arg code)))
         (set! *value0* #?=(index-closure *closure* n))
         (set! *valnum* 1)
         (vm)))
      ((indirect)
       (set! *value0* #?=(unbox *value0*))
       (set! *valnum* 1)
       (vm))
      ((constant)
       (set! *value0* (mem-read-word *pc*))
       (set! *valnum* 1)
       (inc! *pc* 1)
       (vm))
      ((close)
       (let ((n (code->arg code))
             (body (mem-read-word *pc*)))
         (set! *value0* #?=(closure body n))
         (set! *valnum* 1)
         (set! *stack-pointer* (+ *stack-pointer* n))
         (inc! *pc* 1)
         (vm)))
      ((box)
       (let ((n (code->arg code)))
         (index-set! *stack-pointer* n (box (index *stack-pointer* n)))
         (vm)))
      ((branch-false)
       (unless (code->obj *value0*)
         (set! *pc* (+ *pc* (code->arg code))))
       (vm))
      ((jump)
       (set! *pc* (+ *pc* (code->arg code)))
       (vm))
      ((set-$pc)
       (set! *pc (code->arg code))
       (vm))
      ((assign-local)
       (set-box! (mem-read-word (- *frame-pointer* (code->arg code))) *value0*)
       (vm))
      ((assign-free)
       (set-box! (index-closure *closure* (code->arg code)) *value0*)
       (vm))
      ((conti)
       (set! *value0* (continuation))
       (set! *valnum* 1)
       (vm))
      ((nuate stack)
       (let ((stack (mem-read-word *pc*)))
         (inc! *pc* 1)
         (restore-stack stack)
         (vm)))
      ((frame)
       (let ((ret (code->arg code)))
         (push-stack *closure*)
         (push-stack *frame-pointer*)
         (push-stack *arg-pointer*)
         (push-stack (+ *pc* ret))
         (set! *arg-pointer* *stack-pointer*)
         (vm)))
      ((save-$ap)
       (push-stack *arg-pointer*)
       (set! *arg-pointer* *stack-pointer*)
       (vm))
      ((values)
       (let loop ((i 0)
                  (p *arg-pointer*))
         (cond
          ((= i 16)
           (error "Too many values"))
          ((= p *stack-pointer*)
           (set! *valnum* i)
           (set! *stack-pointer* *arg-pointer*)
           (set! *arg-pointer* (pop-stack)))
          ((= i 0)
           (set! *value0* (mem-read-word p))
           (loop (+ i 1) (- p 1)))
          (else
           (vector-set! *values* (- i 1) (mem-read-word p))
           (loop (+ i 1) (- p 1)))))
       (vm))
      ((reset-$ap)
       (set! *arg-pointer* *stack-pointer*)
       (vm))
      ((argument)
       (push-stack *value0*)
       (vm))
      ((list-argument)
       (unless (or (= #?=(mem-type #?=*value0*) @pair)
                   (= *value0* (obj->code '())))
         (error "not pair"))
       (let loop ((pair *value0*))
         (if (= pair (obj->code '()))
             (vm)
             (begin
               (push-stack (mem-read-word pair))
               (loop (mem-read-word (+ pair 1)))))))
      ((values-argument)
       (let loop ((i 0))
         (cond
          ((= i *valnum*)
           (vm))
          ((= i 0)
           (push-stack *value0*)
           (loop (+ i 1)))
          (else
           (push-stack (vector-ref *values* (- i 1)))
           (loop (+ i 1))))))
      ((shift)
       (let ((n (- *arg-pointer* *stack-pointer*))
             (m (code->arg code)))
         (shift-args n m)
         (vm)))
      ((apply)
       (set! *closure* *value0*)
       (set! *pc* (closure-body *value0*))
       (set! *frame-pointer* *arg-pointer*)
       (vm))
      ((check-args)
       (let ((n (code->arg code))
             (argnum (- *frame-pointer* *stack-pointer*)))
         (if (<= 0 n)
             (unless (= n argnum)
               (error "invalid arg num"))
             (let ((m (- (- n) 1)))
               (unless (<= m argnum)
                 (error "invalid arg num"))))
         (vm)))
      ((adjust-vaargs)
       (let ((n (code->arg code)))
         (unless (<= 0 n)
           (let ((m (- n)))
             (set! *stack-pointer* #?=(- *frame-pointer* m))
             (mem-write-word (+ *stack-pointer* 1) *value0*))))
       (vm))
      ((return)
       (let ((n (code->arg code)))
         (set! *stack-pointer* (+ *stack-pointer* n))
         (set! *pc* (index *stack-pointer* 0))
         (set! *arg-pointer* (index *stack-pointer* 1))
         (set! *frame-pointer* (index *stack-pointer* 2))
         (set! *closure* (index *stack-pointer* 3))
         (set! *stack-pointer* (+ *stack-pointer* 4))
         (vm)))
      ((add)
       (let ((v0 (code->obj (pop-stack)))
             (v1 (code->obj *value0*)))
         (set! *value0* (obj->code (+ v0 v1)))
         (set! *valnum* 1)
         (vm)))
      ((sub)
       (let ((v0 (code->obj (pop-stack)))
             (v1 (code->obj *value0*)))
         (set! *value0* (obj->code #?=(- v0 v1)))
         (set! *valnum* 1)
         (vm)))
      ((mul)
       (let ((v0 (code->obj (pop-stack)))
             (v1 (code->obj *value0*)))
         (set! *value0* (obj->code #?=(* v0 v1)))
         (set! *valnum* 1)
         (vm)))
      ((eq)
       (let ((v0 (pop-stack))
             (v1 *value0*))
         (set! *value0* (= v0 v1))
         (set! *valnum* 1)
         (vm)))
      ((list)
       (let* ((n #?=(- (code->arg code) 1))
              (limit (- *frame-pointer* n)))
         (let loop ((i (+ *stack-pointer* 1))
                    (addr (obj->code '())))
           (if (= i limit)
               (begin
                 (set! *value0* addr)
                 (set! *valnum* 1))
               (let ((pair (alloc-object @pair 2)))
                 (mem-write-word pair (mem-read-word i))
                 (mem-write-word (+ pair 1) addr)
                 (loop (+ i 1) pair)))))
       (vm))
      ((alloc)
       (let ((w (code->arg code)))
         (set! *value0* (alloc-object *value0* w))
         (set! *valnum* 1))
       (vm))
      ((alloc-n)
       (let ((w (code->obj (pop-stack))))
         (set! *value0* (alloc-object *value0* w))
         (set! *valnum* 1))
       (vm))
      ((refer-object)
       (let ((n (code->arg code)))
         (set! *value0* (mem-read-word (+ *value0* n)))
         (set! *valnum* 1)
         (vm)))
      ((assign-object)
       (let ((n (code->arg code))
             (v (pop-stack)))
         (mem-write-word (+ *value0* n) v)
         (vm)))
      ((refer-object-n)
       (let ((n (code->obj (pop-stack))))
         (set! *value0* (mem-read-word (+ *value0* n)))
         (set! *valnum* 1)
         (vm)))
      ((assign-object-n)
       (let ((v (pop-stack)))
         (let ((n (code->obj (pop-stack))))
           (mem-write-word (+ *value0* n) v)
           (vm))))
      ((type)
       (cond
        ((= (logand *value0* #b11) 0)
         (set! *value0* (mem-type *value0*)))
        ((= (logand *value0* #b11) 1)
         (set! *value0* (obj->code @fixnum)))
        ((= (logand *value0* #b11111111) #b00000011)
         (set! *value0* (obj->code @char)))
        ((= *value0* (obj->code #f))
         (set! *value0* (obj->code @false)))
        ((= *value0* (obj->code #t))
         (set! *value0* (obj->code @true)))
        ((= *value0* (obj->code '()))
         (set! *value0* (obj->code @nil)))
        ((= *value0* (obj->code (eof-object)))
         (set! *value0* (obj->code @eof)))
        ((= *value0* #b00000000000000000000010000001011)
         (set! *value0* (obj->code @undefined))))
       (set! *valnum* 1)
       (vm))
      ((size)
       (set! *value0* (obj->code (sizeof-word *value0*)))
       (set! *valnum* 1)
       (vm))
      ((fixnum->char)
       (set! *value0* (logior (logand *value0* #x00ffffff) #x40000000))
       (set! *valnum* 1)
       (vm))
      ((char->fixnum)
       (set! *value0* (logand *value0* #x00ffffff))
       (set! *valnum* 1)
       (vm))
      )))

;;;
(define (evaluate x)
  (let ((code (bytecodes-serialize (compile x '() '() ($asm 'halt)))))
    (set! *pc* code)
    #?=(code->obj (vm))))

(define (spica-load filename)
  (call-with-input-file filename
    (lambda (in)
      (port-for-each evaluate (cut read in)))))

(define (spica-disasm filename)
  (call-with-input-file filename
    (lambda (in)
      (port-for-each (lambda (expr)
                       (format #t "~%~%~s~%" expr)
                       (let ((code (serialize-compiled-code (compile expr '() '() ($asm 'halt)))))
                         (print-asm code)
                         (set! *pc* code)
                         (code->obj (vm))))
                     (cut read in)))))

(define (show-help progname)
  (format #t "Usage: ~a filename ...~%" progname)
  (exit 1))

(define (make-bootcode)
  (let ((code ($begin! ($asm 'set-$free *heap-pointer*)
                       ($asm 'constant 'spica-boot)
                       ($asm 'refer-object 1)
                       ($asm 'apply))))
    (fold (lambda (w i)
            (mem-write-word i w)
            (+ i 1))
          0
          (u32vector->list (bytecodes-serialize code)))))

;; (define (main args)
;;   (let-args (cdr args)
;;       ((verbose "v|verbose" #f)
;;        (help "h|help" => (cut show-help (car args)))
;;        (output "o|output=s" "boot.ms")
;;        (compile? "c|compile" #f)
;;        (disasm? "S" #f)
;;        . filenames)
;;     (clear-heap)
;;     (if disasm?
;;         (for-each spica-disasm filenames)
;;         (begin
;;           (for-each spica-load filenames)
;;           (when compile?
;;             (make-bootcode)
;;             (dump-heap-as-data output))))
;;     0))
      
#|

(begin
  (clear-heap)
  (evaluate '(begin
               (define fact (lambda (n)
                              (if (eq? n 1)
                                  1
                                  (* n (fact (- n 1))))))
               (fact 10))))

(begin
  (clear-heap)
  (evaluate '(define fact (lambda (n)
                            (if (eq? n 1)
                                1
                                (* n (fact (- n 1))))))))

(begin
  (clear-heap)
  (evaluate '(let ((pair (cons 'a 'b)))
               (set-cdr! pair 'c)
               (cdr pair))))

(begin
  (clear-heap)
  (print-asm (obj->code (compile '(car (cons 'a 'b))))))

(begin
  (clear-heap)
  (precompile '(begin
                 (define fact (lambda (n)
                                (if (eq? n 1)
                                    1
                                    (* n (fact (- n 1))))))
                 (fact 10)))
  (dump-heap "./work/spica/tool/fact.bin")
;;   (dump-heap-as-data "./work/spica/tool/fact.asm")
  )

(evaluate '(begin
             (define flag 0)
             (define cont #f)
             (define proc (lambda ()
                            (if (eq? (call/cc (lambda (c)
                                                (set! cont c)
                                                (if (eq? flag 0)
                                                    'a
                                                    'b)))
                                     'a)
                                'foo
                                'bar)))
             (proc)
             (set! flag 1)
             (proc)))
             
(begin
  (clear-heap)
  (print-asm (serialize-compiled-code (compile '(begin
                                                  (define fact (lambda (n)
                                                                 (if (eq? n 1)
                                                                     1
                                                                     (* n (fact (- n 1))))))
                                                  (fact 10))
                                               '() '() (asm 'halt)))))
|#

;; end of file
