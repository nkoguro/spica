;; -*- coding: utf-8; mode: scheme -*-
;;
;; eval.scm - Spica Lisp Evaluator
;;
;;   Copyright (c) 2011 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

;; just workaround for executable in Gauche
(define-module spica
  (use util.list)
  )

(select-module spica)

;; object
(define-class <spica-object> ()
  ((object-list :allocation :class
                :init-value '())
   (type :init-keyword :type)
   (counter :init-value 0)
   (mem :init-keyword :mem)
   (info-alist :init-value '())))

(define (make-object type size)
  (let ((obj (make <spica-object> :type type :mem (make-vector size))))
    (push! (~ obj'object-list) obj)
    obj))

(define (object-ref obj index)
  (if (= index 0)
      (logior (ash (logand (~ obj'counter) #x3f) 24)
              (vector-length (~ obj'mem)))
      (vector-ref (~ obj'mem) index)))

(define (object-set! obj index value)
  (if (= index 0)
      (error "can't write value at the object header")
      (vector-set! (~ obj'mem) index value)))

(define (object-add-info! obj key val)
  (push! (~ obj'object-list) (cons key val)))

(define (object-info obj key)
  (assoc-ref (~ obj'info-alist) key #f))

(define $uvector 0)
(define $vector 1)
(define $bignum 2)
(define $string 3)
(define $symbol 4)
(define $pair 5)
(define $procedure 6)
(define $port 7)
(define $hash-table 6)
(define $module 7)
(define $gloc 8)
(define $gref 9)

;; gloc
(define (make-gloc val public?)
  (let ((gloc (make-object $gloc 3)))
    (object-set! gloc 1 val)
    (object-set! gloc 2 public?)
    gloc))

(define (gloc-ref gloc)
  (object-ref gloc 1))

(define (gloc-set! gloc val)
  (object-set! gloc 1 val))

(define (gloc-public? gloc)
  (object-ref gloc 2))

(define (gloc-public?-set! gloc public?)
  (object-ref gloc 2 public?))

;; gref
(define (make-gref mod name)
  (let ((gref (make-object $gref 4)))
    (object-set! gref 1 mod)
    (object-set! gref 2 name)
    (object-set! gref 3 #f)
    gref))

(define (gref-module gref)
  (object-ref gref 1))

(define (gref-name gref)
  (object-ref gref 2))

(define (gref-value gref)
  (or (object-ref gref 3)
      (let ((gloc (find-gloc (gref-module gref) (gref-name gref) #t)))
        (if gloc
            (begin
              (object-set! gref 3 gloc)
              (gloc-ref gloc))
            (error "unbound variable: ~a" (gref-name gref))))))

(define (find-gloc mod name ignore-visiblity?)
  (let ((gloc (module-ref mod name)))
    (if gloc
        (if (or ignore-visiblity?
                (gloc-public? gloc))
            gloc
            (find (lambda (mod)
                    (find-gloc mod name #f))
                  (module-imports mod))))))
            
;; module
(define *module-dictionary* (make-hash-table))

(define (make-module name)
  (let ((mod (make-object $module 3)))
    (object-set! mod 1 '())
    (object-set! mod 2 (make-hash-table))
    (hash-table-put! *module-dictionary* name mod)
    mod))

(define (module-imports mod)
  (object-ref mod 1))

(define (module-table mod)
  (object-ref mod 2))

(define (find-module name)
  (hash-table-get *module-dictionary* name))

(define (module-ref mod name)
  (hash-table-get (module-table mod) name #f))

(define (module-set! mod name val public?)
  (hash-table-put! (module-table mod) name (make-gloc val public?)))

;; compile-closure (for Gauche)
(define (expand-binding mod body ignore-visibility?)
  `(let ,(hash-table-fold (module-table mod)
                          (lambda (name gloc forms)
                            (if (or ignore-visibility?
                                    (gloc-public? gloc))
                                (cons (list name (gloc-ref gloc))
                                      forms)
                                forms))
                          '())
     ,body))

(define (compile-closure vars expr-list mod)
  (let loop ((body (expand-binding mod `(lambda ,vars ,@expr-list) #t))
             (module-list (module-imports mod)))
    (if (null? module-list)
        ;; TODO: we have to make closure object.
        (let ((proc (make-object $procedure 3)))
          (object-add-info! proc
                            'gauche-procedure
                            (eval body (current-module)))
          proc)
        (loop (expand-binding (car module-list) body #f)
              (append (cdr module-list) (module-imports (car module-list)))))))

(define-method object-apply ((obj <vector>) . arg)
  (let ((proc (object-info obj 'gauche-procedure)))
    (if proc
        (apply proc arg)
        (error "invalid application: ~s" (cons obj arg)))))

;; TODO: next, write unit tests for compile-closure.

;; eval
#|
(define (eval expr mod) 
  (if (pair? expr)
      (cond
        ((eq? (car expr)
|# 
;; end of file
