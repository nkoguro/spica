`timescale 1ns/1ps

module selector(/*AUTOARG*/
   // Outputs
   read_data, main_write_enable, main_addr, main_write_data,
   tvram_write_enable, tvram_addr, tvram_write_data,
   cgram_write_enable, cgram_addr, cgram_write_data, io_write_enable,
   io_addr, io_write_data, lcd_write_enable, lcd_addr, lcd_write_data,
   serial0_write_enable, serial0_read_request, serial0_addr,
   serial0_write_data, serial1_write_enable, serial1_read_request,
   serial1_addr, serial1_write_data, sd_write_enable, sd_addr,
   sd_write_data,
   // Inputs
   clk, write_enable, read_request, addr, write_data, main_read_data,
   tvram_read_data, cgram_read_data, io_read_data, lcd_read_data,
   serial0_read_data, serial1_read_data, sd_read_data
   );
   input clk;
   input write_enable;
   input read_request;
   input [15:0] addr;
   input [31:0] write_data;
   output [31:0] read_data;

   output main_write_enable;
   output [11:0] main_addr;
   output [31:0] main_write_data;
   input [31:0] main_read_data;

   output tvram_write_enable;
   output [11:0] tvram_addr;
   output [15:0] tvram_write_data;
   input [15:0] tvram_read_data;

   output cgram_write_enable;
   output [11:0] cgram_addr;
   output [7:0] cgram_write_data;
   input [7:0] cgram_read_data;

   output io_write_enable;
   output [7:0] io_addr;
   output [7:0] io_write_data;
   input [7:0] io_read_data;

   output lcd_write_enable;
   output [4:0] lcd_addr;
   output [7:0] lcd_write_data;
   input [7:0] lcd_read_data;

   output serial0_write_enable;
   output serial0_read_request;
   output [3:0] serial0_addr;
   output [7:0] serial0_write_data;
   input [7:0] serial0_read_data;
   
   output serial1_write_enable;
   output serial1_read_request;
   output [3:0] serial1_addr;
   output [7:0] serial1_write_data;
   input [7:0] serial1_read_data;

   output sd_write_enable;
   output [8:0] sd_addr;
   output [31:0] sd_write_data;
   input [31:0] sd_read_data;
   
   wire main_select = (addr[15:12] == 4'h0);
   wire cgram_select = (addr[15:12] == 4'hD);
   wire tvram_select = (addr[15:12] == 4'hE);
   wire sd_select = (addr[15:9] == 7'h78); // F000 - F1FF
   wire serial0_select = (addr[15:4] == 12'hFD0);
   wire serial1_select = (addr[15:4] == 12'hFD1);
   wire lcd_select = (addr[15:8] == 8'hFE);
   wire io_select = (addr[15:8] == 8'hFF);

   reg main_read_select;
   reg cgram_read_select;
   reg tvram_read_select;
   reg lcd_read_select;
   reg io_read_select;
   reg serial0_read_select;
   reg serial1_read_select;
   reg sd_read_select;
   
   always @(posedge clk) begin
      main_read_select <= main_select;
      cgram_read_select <= cgram_select;
      tvram_read_select <= tvram_select;
      lcd_read_select <= lcd_select;
      io_read_select <= io_select;
      serial0_read_select <= serial0_select;
      serial1_read_select <= serial1_select;
      sd_read_select <= sd_select;
   end
   
   assign main_addr = addr[11:0];
   assign main_write_enable = main_select & write_enable;
   assign main_write_data = write_data[31:0];

   assign cgram_addr = addr[11:0];
   assign cgram_write_enable = cgram_select & write_enable;
   assign cgram_write_data = write_data[7:0];
   
   assign tvram_addr = addr[11:0];
   assign tvram_write_enable = tvram_select & write_enable;
   assign tvram_write_data = write_data[15:0];
   
   assign io_addr = addr[7:0];
   assign io_write_enable = io_select & write_enable;
   assign io_write_data = write_data[7:0];
   
   assign lcd_addr = addr[4:0];
   assign lcd_write_enable = lcd_select & write_enable;
   assign lcd_write_data = write_data[7:0];

   assign serial0_addr = addr[3:0];
   assign serial0_write_enable = serial0_select & write_enable;
   assign serial0_read_request = serial0_select & read_request;
   assign serial0_write_data = write_data[7:0];

   assign serial1_addr = addr[3:0];
   assign serial1_write_enable = serial1_select & write_enable;
   assign serial1_read_request = serial1_select & read_request;
   assign serial1_write_data = write_data[7:0];

   assign sd_addr = addr[8:0];
   assign sd_write_enable = sd_select & write_enable;
   assign sd_write_data = write_data[31:0];
   
   assign read_data = main_read_select ? main_read_data[31:0] :
                      sd_read_select ? sd_read_data[31:0] :
                      tvram_read_select ? {16'h0, tvram_read_data} :
                      cgram_read_select ? {24'h0, cgram_read_data} :
                      serial0_read_select ? {24'h0, serial0_read_data} :
                      serial1_read_select ? {24'h0, serial1_read_data} :
                      io_read_select ? {24'h0, io_read_data} :
                      lcd_read_select ? {24'h0, lcd_read_data} :
                      32'hx;
endmodule
