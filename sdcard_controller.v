`timescale 1ns / 1ps

module sdcard_controller(/*AUTOARG*/
   // Outputs
   sd_data_in, sd_clk, sd_cs, read_data,
   // Inputs
   sd_data_out, cd_n, clk50, addr, write_enable, write_data
   );

   input sd_data_out;
   output sd_data_in;
   output sd_clk;
   output reg sd_cs = 1;

   input cd_n;
   input clk50;
   input [8:0] addr;
   input write_enable;
   output [31:0] read_data;
   input [31:0] write_data;

   reg [1:0] sd_clks = 2'b00;
   reg [31:0] read_reg;
   reg [1:0] read_data_sel;
   parameter
      RESP_R1 = 3'h1,
       RESP_R2 = 3'h2,
       RESP_R3_OR_R7 = 3'h3,
       RECV_DATA = 3'h6,
       SEND_DATA = 3'h7;
   reg [2:0] mode = RESP_R1;
   parameter CMD_MAX = 47;
   parameter DATA_MAX = 4095;
   reg [5:0] cmd;
   reg [31:0] arg;
   reg [6:0] crc7;
   reg [31:0] reading_data = 32'hffffffff;
   reg [47:0] writing_data = {48{1'b1}};
   reg [38:0] cmd_resp = 39'h7fffffffff;
   reg [7:0] data_resp = 8'hff;
   reg [2:0] clk_counter = 0;
   reg [11:0] recv_counter = 12'h0;
   reg [12:0] send_counter = 13'h0;
   reg cd_status = 1'b1; // 0: inserted, 1: not inserted 
   reg [19:0] sampling_counter = 0;
   reg [6:0] shamt = 7'h7f;
   
   // recv_status[4] means write_enable for read_buf
   parameter
      RECV_IDLE = 5'h0,
       RECV_R1 = 5'h1,
       RECV_R2 = 5'h2,
       RECV_R3_OR_R7 = 5'h3,
       RECV_START_TOKEN = 5'h6,
       RECV_DATA_RESP = 5'h7,
       SKIP_BUSY = 5'h8,
       SHUTDOWN_WAIT = 5'h9,
       RECV_CRC16 = 5'ha,
       WAIT_SEND_DATA = 5'hb,
       READ_DATA = 5'h1c;
   reg [4:0] recv_status = RECV_IDLE;

   parameter
      SEND_IDLE = 3'h0,
       SEND_CMD = 3'h1,
       CHECK_R1 = 3'h2,
       SEND_START_TOKEN = 3'h3,
       WRITE_DATA = 3'h4;
   reg [2:0] send_status = SEND_IDLE;

   wire sd_clk_posedge = (sd_clks == 2'b01);
   wire sd_clk_negedge = (sd_clks == 2'b10);

   reg start_request = 0;

   wire [31:0] read_buf_word_out;
   read_buf read_buf(.word_out(read_buf_word_out),
                     .clk50(clk50),
                     .write_enable(recv_status[4]),
                     .byte_addr(recv_counter[11:3]),
                     .byte_in(reading_data[7:0]),
                     .word_addr(addr[6:0]));

   wire [31:0] write_buf_word_out;
   wire [7:0] write_buf_byte_out;
   wire write_buf_write_enable;
   write_buf write_buf(.word_out(write_buf_word_out),
                       .byte_out(write_buf_byte_out),
                       .clk50(clk50),
                       .write_enable(write_buf_write_enable),
                       .word_addr(addr[6:0]),
                       .word_in(write_data),
                       .byte_addr(send_counter[11:3]));

   always @(posedge clk50) begin
      sampling_counter <= sampling_counter + 1;
      if (cd_n | sampling_counter == 0) begin
         cd_status <= cd_n;
      end
   end
   
   always @(posedge clk50) begin
      sd_clks[1] <= sd_clks[0];
      clk_counter <= clk_counter + 1;
      if (clk_counter == 0) begin
         sd_clks[0] <= ~sd_clks[0];
      end
   end

   always @(posedge clk50) begin
      if (cd_n | sd_cs) begin
         reading_data <= 32'hffffffff;
         recv_status <= RECV_IDLE;
      end
      else if (sd_clk_posedge) begin
         reading_data <= {reading_data[30:0], sd_data_out};
         case (recv_status)
           RECV_IDLE: begin
              if (start_request) begin
                 cmd_resp <= 39'h7fffffffff;
                 recv_status <= RECV_R1;
              end
           end
           RECV_R1: begin
              recv_counter <= 0;
              if (reading_data[7] == 0) begin
                 cmd_resp[38:32] <= reading_data[6:0];
                 case (mode)
                   RESP_R1: begin
                      recv_status <= SKIP_BUSY;
                   end
                   RESP_R2: begin
                      recv_status <= RECV_R2;
                   end
                   RESP_R3_OR_R7: begin
                      recv_status <= RECV_R3_OR_R7;
                   end
                   RECV_DATA: begin
                      recv_status <= RECV_START_TOKEN;
                   end
                   SEND_DATA: begin
                      recv_status <= WAIT_SEND_DATA;
                   end
                 endcase
              end // if (reading_data[7] == 0)
           end // case: RECV_R1
           SKIP_BUSY: begin
              recv_counter <= 0;
              if (reading_data[0] == 1) begin
                 recv_status <= SHUTDOWN_WAIT;
              end
           end
           SHUTDOWN_WAIT: begin
              recv_counter <= recv_counter + 1;
              if (recv_counter == 8) begin
                 recv_status <= RECV_IDLE;
              end
           end
           RECV_R2: begin
              if (recv_counter == 7) begin
                 cmd_resp[31:24] <= reading_data[7:0];
                 recv_counter <= 0;
                 recv_status <= SHUTDOWN_WAIT;
              end
              else begin
                 recv_counter <= recv_counter + 1;
              end
           end
           RECV_R3_OR_R7: begin
              if (recv_counter == 31) begin
                 cmd_resp[31:0] <= reading_data[31:0];
                 recv_counter <= 0;
                 recv_status <= SHUTDOWN_WAIT;
              end
              else begin
                 recv_counter <= recv_counter + 1;
              end
           end
           RECV_START_TOKEN: begin
              recv_counter <= 0;
              if (reading_data[0] == 0) begin
                 recv_status <= READ_DATA;
              end
           end
           READ_DATA: begin
              if (recv_counter == DATA_MAX) begin
                 recv_counter <= 0;
                 recv_status <= RECV_CRC16;
              end
              else begin
                 recv_counter <= recv_counter + 1;
              end
           end
           RECV_CRC16: begin
              if (recv_counter == 15) begin
                 recv_counter <= 0;
                 recv_status <= SHUTDOWN_WAIT;
              end
              else begin
                 recv_counter <= recv_counter + 1;
              end
           end
           RECV_DATA_RESP: begin
              if (reading_data[4] == 0 && reading_data[0] == 1) begin
                 data_resp <= reading_data[7:0];
                 recv_counter <= 0;
                 recv_status <= SKIP_BUSY;
              end
           end
           WAIT_SEND_DATA: begin
              if (reading_data[7:0] == 8'hff) begin
                 recv_status <= RECV_DATA_RESP;
              end
           end
         endcase // case (recv_status)
      end // if (sd_clk_posedge)
   end // always @ (posedge clk50)

   always @(posedge clk50) begin
      if (cd_n | sd_cs) begin
         writing_data <= {48{1'b1}};
         send_status <= SEND_IDLE;
      end
      else if (sd_clk_negedge) begin
         case (send_status)
           SEND_IDLE: begin
              if (start_request) begin
                 writing_data <= {2'b01, cmd, arg, crc7, 1'b1};
                 send_counter <= 0;
                 send_status <= SEND_CMD;
              end
           end
           SEND_CMD: begin
              writing_data <= {writing_data[46:0], 1'b1};
              send_counter <= send_counter + 1;
              if (send_counter == CMD_MAX) begin
                 send_status <= CHECK_R1;
              end
           end
           CHECK_R1: begin
              send_counter <= 0;
              case (cmd_resp[38:32])
                7'h00: begin
                   if (mode == SEND_DATA) begin
                      writing_data[47] <= 1;
                      send_status <= SEND_START_TOKEN;
                   end
                   else begin
                      writing_data[47] <= 1;
                      send_status <= SEND_IDLE;
                   end
                end
                7'h7f: begin
                   writing_data[47] <= 1;
                   send_status <= CHECK_R1;
                end
                default: begin
                   writing_data[47] <= 1;
                   send_status <= SEND_IDLE;
                end
              endcase // case (cmd_resp[38:32])
           end // case: CHECK_R1
           SEND_START_TOKEN: begin
              if (send_counter == 6) begin
                 // send start bit
                 writing_data[47] <= 0;
                 send_counter <= 0;
                 send_status <= WRITE_DATA;
              end
              else begin
                 send_counter <= send_counter + 1;
              end
           end
           WRITE_DATA: begin
              // "+ 16" is for to send dummy CRC16
              if (send_counter == DATA_MAX + 16) begin
                 send_counter <= 0;
                 writing_data[47] <= 1;
                 send_status <= SEND_IDLE;
              end
              else begin
                 send_counter <= send_counter + 1;
                 if (send_counter[2:0] == 3'h0) begin
                    writing_data[47:40] <= write_buf_byte_out;
                 end
                 else begin
                    writing_data <= {writing_data[46:0], 1'b1};
                 end
              end // else: !if(send_counter[12:3] == DATA_MAX + 2)
           end // case: WRITE_DATA
         endcase
      end // if (sd_clk_negedge)
   end

   always @(posedge clk50) begin
      read_data_sel <= addr[8:7];
      casex (addr) 
        // command index & mode & crc7
        9'h000: read_reg <= {1'b0, crc7, 13'b0, mode, 2'b0, cmd};
        // argument
        9'h001: read_reg <= arg;
        // sd_cs
        9'h00f: read_reg <= {31'b0, sd_cs};
        // status
        9'h010: read_reg <= {24'b0, recv_status, send_status, start_request};
        // R1
        9'h011: read_reg <= {25'b0, cmd_resp[38:32]};
        // R2
        9'h012: read_reg <= {17'b0, cmd_resp[38:24]};
        // R3&R7
        9'h013: read_reg <= cmd_resp[31:0];
        // card detect & shift amount
        //  1xxxxxxx: card is not inserted.
        //  01xxxxxx: card is inserted, but not ready to access.
        //  00xxxxxx: card is inserted, and ready to access.
        9'h020: read_reg <= {24'b0, cd_status, shamt};
        // data response
        9'h01f: read_reg <= {24'b0, data_resp};
        // debug
        9'h0fc: read_reg <= reading_data;
        9'h0fd: read_reg <= writing_data[31:0];
        9'h0fe: read_reg <= {17'b0, writing_data[47:32]};
        9'h0ff: read_reg <= {27'b0, sd_clk_posedge, sd_clk_negedge, sd_data_in, sd_data_out, sd_clk};
        default: read_reg <= 32'hffffffff;
      endcase
   end

   always @(posedge clk50) begin
      if (cd_n) begin
         sd_cs <= 1;
         shamt <= 7'h7f;
      end
      else begin
         if (send_status != SEND_IDLE && recv_status != RECV_IDLE) begin
            start_request <= 0;
         end

         if (write_enable) begin
            case (addr)
              // command index & mode & crc7
              9'h000: {crc7, mode, cmd} <= {write_data[30:24], write_data[10:8], write_data[5:0]};
              // argument
              9'h001: arg <= write_data;
              // sd_cs & start_request
              9'h00f: {start_request, sd_cs} <= {~write_data[0], write_data[0]};
              // shift amount
              9'h020: shamt <= {1'b0, write_data[5:0]};
            endcase
         end
      end
   end

   assign sd_clk = sd_clks[1];
   assign sd_data_in = writing_data[47];
   assign write_buf_write_enable = (addr[8:7] == 2'b11) & write_enable;
   assign read_data = (read_data_sel == 2'b10) ? read_buf_word_out :
                      (read_data_sel == 2'b11) ? write_buf_word_out :
                      read_reg;
endmodule

module read_buf(/*AUTOARG*/
   // Outputs
   word_out,
   // Inputs
   clk50, write_enable, byte_addr, byte_in, word_addr
   );
   input clk50;
   input write_enable;
   input [8:0] byte_addr;
   input [7:0] byte_in;
   input [6:0] word_addr;
   output [31:0] word_out;

   wire [3:0] write_enable_buf;
   
   function [3:0] sel;
      input [1:0] v;
      case (v)
        2'b00: sel = 4'b0001;
        2'b01: sel = 4'b0010;
        2'b10: sel = 4'b0100;
        2'b11: sel = 4'b1000;
      endcase
   endfunction
   
   dual_port_ram #(.ADDR_BITS(7), .DATA_BITS(8))
   buf0(.clk1(clk50), .clk2(clk50),
         .addr1(byte_addr[8:2]),
         .write_enable1(write_enable_buf[0]),
         .data_in1(byte_in),
         .addr2(word_addr),
         .data_out2(word_out[7:0]));

   dual_port_ram #(.ADDR_BITS(7), .DATA_BITS(8))
   buf1(.clk1(clk50), .clk2(clk50),
         .addr1(byte_addr[8:2]),
         .write_enable1(write_enable_buf[1]),
         .data_in1(byte_in),
         .addr2(word_addr),
         .data_out2(word_out[15:8]));

   dual_port_ram #(.ADDR_BITS(7), .DATA_BITS(8))
   buf2(.clk1(clk50), .clk2(clk50),
         .addr1(byte_addr[8:2]),
         .write_enable1(write_enable_buf[2]),
         .data_in1(byte_in),
         .addr2(word_addr),
         .data_out2(word_out[23:16]));

   dual_port_ram #(.ADDR_BITS(7), .DATA_BITS(8))
   buf3(.clk1(clk50), .clk2(clk50),
         .addr1(byte_addr[8:2]),
         .write_enable1(write_enable_buf[3]),
         .data_in1(byte_in),
         .addr2(word_addr),
         .data_out2(word_out[31:24]));

   assign write_enable_buf = {4{write_enable}} & sel(byte_addr[1:0]);
endmodule // read_buffer

module write_buf(/*AUTOARG*/
   // Outputs
   word_out, byte_out,
   // Inputs
   clk50, write_enable, word_addr, word_in, byte_addr
   );
   input clk50;
   input write_enable;
   input [6:0] word_addr;
   input [31:0] word_in;
   output [31:0] word_out;
   input [8:0] byte_addr;
   output [7:0] byte_out;

   wire [31:0] bulk_data;
   
   function [7:0] sel;
      input [1:0] v;
      input [31:0] data;
      case (v)
        2'b00: sel = data[7:0];
        2'b01: sel = data[15:8];
        2'b10: sel = data[23:16];
        2'b11: sel = data[31:24];
      endcase
   endfunction
   
   dual_port_ram #(.ADDR_BITS(7), .DATA_BITS(8))
   buf0(.clk1(clk50), .clk2(clk50),
         .addr1(word_addr),
         .write_enable1(write_enable),
         .data_in1(word_in[7:0]),
         .data_out1(word_out[7:0]),
         .addr2(byte_addr[8:2]),
         .data_out2(bulk_data[7:0]));

   dual_port_ram #(.ADDR_BITS(7), .DATA_BITS(8))
   buf1(.clk1(clk50), .clk2(clk50),
         .addr1(word_addr),
         .write_enable1(write_enable),
         .data_in1(word_in[15:8]),
         .data_out1(word_out[15:8]),
         .addr2(byte_addr[8:2]),
         .data_out2(bulk_data[15:8]));

   dual_port_ram #(.ADDR_BITS(7), .DATA_BITS(8))
   buf2(.clk1(clk50), .clk2(clk50),
         .addr1(word_addr),
         .write_enable1(write_enable),
         .data_in1(word_in[23:16]),
         .data_out1(word_out[23:16]),
         .addr2(byte_addr[8:2]),
         .data_out2(bulk_data[23:16]));

   dual_port_ram #(.ADDR_BITS(7), .DATA_BITS(8))
   buf3(.clk1(clk50), .clk2(clk50),
         .addr1(word_addr),
         .write_enable1(write_enable),
         .data_in1(word_in[31:24]),
         .data_out1(word_out[31:24]),
         .addr2(byte_addr[8:2]),
         .data_out2(bulk_data[31:24]));

   assign byte_out = sel(byte_addr[1:0], bulk_data);
endmodule // write_buf

