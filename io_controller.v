`timescale 1ns/1ps

module io_controller(/*AUTOARG*/
   // Outputs
   io_read_data, LED,
   // Inputs
   clk, io_write_enable, io_addr, io_write_data, SW, BTN_NORTH,
   BTN_SOUTH, BTN_EAST, BTN_WEST, ROT_A, ROT_B, ROT_CENTER
   );

   input clk;
   input io_write_enable;
   input [7:0] io_addr;
   input [7:0] io_write_data;
   output reg [7:0] io_read_data;

   input [3:0] SW;
   input BTN_NORTH;
   input BTN_SOUTH;
   input BTN_EAST;
   input BTN_WEST;
   input ROT_A;
   input ROT_B;
   input ROT_CENTER;
   output reg [7:0] LED = 0;

   parameter BTN_ADDR = 8'h00;
   parameter SW_ADDR = 8'h01;
   parameter ROTSW_ADDR = 8'h02;
   parameter LED_ADDR = 8'h80;

   reg [19:0] counter = 0;
   reg sampling_clk = 0;

   reg btn_north_r;
   reg btn_south_r;
   reg btn_east_r;
   reg btn_west_r;
   reg [3:0] sw_r;
   reg rot_a_r;
   reg rot_b_r;
   reg rot_center_r;
   
   reg [1:0] shift;

   initial begin
      $monitor("LED=%b", LED);
   end
   
   always @(posedge clk) begin
      counter <= counter + 1;
      if (counter == 0) sampling_clk <= ~sampling_clk;

      shift <= {shift[0:0], sampling_clk};
      
      if (shift == 2'b01) begin
         btn_north_r <= BTN_NORTH;
         btn_south_r <= BTN_SOUTH;
         btn_east_r <= BTN_EAST;
         btn_west_r <= BTN_WEST;
         sw_r <= SW;
         rot_a_r <= ROT_A;
         rot_b_r <= ROT_B;
         rot_center_r <= ROT_CENTER;
      end
   end
   
   always @(posedge clk) begin
      if (io_write_enable) begin
         if (io_addr == LED_ADDR) LED <= io_write_data[7:0];
      end
      else begin
         case (io_addr)
           BTN_ADDR: io_read_data <= {4'h0, btn_north_r, btn_south_r, btn_east_r, btn_west_r};
           SW_ADDR: io_read_data <= {4'h0, sw_r};
           ROTSW_ADDR: io_read_data <= {5'h0, rot_center_r, rot_a_r, rot_b_r};
           LED_ADDR: io_read_data <= {LED};
           default: io_read_data <= 8'hx;
         endcase
      end
   end
endmodule
