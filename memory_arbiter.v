`timescale 1ns / 1ps

module memory_arbiter(/*AUTOARG*/
  // Outputs
  cs_n, rd_n, wr_n, addr, exec0_n, exec1_n,
  // Inputs
  clk100, exec_n, cs0_n, rd0_n, wr0_n, addr0, cs1_n, rd1_n, wr1_n,
  addr1
  );

  input clk100;
   
  output cs_n;
  output rd_n;
  output wr_n;
  input exec_n;
  output [21:0] addr;

  input cs0_n;
  input rd0_n;
  input wr0_n;
  output exec0_n;
  input [21:0] addr0;

  input cs1_n;
  input rd1_n;
  input wr1_n;
  output exec1_n;
  input [21:0] addr1;

  reg [1:0] state = 2'b00;
  
  always @(posedge clk100) begin
    case (state)
      2'b00: begin
        if (!cs0_n) begin
          if ((!rd0_n || !wr0_n) && exec_n) begin
            state <= 2'b01;
          end
        end
        else if (!cs1_n && (!rd1_n || !wr1_n) && exec_n) begin
          state <= 2'b10;
        end
      end
      2'b01: begin
        if (rd0_n && wr0_n) begin
          state <= 2'b00;
        end
      end
      2'b10: begin
        if (rd1_n && wr1_n) begin
          state <= 2'b00;
        end
      end
      default: begin
        state <= 2'b00;
      end
    endcase
  end

  assign cs_n = state[0] ? cs0_n :
                state[1] ? cs1_n :
                1'b1;
  assign rd_n = state[0] ? rd0_n :
                state[1] ? rd1_n :
                1'b1;
  assign wr_n = state[0] ? wr0_n :
                state[1] ? wr1_n :
                1'b1;
  assign addr = state[0] ? addr0 :
                state[1] ? addr1 :
                'b1;
  assign exec0_n = state[0] ? exec_n : 1'b1;
  assign exec1_n = state[1] ? exec_n : 1'b1;
endmodule
 
