`timescale 1ns/1ps

module single_port_ram(/*AUTOARG*/
   // Outputs
   read_data,
   // Inputs
   clk, write_enable, addr, write_data
   );
   parameter ADDR_BITS = 4;
   parameter DATA_BITS = 8;
   parameter [256*8:1] DATAFILE = 2048'b0;
   
   input clk;
   input write_enable;
   input [ADDR_BITS-1:0] addr;
   input [DATA_BITS-1:0] write_data;
   output reg [DATA_BITS-1:0] read_data;

   reg [DATA_BITS-1:0] mem [0:(2**ADDR_BITS)-1];

   integer i;
   generate
      if (DATAFILE[8:1] == 8'h0) begin
         initial begin
            for (i = 0; i < 2**ADDR_BITS; i = i + 1) mem[i] = 0;
         end
      end
      else begin
         initial begin
            $readmemh(DATAFILE, mem, 0, (2**ADDR_BITS)-1);
         end
      end
   endgenerate

   always @(posedge clk) begin
      if (write_enable) mem[addr] <= write_data;
      read_data <= mem[addr];
   end
endmodule
 
