;; -*- coding: utf-8; mode: scheme -*-
;;
;; sdcard.scm - 
;;
;;   Copyright (c) 2010 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(macro (hex->char rd rs)
  `(begin
     (sub $at ,rs 10)
     (bltzl $zero $at :under10)
     (add ,rd ,rs #x30)
     (add ,rd ,rd 7)
     :under10))

(macro (wait-data addr)
  `(begin
     :loop
     (lw $at $zero ,addr)
     (nop)
     (beq $at $zero :loop)
     (nop)))

(macro (pause btn)
  (let ((v (cdr (assq btn '((north . 8) (south . 4) (east . 2) (west . 1))))))
    `(begin
       (repeat (!= $at 0)
         (lw $at $zero #xff00)
         (nop))
       (repeat (= $at 0)
         (lw $at $zero #xff00)
         (nop)
         (and $at $at ,v)))))


(macro (serial:set-bps bps . args)
  (let-optionals* args ((addr #xfd00))
    (let ((v (cdr (assv bps
                        '((50 . #x00) (75 . #x01) (110 . #x02) (150 . #x03)
                          (300 . #x04) (600 . #x05) (1200 . #x06) (1800 . #x07)
                          (2000 . #x08) (2400 . #x09) (3600 . #x0a) (4800 . #x0b)
                          (7200 . #x0c) (9600 . #x0d) (19200 . #x0e) (38400 . #x0f)
                          (57600 . #x10) (76800 . #x11) (115200 . #x12))))))
      `(begin
         (mov $at ,v)
         (sw $at $zero ,(+ addr 1))))))

(macro (serial:read-byte reg . args)
  (let-optionals* args ((addr #xfd00))
    `(begin
       (repeat (= ,reg 0)
         (lw ,reg $zero ,(+ addr 2))
         (nop))
       (lw ,reg ,addr)
       (nop))))

(macro (serial:write-byte v . args)
  `(serial:write-byte-list (,v) ,@args))

(macro (serial:write-byte-list lst . args)
  (let-optionals* args ((addr #xfd00))
    `(begin
       (repeat (< 0 $at)
        (lw $at $zero ,(+ addr 3))
        (nop)
        (sub $at $at ,(- 15 (length lst))))
       ,@(map (lambda (v)
                (if (symbol? v)
                    `(sw ,v $zero ,addr)
                    `(begin
                       (mov $at ,v)
                       (sw $at $zero ,addr))))
              lst))))

(macro (sdcard:enable)
  `(begin
     (sw $zero $zero #xf00f)))

(macro (sdcard:disable)
 `(begin
    (mov $at 1)
    (sw $at $zero #xf00f)))

(macro (sdcard:cmd cmd arg)
  `(begin
     (mov $at ,cmd)
     (sw $at $zero #xf000)
     (debug-led $at)
     (mov $at ,arg)
     (sw $at $zero #xf001)))

(macro (sdcard:wait)
  `(repeat (!= $at 0)
     (lw $at $zero #xf010)
     (nop)
     (debug-led $at)))

(macro (sdcard:wait-finish treg us)
  `(begin
     (timeout-loop (,us ,treg 5)
       (lw $at $zero #xf010)
       (nop)
       (branch-if (= $at 0) :exit)
       (nop))
     :exit))

(main-code
 (mov $sp #xfff)

 (serial:set-bps 9600)
 (loop
  (:serial:print "Insert SD-CARD, then press south button.\r\n")
  (pause south)

  ;; read MBR
  (:sdcard:read 0)

  ;; read BPB
  (lw $t0 $zero #xf171)
  (nop)
  (srl $t1 $t0 16)
  (lw $t0 $zero #xf172)
  (nop)
  (sll $t0 $t0 16)
  (add $t1 $t1 $t0) ;; $t1 <= BPB sector
  (:sdcard:read $t1)
  (:serial:print-dump #xf100 #x80)
  )

 ;; sdcard:init
 ;; return: $v0 (last R1 response)
 ;; return: $v1 (last command)
 (defun :sdcard:init () (%t0 %t1 %acmd41 %param %v2?)
   ;; send dummy clock
   (sdcard:disable)
   (usleep 1000)

   ;; CMD0
   (mov $v1 #x4a000100)
   (sdcard:cmd $v1 0)
   (sdcard:enable)
   (sdcard:wait-finish %t0 1000000)
   (sdcard:disable)
   (lw $v0 $zero #xf011)
   (nop)
   (ret-unless (= $v0 #x01))

   ;; CMD8
   (mov $v1 #x43000308)
   (sdcard:cmd $v1 #x000001aa)
   (sdcard:enable)
   (sdcard:wait-finish %t0 1000000)
   (sdcard:disable)
   (lw $v0 $zero #xf011)
   (nop)
   (if (= $v0 #x01)
       ;; version 2.00 or later if CMD8 is accepted
       (mov %v2? 1)
       (if (= $v0 #x05)
           ;; version 1.x if CMD8 is rejected
           (mov %v2? 0)
           (ret)))

   ;; ACMD41 for version 1.x
   (mov %acmd41 #x72000129)
   (mov %param #x0)
   (unless (= %v2? 0)
     ;; ACMD41 for version 2.00 or later
     (mov %acmd41 #x3b000129)
     (mov %param #x40000000))
   (repeat (= $v0 #x01)
     ;; CMD55
     (mov $v1 #x32000137)
     (sdcard:cmd $v1 0)
     (sdcard:enable)
     (sdcard:wait-finish %t0 1000000)
     (lw $v0 $zero #xf011)
     (nop)
     (ret-unless (= $v0 #x01))
     ;; ACMD41
     (mov $v1 %acmd41)
     (sdcard:cmd %acmd41 %param)
     (sdcard:enable)
     (sdcard:wait-finish %t0 1000000)
     (sdcard:disable)
     (lw $v0 $zero #xf011)
     (nop)
     (when (= $v0 #x01)
       (usleep 1000000)))
   (ret-unless (= $v0 0))

   ;; get current disk ID. [-> %t1]
   (lw %t1 $zero #xf020)
   ;; determine the sector-to-address coffecient. (x1 or x512) [-> %t0]
   (if %v2?
       (begin
         ;; CMD58 (version 2.00 or later) to check CCS bit
         (mov $v1 #x7e00033a)
         (sdcard:cmd $v1 0)
         (sdcard:enable)   
         (sdcard:wait-finish %t0 1000000)
         (sdcard:disable)
         (lw $v0 $zero #xf011)
         (lw %t0 $zero #xf013)
         (ret-unless (= $v0 0))
         (nop)
         ;; check CCS
         (srl %t0 %t0 30)
         (and %t0 %t0 1)
         (if  (= %t0 0)
           ;; address unit is byte if CCS = 0
           (mov %t0 9)
           ;; address unit is 512 bytes block if CCS = 1
           (mov %t0 0)))
       ;; address unit is byte when SDSD
       (mov %t0 9))
   (sll %t1 %t1 8)
   (add %t0 %t0 %t1)
   (sw %t0 $zero :sdcard:sector->addr-shift)
   ;; CMD16
   (mov $v1 #x7f000110)
   (sdcard:cmd $v1 #x200)
   (sdcard:enable)
   (sdcard:wait-finish %t0 1000000)
   (sdcard:disable)
   (lw $v0 $zero #xf011))
 
 ;; sdcard:read
 ;; return: $v0 (last R1 response)
 ;; return: $v1 (last command)
 (defun :sdcard:read (sector) (%t0 %t1 %addr)
   ;; check whether the SD Card is changed or not.
   (lw %t0 $zero :sdcard:sector->addr-shift)
   (lw %t1 $zero #xf020)
   (srl %t0 %t0 8)
   (unless (= %t0 %t1)
     ;; The SD Card is changed.
     (:sdcard:init)
     (ret-unless (= $v0 0)))
   (lw %t0 $zero :sdcard:sector->addr-shift)
   (nop)
   ;; calc address
   (sll %addr sector %t0)
   ;; CMD17
   (mov $v1 #x7f000611)
   (sdcard:cmd $v1 %addr)
   (sdcard:enable)
   (sdcard:wait-finish %t0 1000000)
   (sdcard:disable)
   (lw $v0 $zero #xf011))

 ;; sdcard:write
 ;; return: $v0 (last R1 response)
 ;; return: $v1 (last command)
 (defun :sdcard:write (sector) (%t0 %t1 %addr)
   ;; check whether the SD Card is changed or not.
   (lw %t0 $zero :sdcard:sector->addr-shift)
   (lw %t1 $zero #xf020)
   (srl %t0 %t0 8)
   (unless (= %t0 %t1)
     ;; The SD Card is changed.
     (:sdcard:init)
     (ret-unless (= $v0 0)))
   (lw %t0 $zero :sdcard:sector->addr-shift)
   (nop)
   ;; calc address
   (sll %addr sector %t0)
   ;; CMD24
   (mov $v1 #x7f000718)
   (sdcard:cmd $v1 %t0)
   (sdcard:enable)
   (sdcard:wait-finish %t0 1000000)
   (sdcard:disable)
   (lw $v0 $zero #xf011))

 :sdcard:sector->addr-shift
 (@data #xffffffff)

 ;; serial:print
 (defun :serial:print (msg-addr) (%bulk-char %char %shift)
   (loop 
    (lw %bulk-char $zero msg-addr)
    (inc! msg-addr)
    (mov %shift 24)
    (repeat (<= 0 %shift)
      (byte-ref %char %bulk-char %shift)
      (ret-if (= %char 0))
      (serial:write-byte %char)
      (dec! %shift 8))))

 ;; serial:print-hex
 (defun :serial:print-hex (value len) (%val %shift)
   (sub %shift len 1)
   (sll %shift %shift 2)
   (repeat (<= 0 %shift)
     (srl %val value %shift)
     (and %val %val #xf)
     (hex->char %val %val)
     (sub %shift %shift 4)
     (serial:write-byte %val)))

 ;; serial:print-dump
 (defun :serial:print-dump (addr word-len) (%val %word %pos)
   (mov %pos 0)
   (while (!= word-len %pos)
     (and %val addr #x3)
     (if (= %val 0)
         (begin
           (:serial:print "\r\n")
           (:serial:print-hex addr 8)
           (:serial:print ": "))
         (:serial:print "  "))
     (lw %word $zero addr)
     (inc! addr)
     (inc! %pos)
     (byte-ref %val %word 0)
     (:serial:print-hex %val 2)
     (:serial:print " ")
     (byte-ref %val %word 8)
     (:serial:print-hex %val 2)
     (:serial:print " ")
     (byte-ref %val %word 16)
     (:serial:print-hex %val 2)
     (:serial:print " ")
     (byte-ref %val %word 24)
     (:serial:print-hex %val 2))
   (:serial:print "\r\n"))   
     
 ;; print
 (defun :print (msg-addr pos-addr) (%shift %bulk-char %char %attr)
   (loop 
    (lw %bulk-char $zero msg-addr)
    (inc! msg-addr)
    (mov %shift 24)
    (repeat (<= 0 %shift)
      (srl %char %bulk-char %shift)
      (and %char %char #xff)
      (ret-if (= %char 0))
      (lw %attr $zero pos-addr)
      (sub %shift %shift 8)
      (and %attr %attr #xff00)
      (or %char %attr %char)
      (sw %char $zero pos-addr)
      (inc! pos-addr))))

 ;; print-hex
 (defun :print-hex (value len pos-addr) (%val %shift %attr-mask)
   (sub %shift len 1)
   (sll %shift %shift 2)
   (repeat (<= 0 %shift)
     (srl %val value %shift)
     (and %val %val #xf)
     (hex->char %val %val)
     (lw %attr-mask $zero pos-addr)
     (sub %shift %shift 4)
     (and %attr-mask %attr-mask #xff00)
     (or %val %attr-mask %val)
     (sw %val $zero pos-addr)
     (inc! pos-addr)))

 ;; (@org #xf80)
 ;; (begin
 ;;   (mov $t1 #xf80) ; 4096 - 128 word
 ;;   :loop
 ;;   (mread $a0)
 ;;   (inc! $a0 4)
 ;;   (mfetch $t0)
 ;;   (sw $t0 $zero $a1)
 ;;   (bne $a1 $t1 :loop)
 ;;   (inc! $a1)
 ;;   (j 0)
 ;;   (nop))
 )
;; end of file
