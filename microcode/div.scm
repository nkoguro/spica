;; -*- coding: utf-8; mode: scheme -*-
;;
;; div.scm - 
;;
;;   Copyright (c) 2010 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(macro (hex->char rd rs)
  `(begin
     (sub $at ,rs 10)
     (bltzl $zero $at :under10)
     (add ,rd ,rs #x30)
     (add ,rd ,rd 7)
     :under10))

(macro (dec->char rd rs)
  `(add ,rd ,rs #x30))

(main-code
 (mov $sp #xfff)

 (:div 234 11)
 (mov $t0 $v0)
 (mov $t1 $v1)
 (:print-udec $t0 #xfe00)
 (:print-udec $t1 #xfe10)
 (halt)

 ;; mul
 ;; return: $v0 (mul)
 (defun :mul (m n) (%t)
   (branch-if (< n m) :dont-swap)
   (mov $v0 m)
   (mov m n)
   (mov n $v0)
   :dont-swap
   (beq n $zero :exit)
   (mov $v0 0)
   :loop
   (and %t n 1)
   (beq %t $zero :skip)
   (srl n n 1)
   (add $v0 $v0 m)
   :skip
   (bne n $zero :loop)
   (sll m m 1)
   :exit)

 ;; div
 ;; return: $v0 (quotient)
 ;; return: $v1 (remainer)
 (defun :div (m n) (%t %shift)
   (with-reg-alias ((q $v0) (r $v1))
     (mov q 0)
     (mov r m)
     (mov %shift 31)
     (srl %t r %shift)
     :loop
     (sub %t %t n)
     (bgez %t :q1)
     (sll q q 1)
     (beq %shift $zero :exit)
     (sub %shift %shift 1)
     (j :loop)
     (srl %t r %shift)
     :q1
     (sll %t n %shift)
     (sub r r %t)
     (or q q 1)
     (beq %shift $zero :exit)
     (sub %shift %shift 1)
     (j :loop)
     (srl %t r %shift)
     :exit))
 
 ;; print-udec
 ;; return: $v0 (next pos-addr)
 (defun :print-udec (value pos-addr) (%data %len %attr-mask)
   (mov %len 0)
   (repeat (!= value 0)
     (:div value 10)
     (mov value $v0)
     (dec->char $v1 $v1)
     (push $v1)
     (inc! %len))
   (while (< 0 %len)
     (lw %attr-mask $zero pos-addr)
     (pop value)
     (and %attr-mask %attr-mask #xff00)
     (or %data %attr-mask value)
     (sw %data $zero pos-addr)
     (inc! pos-addr)
     (dec! %len))
   (mov $v0 pos-addr))
     
 ;; print-hex
 ;; return: $v0 (last pos-addr)
 (defun :print-hex (value len pos-addr) (%val %shift %attr-mask)
   (sub %shift len 1)
   (sll %shift %shift 2)
   (while (<= 0 %shift)
     (srl %val value %shift)
     (and %val %val #xf)
     (hex->char %val %val)
     (lw %attr-mask $zero pos-addr)
     (sub %shift %shift 4)
     (and %attr-mask %attr-mask #xff00)
     (or %val %attr-mask %val)
     (sw %val $zero pos-addr)
     (inc! pos-addr))
   (mov $v0 pos-addr))
 )

;; end of file
