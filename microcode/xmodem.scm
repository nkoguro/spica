;; -*- coding: utf-8; mode: scheme -*-
;;
;; xmodem.scm - 
;;
;;   Copyright (c) 2010 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(use srfi-1)

(macro (hex->char rd rs)
  `(begin
     (sub $at ,rs 10)
     (bltzl $zero $at :under10)
     (add ,rd ,rs #x30)
     (add ,rd ,rd 7)
     :under10))

(macro (wait-data)
  `(begin
     :loop
     (lw $at $zero #xfd02)
     (nop)
     (beq $at $zero :loop)
     (nop)))

(macro (funcall func . args)
  (when (<= 4 (length args))
    (errorf "Too many args: ~s @ ~8,'0x" args func))
  `(begin
     ,@(map (lambda (i arg)
              `(mov ,(string->symbol (format "$a~d" i)) ,arg))
            (iota 4)
            args)
     (jal $ra ,func)
     (nop)))

(macro (ret . opts)
  (let-optionals* opts ((r '$ra))
    `(begin
       (j ,r)
       (nop))))

(main-code
 (@equ :SOH #x01)
 (@equ :EOT #x04)
 (@equ :ACK #x06)
 (@equ :NAK #x15)
 
 (mov $sp #xfff)

 ;; set 9600bps
 (mov $t0 #xd)
 (sw $t0 $zero #xfd01)

 (funcall :clear-screen)

 (funcall :print :start-message0 #xfe00)
 (funcall :print :start-message1 #xfe10)

 (begin
   :loop
   (lw $t0 $zero #xff00)
   (mov $t1 #b0100)
   (bne $t0 $t1 :loop)
   (nop))

 (funcall :print :clear-message #xfe00)
 (funcall :print :clear-message #xfe10)

 (funcall :download-xmodem #xf00000)
 (halt)
 
 ;; download with XMODEM
 ;; input: $a0 (addr)
 :download-xmodem
 (let-temp (%addr %nak-or-ack %i %checksum %data %tmp)
   (mov %addr $a0)
   
   (mov %nak-or-ack :NAK)
   :nak-or-ack
   (save-reg (%addr %nak-or-ack)
     (funcall :print :waiting-soh #xfe00))
   (sw %nak-or-ack $zero #xfd00)
   (debug-led %nak-or-ack)
   (mov %i 50000000)
   (begin
     :wait-loop
     (beq %i $zero :nak-or-ack)
     (lw %data $zero #xfd02)
     (sub %i %i 1)
     (beq %data $zero :wait-loop)
     (debug-led %data)
     (lw %data $zero #xfd00)
     (mov %tmp :EOT)
     (beq %data %tmp :end-of-transmission)
     (mov %tmp :SOH)
     (beq %data %tmp :start-of-heading)
     (nop)
     (save-reg (%addr)
       (funcall :print-hex %data 2 #xfe1a)
       (funcall :print :bad-control #xfe10))
     (ret))

   :start-of-heading
   (save-reg (%addr)
     (funcall :print :receiving #xfe00)
     (funcall :print :clear-message #xfe10))
   ;; Receive BN
   (wait-data)
   (lw %data $zero #xfd00)
   (nop)
   (save-reg (%addr)
     (funcall :print-hex %data 2 #xfe07)
     (funcall :print :block #xfe00))

   ;; Receive BNC
   (wait-data)
   (lw %data $zero #xfd00)

   ;; Receive data
   (mov %checksum 0)
   (mov %i 32)
   (begin
     :loop
     (wait-data)
     (lw %data $zero #xfd00)
     (nop)
     (add %checksum %checksum %data)
     (add %tmp $zero %data)
     (debug-led %data)

     (wait-data)
     (lw %data $zero #xfd00)
     (nop)
     (add %checksum %checksum %data)
     (sll %data %data 8)
     (add %tmp %tmp %data)
     (debug-led %data)
     
     (wait-data)
     (lw %data $zero #xfd00)
     (nop)
     (add %checksum %checksum %data)
     (sll %data %data 16)
     (add %tmp %tmp %data)
     (debug-led %data)
     
     (wait-data)
     (lw %data $zero #xfd00)
     (nop)
     (add %checksum %checksum %data)
     (sll %data %data 24)
     (add %tmp %tmp %data)
     (debug-led %data)

     (mwrite %tmp %addr)
     (add %addr %addr 4)
     (sub %i %i 1)
     (msync)
     (bne %i $zero :loop))

   (wait-data)
   (lw %data $zero #xfd00)
   (and %checksum %checksum #xff)
   (beq %data %checksum :nak-or-ack)
   (mov %nak-or-ack :ACK)
   
   (save-reg (%addr)
     (funcall :print :checksum-error #xfe00))
   (mov %nak-or-ack :NAK)
   (sub %addr %addr 128)
   (j :nak-or-ack)
   (nop)
   
   :end-of-transmission
   (mov %nak-or-ack :ACK)
   (sw %nak-or-ack $zero #xfd00)
   (funcall :print :clear-message #xfe00)
   (funcall :print :clear-message #xfe10)
   (funcall :print :completed #xfe00)
   (ret))
   
 ;; clear graphic screen
 :clear-screen
 (let-temp (%addr %end-addr)
   (mov %addr #xf00000)
   (mov %end-addr #xf4b000)
   :loop
   (beq %addr %end-addr :exit)
   (nop)
   (mwrite $zero %addr)
   (add %addr %addr 4)
   (msync)
   (j :loop)
   (nop)
   :exit
   (ret)
   )

  ;; print
  ;; input: $a0 (msg addr)
  ;; input: $a1 (position addr)
  :print
  (begin
    (mov $t0 0) ; $t0: msg index
    (mov $t3 0) ; $t3: pos delta
    :loop-word
    (lw $t1 $a0 $t0)
    (add $t0 $t0 1)
    (mov $t5 24) ; $t5: shift
    :loop-byte
    (srl $t2 $t1 $t5)
    (and $t2 $t2 #xff)
    (beq $t2 $zero :exit)
    (lw $t4 $a1 $t3)
    (sub $t5 $t5 8)
    (and $t4 $t4 #xff00)
    (or $t2 $t4 $t2)
    (sw $t2 $a1 $t3)
    (bgezl $zero $t5 :loop-byte)
    (add $t3 $t3 1)
    (j :loop-word)
    (nop)
    :exit
    (ret))

  ;; print-hex
  ;; input: $a0 (value)
  ;; input: $a1 (len)
  ;; input: $a2 (position addr)
  :print-hex
  (let-temp (%pos-delta %val %shift %attr-mask)
    (mov %pos-delta 0)
    (sub %shift $a1 1)
    (sll %shift %shift 2)
    :loop
    (srl %val $a0 %shift)
    (and %val %val #xf)
    (hex->char %val %val)
    (lw %attr-mask $a2 %pos-delta)
    (sub %shift %shift 4)
    (and %attr-mask %attr-mask #xff00)
    (or %val %attr-mask %val)
    (sw %val $a2 %pos-delta)
    (bgezl $zero %shift :loop)
    (add %pos-delta %pos-delta 1))
  (ret)

  :start-message0
  (@string "Push SOUTH Btn  ")
  :start-message1
  (@string "to download     ")
  :clear-message
  (@string "                ") 

  :waiting-soh
  (@string "Waiting SOH...") 

  :bad-control
  (@string "Bad ctrl: ")

  :receiving
  (@string "Receiving...    ") 

  :block
  (@string "Block: ")

  :checksum-error
  (@string "Checksum Error  ")

  :completed
  (@string "Completed")
 )
;; end of file
