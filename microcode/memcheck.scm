;; -*- coding: utf-8; mode: scheme -*-
;;
;; memcheck.scm - Memory check
;;
;;   Copyright (c) 2010 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(macro (hexchar rd rs)
  `(begin
     (sub $at ,rs 10)
     (bltzl $zero $at :under10)
     (add ,rd ,rs #x30)
     (add ,rd ,rd 7)
     :under10))

(macro (printhex rs addr)
  `(begin
     (mov $t0 ,addr)
     (srl $t1 ,rs 28)
     (hexchar $t1 $t1)
     (sw $t1 $t0 0)
     (srl $t1 ,rs 24)
     (and $t1 $t1 #xf)
     (hexchar $t1 $t1)
     (sw $t1 $t0 1)
     (srl $t1 ,rs 20)
     (and $t1 $t1 #xf)
     (hexchar $t1 $t1)
     (sw $t1 $t0 2)
     (srl $t1 ,rs 16)
     (and $t1 $t1 #xf)
     (hexchar $t1 $t1)
     (sw $t1 $t0 3)
     (srl $t1 ,rs 12)
     (and $t1 $t1 #xf)
     (hexchar $t1 $t1)
     (sw $t1 $t0 4)
     (srl $t1 ,rs 8)
     (and $t1 $t1 #xf)
     (hexchar $t1 $t1)
     (sw $t1 $t0 5)
     (srl $t1 ,rs 4)
     (and $t1 $t1 #xf)
     (hexchar $t1 $t1)
     (sw $t1 $t0 6)
     (and $t1 ,rs #xf)
     (hexchar $t1 $t1)
     (sw $t1 $t0 7)))

(macro (debug-led v)
  `(begin
     (mov $at ,v)
     (sw $at $zero #xff80)))

(main-code
 (check-point
  (print "test new op"))

 (check-point
  (print "test adh"))
 ;; test 1
 (mov $t0 #x00123456)
 (adh $t1 $t0 #x3f01)
 (mov $t2 #x01123456)
 (bne $t1 $t2 :fail-op)
 (mov $t3 #x01)

 ;; test 2
 (mov $t0 #x01123456)
 (adh $t1 $t0 #x3fff)
 (mov $t2 #x00123456)
 (bne $t1 $t2 :fail-op)
 (mov $t3 #x02)

 ;; test 3
 (mov $t0 #x3f123456)
 (adh $t1 $t0 #x3f01)
 (mov $t2 #x3f123456)
 (bne $t1 $t2 :fail-op)
 (mov $t3 #x03)

 ;; test 4
 (mov $t0 #x3f123456)
 (adh $t1 $t0 #x3fff)
 (mov $t2 #x3f123456)
 (bne $t1 $t2 :fail-op)
 (mov $t3 #x04)
 
 (check-point
  (print "test btt"))
 ;; test 5
 (begin
   (mov $t0 #xBFFFFFFF)
   (mov $t1 #xC080)
   (btt $t1 $t0 :success)
   (nop)
   (j :fail-op)
   (mov $t3 #x05)
   :success)

 (check-point
  (print "test bnt"))
 ;; test 6
 (begin
   (mov $t0 #xc0000030)
   (mov $t1 #xC080)
   (bnt $t1 $t0 :success)
   (nop)
   (j :fail-op)
   (mov $t3 #x06)
   :success)
 
 (mov $s0 2463534242)  ;; $s0 is a random value.
 (mov $s4 16)          ;; loop count

 :main_loop
 (check-point
  (print "start main_loop")
  (format #t "loop count=~d~%" (reg '$s4)))
 (mov $s1 0)           ;; $s1 is a check address in DDR2-SDRAM.
 
 :check
 (mov $s2 :workarea)
 (mov $s3 #x100)
 (printhex $s1 #xfe10)
 :loop_make_random_data
 ;; (printhex $s1 #xfe10)
 (jal $ra :next_rand)
 (nop)
 (mswr $s0 $s1 $zero)
 (sw $s0 $s2)
 (add $s1 $s1 1)
 (add $s2 $s2 1)
 (sub $s3 $s3 1)
 (msync)
 (bne $s3 $zero :loop_make_random_data)
 (nop)
 (sub $s1 $s1 #x100)
 (mov $s2 :workarea)
 (mov $s3 #x100)
 :loop_check_page
 ;; (printhex $s1 #xfe18)
 (mlwr $zero $s1 $zero)
 (lw $a0 $s2)
 (mfetch $a1)
 (bne $a0 $a1 :failed)
 (sub $s3 $s3 1)
 (add $s1 $s1 1)
 (add $s2 $s2 1)
 (bne $s3 $zero :loop_check_page)
 (mov $t0 #x400000)
 (check-point
  (format #t "SDRAM check: ~x done~%" (reg '$s1)))
 (bne $s1 $t0 :check)
 (nop)
 
 (sub $s4 $s4 1)
 (bne $s4 $zero :main_loop)
 (nop)
 (j :success)
 (nop)
 
 :next_rand
 (sll $t0 $s0 13)
 (xor $s0 $s0 $t0)
 (srl $t0 $s0 17)
 (xor $s0 $s0 $t0)
 (sll $t0 $s0 5)
 (xor $s0 $s0 $t0)
 (j $ra)
 (nop)
 
 :success
 (mov $t0 #xfe00)
 (mov $t1 #x43)    ;; 'C'
 (sw $t1 $t0 0)
 (mov $t1 #x6f)    ;; 'o'
 (sw $t1 $t0 1)
 (mov $t1 #x6e)    ;; 'n'
 (sw $t1 $t0 2)
 (mov $t1 #x67)    ;; 'g'
 (sw $t1 $t0 3)
 (mov $t1 #x72)    ;; 'r'
 (sw $t1 $t0 4)
 (mov $t1 #x61)    ;; 'a'
 (sw $t1 $t0 5)
 (mov $t1 #x74)    ;; 't'
 (sw $t1 $t0 6)
 (mov $t1 #x75)    ;; 'u'
 (sw $t1 $t0 7)
 (mov $t1 #x6c)    ;; 'l'
 (sw $t1 $t0 8)
 (mov $t1 #x61)    ;; 'a'
 (sw $t1 $t0 9)
 (mov $t1 #x74)    ;; 't'
 (sw $t1 $t0 10)
 (mov $t1 #x69)    ;; 'i'
 (sw $t1 $t0 11)
 (mov $t1 #x6f)    ;; 'o'
 (sw $t1 $t0 12)
 (mov $t1 #x6e)    ;; 'n'
 (sw $t1 $t0 13)

 (mov $t1 #xff)
 (mov $t0 #xff80)
 (sw $t1 $t0)
 (halt)
 
 :failed
 (printhex $a0 #xfe00)
 (printhex $a1 #xfe08)
 (printhex $s1 #xfe10)
 (halt)

 :fail-op
 (debug-led $t3)
 (printhex $t1 #xfe00)
 (printhex $t2 #xfe10)
 (halt)

 :workarea
 (@data 0)
 )

;; (asm->verilog
;;  spica_inst

;;  :start
;;  (mov $a0 #xfe00)
;;  (jal $ra :write_hello_world)
;;  (mov $a0 #xe000)
;;  (jal $ra :write_hello_world)
;;  (mov $a0 1)
;;  (mov $a1 #x00ffffff)
;;  :loop
;;  (jal $ra :light_leds)
;;  (sll $a0 $a0 1)
;;  (mov $t0 #x100)
;;  (bne $a0 $t0 :loop)

;;  (mov $a0 #xff)
;;  (mov $a1 1)
;;  (jal $ra :light_leds)

;;  :check_loop
;;  (lw $t0 #xff00)
;;  (sw $t0 #xff80)
;;  (mov $t1 #b100)
;;  (beq $t0 $t1 :start)
;;  (j :check_loop)

;;  :light_leds
;;  (sw $a0 #xff80)
;;  (mov $t0 0)
;;  :wait_loop
;;  (add $t0 $t0 1)
;;  (bne $t0 $a1 :wait_loop)
;;  (j $ra)

;;  :write_hello_world
;;  (mov $t0 #x48)   ; 'H'
;;  (sw $t0 $a0)
;;  (mov $t0 #x65)   ; 'e'
;;  (add $a0 $a0 1)
;;  (sw $t0 $a0)
;;  (mov $t0 #x6c)   ; 'l'
;;  (add $a0 $a0 1)
;;  (sw $t0 $a0)
;;  (add $a0 $a0 1)
;;  (mov $t0 #x6c)   ; 'l'
;;  (sw $t0 $a0)
;;  (add $a0 $a0 1)
;;  (mov $t0 #x6f)   ; 'o'
;;  (sw $t0 $a0)
;;  (add $a0 $a0 1)
;;  (mov $t0 #x2c)   ; ','
;;  (sw $t0 $a0)
;;  (add $a0 $a0 1)
;;  (mov $t0 #x20)   ; ' '
;;  (sw $t0 $a0)
;;  (add $a0 $a0 1)
;;  (mov $t0 #x77)   ; 'w'
;;  (sw $t0 $a0)
;;  (add $a0 $a0 1)
;;  (mov $t0 #x6f)   ; 'o'
;;  (sw $t0 $a0)
;;  (add $a0 $a0 1)
;;  (mov $t0 #x72)   ; 'r'
;;  (sw $t0 $a0)
;;  (add $a0 $a0 1)
;;  (mov $t0 #x6c)   ; 'l'
;;  (sw $t0 $a0)
;;  (add $a0 $a0 1)
;;  (mov $t0 #x64)   ; 'd'
;;  (sw $t0 $a0)
;;  (add $a0 $a0 1)
;;  (mov $t0 #x21)   ; '!'
;;  (sw $t0 $a0)
;;  (j $ra)
;;  )
              
;; end of file
