;; -*- coding: utf-8; mode: scheme -*-
;;
;; spica.scm - Spica
;;
;;   Copyright (c) 2010 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(use srfi-1)

(register-alias $vmpc $k0)
(register-alias $vmsp $k2)

;; TODO: change reference counter
(defmacro vmpush (reg)
  `(msw ,reg $vmsp 0
        (add $vmsp $vmsp 1)))

;; TODO: change reference counter
(defmacro vmpop (reg)
  `(begin
     (mlw ,reg $vmsp -1
          (sub $vmsp $vmsp 1))))
  
(lib-code
 (mov $vmpc 0)
 
 :fetch-mem
 (let-temp (%inst)
   (mlw $code $zero $vmpc
        (add $vmpc $vmpc 1)
        (begin
          (and %inst $code #x3f)
          (lw %inst %inst :inst-table)
          (srl $code $code 8)))
   (j %inst)
   (nop))

 :fetch-reg
 (let-temp (%inst)
   (and %inst $code #x3f)
   (lw %inst %inst :inst-table)
   (srl $code $code 8)
   (j %inst)
   (nop))

 ;; TODO: reimplement
 :inst-halt
 (funcall :print :msg-halted #xfe00)
 (funcall :print :msg-value0 #xfe10)
 (funcall :print-hex $value0 8 #xfe18)
 (halt)

 :inst-reset
 (mread $vmpc)
 (sll $vmpc $a0 2)
 (sw $vmpc $zero :codebase)
 (mov $vmsp #xeffffc)
 (mov $vmfp $vmsp)
 (mfetch $t0)
 (fixnum->int $free $t0)
 (next)

 :inst-refer-local
 (index* $vmfp $a0)
 (mov $valnum 1)
 (mfetch $value0)
 (next)

 :inst-refer-free
 (index-closure* $closure $a0)
 (mov $valnum 1)
 (mfetch $value0)
 (next)

 :inst-refer-global
 (with-temp-reg (%false)
   (lw $a1 $zero :global-binding)
   (arg->obj $a0 $a0)
   (funcall :assq $a0 $a1)
   (mov %false :obj-false)
   (beq $v0 %false :error)
   (car $value0 $v0)
   (mov $valnum 1)
   (next)
   :error
   (panic :errmsg-global-binding-not-found))

 :inst-indirect
 (let-temp (%value0 %valnum)
   (lw %value0 $zero :value0)
   (mov %valnum 1)
   (mlw %value0 $zero %value0
        (sw %valnum $zero :valnum)
        (sw %value0 $zero :value0)))
 (j :fetch-reg)
 (nop)
   
 :inst-constant
 (let-temp (%value0 %valnum)
   (mlw %value0 $vmpc 0
        (begin
          (add $vmpc $vmpc 1)
          (mov %valnum 1)
          (sw %valnum $zero :valnum))
        (sw %value0 $zero :value0)))
 (j :fetch-reg)
 (nop)

 :inst-close
 (mread $vmpc)
 (add $vmpc $vmpc 4)
 (mov $valnum 1)
 (mfetch $a1) ; $a1 <- closure body
 (funcall :closure $a0 $a1)
 (mov $value0 $v0)
 (sll $t0 $a0 2)
 (add $vmsp $vmsp $t0)
 (next)

 :inst-box
 (index* $vmsp $a0)
 (mov $t6 $a0)
 (mfetch $a0)
 (funcall :box $a0)
 (index-set!* $vmsp $t6 $v0)
 (msync)
 (next)

 :inst-branch-false
 (begin
   (mov $t0 #b00000000000000000000000000001011)
   (bne $value0 $t0 :skip)
   (sll $a0 $a0 2)
   (add $vmpc $vmpc $a0)
   :skip
   )
 (next)

 :inst-jump
 (sll $a0 $a0 2)
 (add $vmpc $vmpc $a0)
 (next)

 :inst-assign-local
 (index* $vmfp $a0)
 (mfetch $t0)
 (set-box!* $t0 $value0)
 (msync)
 (next)

 :inst-assign-free
 (index* $closure $a0)
 (mfetch $t0)
 (set-box!* $t0 $value0)
 (msync)
 (next)

 :inst-assign-global
 (with-temp-reg (%false %gref)
   (lw $a1 $zero :global-binding)
   (arg->obj $a0 $a0)
   (funcall :assq $a0 $a1)
   (mov %false :obj-false)
   (beq $v0 %false :error)
   (car %gref $v0)
   (set-box!* %gref $value0)
   (mov $valnum 1)
   (msync)
   (next)
   :error
   (panic :errmsg-global-binding-not-found))

 :inst-global-binding
 (with-temp-reg (%false %gref)
   (lw $s1 $zero :global-binding)
   (arg->obj $a0 $a0)
   (funcall :assq $a0 $s1)
   (mov %false :obj-false)
   (bne $v0 %false :exit)
   ;; #<box> <- (box #<unbound>)
   (mov $s2 $a0) ; $s2 <- symbol
   (mov $a0 :obj-unbound)
   (funcall :box $a0)
   ;; #<pair> <- (cons symbol #<box>)
   (funcall :cons $s2 $v0)
   ;; (cons #<pair> global-binding)
   (funcall :cons $v0 $s1)
   (sw $v0 $zero :global-binding)
   :exit
   (next))

 :inst-conti
 ;; save stack
 (with-temp-reg (%len %stackbase %i %src %dst %data)
   (mov %stackbase #xeffffc)
   (sub %len %stackbase $vmsp)
   (byte->word %len %len)
   (funcall :malloc :bytearray %len)
   (mov %i 0)
   (mov %dst $v0)
   (add %src $vmsp 4)
   :loop
   (beq %i %len :exit)
   (add %i %i 1)
   (mread %src)
   (mfetch %data)
   (mwrite %data %dst)
   (add %src %src 4)
   (add %dst %dst 4)
   (msync)
   (j :loop)
   (nop)
   :exit)
 ;; continuation
 (with-temp-reg (%addr %code %template %data)
   (mov %addr $v0)  ; $v0 has a saved stack.
   (funcall :malloc :rawdata 3)
   (mov %template :continuation-code)

   (lw %data %template 0)
   (mov %code $v0)
   (mwrite %data %code)  ; write (refer-local 0)
   (add %code %code 4)
   (msync)

   (lw %data %template 1)
   (sll %addr %addr 8)
   (or %data %addr %data)
   (mwrite %data %code)  ; write (nuate (save-stack))
   (add %code %code 4)
   (msync)

   (lw %data %template 2)
   (nop)
   (mwrite %data %code)  ; write (return 0)
   (msync)

   (funcall :closure 0 $v0)
   (mov $valnum 1)
   (mov $value0 $v0))
 (next)

 :inst-nuate
 (with-temp-reg (%len %i %src %tmp)
   ;; restore stack
   (arg->obj %src $a0)
   (sizeof-word %len $a0)

   (sll %tmp %len 2)
   (sub %tmp %tmp 4)
   (add %src %src %tmp)

   (mov $vmsp #xeffffc)

   (mov %i 0)
   :loop
   (beq %i %len :exit)
   (add %i %i 1)
   (mread %src)
   (sub %src %src 4)
   (mfetch %tmp)
   (vmpush %tmp)
   (j :loop)
   (nop)
   :exit
   )
 (next)

 :inst-frame
 (vmpush $closure)
 (vmpush* $vmfp)
 (lw $t0 $zero :codebase)
 (msync)
 (vmpush* $t0)
 (sll $t0 $a0 2)
 (add $t0 $vmpc $t0)
 (msync)
 (vmpush $t0)
 (next)

 :inst-argument
 (vmpush $value0)
 (next)

 :inst-shift
 (with-temp-reg (%i %j %tmp)
   (srl $a1 $a0 11)
   (and $a1 $a1 #x7ff)
   (and $a0 $a0 #x7ff)

   (sub %i $a0 1)
   :loop
   (bltzl $zero %i :exit)
   (index* $vmsp %i)
   (add %j %i $a1)
   (mfetch %tmp)
   (index-set!* $vmsp %j %tmp)
   (sub %i %i 1)
   (msync)
   (j :loop)
   (nop)
   :exit
   (sll %tmp $a1 2)
   (add $vmsp $vmsp %tmp))
 (next)

 :inst-apply
 (closure-body* $value0)
 (mov $closure $value0)
 (mov $vmfp $vmsp)
 (mfetch $vmpc)
 (sw $vmpc $zero :codebase)
 (next)

 :inst-return
 (sll $t0 $a0 2)
 (add $vmsp $vmsp $t0) ;; (set! *stack-pointer* (+ *stack-pointer* (* n 4)))
 (mov $t1 0)
 (index* $vmsp $t1)
 (add $t1 $t1 1)
 (mfetch $vmpc)        ;; (set! *pc* (index *stack-pointer* 0))
 (index* $vmsp $t1)
 (add $t1 $t1 1)
 (mfetch $t0)
 (sw $t0 $zero :codebase)   ;; (set! *codebase* (index *stack-pointer* 1))
 (index* $vmsp $t1)
 (add $t1 $t1 1)
 (mfetch $vmfp)        ;; (set! *frame-pointer* (index *stack-pointer* 2))
 (index* $vmsp $t1)
 (add $vmsp $vmsp 16)  ;; (set! *stack-pointer-new* (+ *stack-pointer* (* 4 4)))
 (mfetch $closure)     ;; (set! *closure* (index *stack-pointer* 3))
 (next)

 :inst-add
 (vmpop*)
 (fixnum->int $t0 $value0)
 (mov $valnum 1)
 (mfetch $t1)
 (fixnum->int $t1 $t1)
 (add $t0 $t0 $t1)
 (int->fixnum $value0 $t0)
 (next)

 :inst-sub
 (vmpop*)
 (fixnum->int $t0 $value0)
 (mov $valnum 1)
 (mfetch $t1)
 (fixnum->int $t1 $t1)
 (sub $t0 $t0 $t1)
 (int->fixnum $value0 $t0)
 (next)

 :inst-mul
 (vmpop*)
 (fixnum->int $t0 $value0)
 (mov $valnum 1)
 (mov $value0 0)
 (mfetch $t1)
 (fixnum->int $t1 $t1)
 (begin
   :loop
   (beq $t0 $zero :exit)
   (and $t2 $t0 1)
   (beq $t2 $zero :skip)
   (srl $t0 $t0 1)
   (add $value0 $value0 $t1)
   :skip
   (j :loop)
   (sll $t1 $t1 1)
   :exit
   (int->fixnum $value0 $value0)
   )
 (next)

 :inst-eq
 (begin
   (vmpop $t0)
   (beq $value0 $t0 :true)
   (mov $valnum 1)
   (mov $value0 :obj-false)
   (next)
   :true
   (mov $value0 :obj-true)
   (next))

 :inst-cons
 (vmpop*)
 (mov $valnum 1)
 (mfetch $t0)
 (funcall :cons $value0 $t0)
 (mov $value0 $v0)
 (next)

 :inst-car
 (begin
   (type $t0 $value0)
   (mov $t1 :pair)
   (bne $t0 $t1 :error)
   (nop)
   (car $value0 $value0)
   (next)
   :error
   (panic :errmsg-not-pair))

 :inst-cdr
 (begin
   (type $t0 $value0)
   (mov $t1 :pair)
   (bne $t0 $t1 :error)
   (mov $valnum 1)
   (cdr $value0 $value0)
   (next)
   :error
   (panic :errmsg-not-pair))
 
 :inst-set-car!
 (begin
   (vmpop*)
   (mov $t1 :pair)
   (mfetch $a1)
   (type $t0 $value0)
   (bne $t0 $t1 :error)
   (nop)
   (set-car!* $value0 $a1)
   (msync)
   (next)
   :error
   (panic :errmsg-not-pair))

 :inst-set-cdr!
 (begin
   (vmpop*)
   (mov $t1 :pair)
   (mfetch $a1)
   (type $t0 $value0)
   (bne $t0 $t1 :error)
   (nop)
   (set-cdr!* $value0 $a1)
   (msync)
   (next)
   :error
   (panic :errmsg-not-pair))

 
 :inst-table
 (@data :fetch-mem)
 (@data :inst-halt)
 (@data :inst-reset)
 (@data :inst-refer-local)
 (@data :inst-refer-free)
 (@data :inst-refer-global)
 (@data :inst-indirect)
 (@data :inst-constant)
 (@data :inst-close)
 (@data :inst-box)
 (@data :inst-branch-false)
 (@data :inst-jump)
 (@data :inst-assign-local)
 (@data :inst-assign-free)
 (@data :inst-assign-global)
 (@data :inst-global-binding)
 (@data :inst-conti)
 (@data :inst-nuate)
 (@data :inst-frame)
 (@data :inst-argument)
 (@data :inst-shift)
 (@data :inst-apply)
 (@data :inst-return)
 (@data :inst-add)
 (@data :inst-sub)
 (@data :inst-mul)
 (@data :inst-eq)
 (@data :inst-cons)
 (@data :inst-car)
 (@data :inst-cdr)
 (@data :inst-set-car!)
 (@data :inst-set-cdr!)
 
 )








(register-alias $code $s0)
(register-alias $imm $s1)
(register-alias $vmap $s2)
(register-alias $value0 $s4)
(register-alias $value1 $s5)
(register-alias $value2 $s6)
(register-alias $value3 $s7)
(register-alias $valnum $t8)
(register-alias $vmpc $t9)
(register-alias $closure $k0)
(register-alias $vmsp $k1)
(register-alias $free $gp)
(register-alias $vmfp $fp)

(macro (hex->char rd rs)
  `(begin
     (sub $at ,rs 10)
     (bltzl $zero $at :under10)
     (add ,rd ,rs #x30)
     (add ,rd ,rd 7)
     :under10))

(macro (vmpush* reg)
  `(begin
     (mswr ,reg $zero $vmsp)
     (sub $vmsp $vmsp 4)))

(macro (vmpush reg)
  `(begin
     (vmpush* reg)
     (msync)))

(macro (index* sp-reg i-reg)
  `(begin
     :loop
     (lmdr $zero $zero :loop)
     (sll $at ,i-reg 2)
     (mlwr $zero $at ,sp-reg)))

(macro (index-closure* addr-reg n-reg)
  `(begin
     :loop
     (lmdr $zero $zero :loop)
     (sll $at ,n-reg 2)
     (add $at ,addr-reg $at)
     (mlwr $zero $at 4)))

(macro (fetch*)
  '(begin
     (mlwr $zero $zero $vmpc)
     (inc $vmpc 4)))     

(macro (decode-I)
  '(begin
     (j :decode-I)
     (nop)))

(macro (decode-O)
  '(begin
     (and $t0 $imm #x7f)
     (lw $t1 $t0 :inst-table)
     (and $t0 $imm #x80)
     (beq $t0 $zero $t1)
     (sra $imm $imm 8)
     (mswr $value0 $zero $vmsp)
     (j $t1)
     (sub $vmsp $vmsp 4)))

(main-code
 (mov $sp #xfff)
 
 :fetch
 (fetch*)

 :decode-I
 (lmdr $code $zero :decode-I)
 (and $t0 $code #x7f)
 (lw $t1 $t0 :inst-table)
 (and $t0 $code #x80)
 (beq $t0 $zero $t1)
 (sra $imm $code 8)
 (mswr $value0 $zero $vmsp)
 (j $t1)
 (sub $vmsp $vmsp 4)
 
 :inst-halt
 (begin
   (:lcd:print "Halted@         " 0)
   (:lcd:print "value0:         " 16)
   (:lcd:print-hex $vmpc 6 8)
   (:lcd:print-hex $value0 8 8)
   (halt))

 :inst-refer-local
 (begin
   (index* $vmfp $imm)
   :loop
   (lmdr $value0 $zero :loop)
   (mov $valnum 1)
   (fetch*)
   (decode-I))
 
 :inst-refer-free
 (begin
   (index-closure* $closure $imm)
   :loop
   (lmdr $value0 $zero :loop)
   (mov $valnum 1)
   (fetch*)
   (decode-I))
 
 :inst-indirect
 (begin
   (msync)
   (mlwr $zero $zero $value0)
   :loop
   (lmdr $value0 $zero :loop)
   (mov $valnum 1)
   (decode-O))

 :inst-constant
 (begin
   (msync)
   (mlwr $zero $zero $vmpc)
   (add $vmpc $vmpc 4)
   :loop
   (lmdr $value0 $zero :loop)
   (mov $valnum 1)
   (decode-O))

 
 :decode-O
 (decode-O)
 
 ;; :lcd:print-str
 (defun :lcd:print-str (msg-addr pos) (%shift %bulk-char %char)
   (loop 
    (lw %bulk-char $zero msg-addr)
    (inc msg-addr)
    (mov %shift 0)
    (repeat (<= %shift 24)
      (byte-ref %char %bulk-char %shift)
      (ret-if (= %char 0))
      (sw %char pos #xfe00)
      (inc pos)
      (inc %shift 8))))

 ;; :lcd:print-hex
 (defun :lcd:print-hex (value len pos) (%val %shift)
   (sub %shift len 1)
   (sll %shift %shift 2)
   (repeat (<= 0 %shift)
     (srl %val value %shift)
     (and %val %val #xf)
     (hex->char %val %val)
     (sub %shift %shift 4)
     (sw %val pos #xfe00)
     (inc pos)))

 :stack-base
 (@data #xefffff)

 :inst-table
 (@data :fetch)          ; #x00 (O): fetch
 (@data :inst-refer-local); #x01 (I): fetch
 (@data :inst-indirect)   ; #x02 (O): fetch
 (@data :inst-constant)   ; #x03 (O): fetch
 (@data :fetch)          ; #x04 (O): fetch
 (@data :inst-refer-free) ; #x05 (I): fetch
 (@data :fetch)          ; #x06 (O): fetch
 (@data :fetch)          ; #x07 (O): fetch
 (@data :fetch)          ; #x08 (O): fetch
 (@data :fetch)          ; #x09 (I): fetch
 (@data :fetch)          ; #x0a (O): fetch
 (@data :fetch)          ; #x0b (O): fetch
 (@data :fetch)          ; #x0c (O): fetch
 (@data :fetch)          ; #x0d (I): fetch
 (@data :fetch)          ; #x0e (O): fetch
 (@data :fetch)          ; #x0f (O): fetch
 (@data :fetch)          ; #x10 (O): fetch
 (@data :fetch)          ; #x11 (I): fetch
 (@data :fetch)          ; #x12 (O): fetch
 (@data :fetch)          ; #x13 (O): fetch
 (@data :fetch)          ; #x14 (O): fetch
 (@data :fetch)          ; #x15 (I): fetch
 (@data :fetch)          ; #x16 (O): fetch
 (@data :fetch)          ; #x17 (O): fetch
 (@data :fetch)          ; #x18 (O): fetch
 (@data :fetch)          ; #x19 (I): fetch
 (@data :fetch)          ; #x1a (O): fetch
 (@data :fetch)          ; #x1b (O): fetch
 (@data :fetch)          ; #x1c (O): fetch
 (@data :fetch)          ; #x1d (I): fetch
 (@data :fetch)          ; #x1e (O): fetch
 (@data :fetch)          ; #x1f (O): fetch
 (@data :fetch)          ; #x20 (O): fetch
 (@data :fetch)          ; #x21 (I): fetch
 (@data :fetch)          ; #x22 (O): fetch
 (@data :fetch)          ; #x23 (O): fetch
 (@data :fetch)          ; #x24 (O): fetch
 (@data :fetch)          ; #x25 (I): fetch
 (@data :fetch)          ; #x26 (O): fetch
 (@data :fetch)          ; #x27 (O): fetch
 (@data :fetch)          ; #x28 (O): fetch
 (@data :fetch)          ; #x29 (I): fetch
 (@data :fetch)          ; #x2a (O): fetch
 (@data :fetch)          ; #x2b (O): fetch
 (@data :fetch)          ; #x2c (O): fetch
 (@data :fetch)          ; #x2d (I): fetch
 (@data :fetch)          ; #x2e (O): fetch
 (@data :fetch)          ; #x2f (O): fetch
 (@data :fetch)          ; #x30 (O): fetch
 (@data :fetch)          ; #x31 (I): fetch
 (@data :fetch)          ; #x32 (O): fetch
 (@data :fetch)          ; #x33 (O): fetch
 (@data :fetch)          ; #x34 (O): fetch
 (@data :fetch)          ; #x35 (I): fetch
 (@data :fetch)          ; #x36 (O): fetch
 (@data :fetch)          ; #x37 (O): fetch
 (@data :fetch)          ; #x38 (O): fetch
 (@data :fetch)          ; #x39 (I): fetch
 (@data :fetch)          ; #x3a (O): fetch
 (@data :fetch)          ; #x3b (O): fetch
 (@data :fetch)          ; #x3c (O): fetch
 (@data :fetch)          ; #x3d (I): fetch
 (@data :fetch)          ; #x3e (O): fetch
 (@data :fetch)          ; #x3f (O): fetch
 (@data :fetch)          ; #x40 (O): fetch
 (@data :fetch)          ; #x41 (I): fetch
 (@data :fetch)          ; #x42 (O): fetch
 (@data :fetch)          ; #x43 (O): fetch
 (@data :fetch)          ; #x44 (O): fetch
 (@data :fetch)          ; #x45 (I): fetch
 (@data :fetch)          ; #x46 (O): fetch
 (@data :fetch)          ; #x47 (O): fetch
 (@data :fetch)          ; #x48 (O): fetch
 (@data :fetch)          ; #x49 (I): fetch
 (@data :fetch)          ; #x4a (O): fetch
 (@data :fetch)          ; #x4b (O): fetch
 (@data :fetch)          ; #x4c (O): fetch
 (@data :fetch)          ; #x4d (I): fetch
 (@data :fetch)          ; #x4e (O): fetch
 (@data :fetch)          ; #x4f (O): fetch
 (@data :fetch)          ; #x50 (O): fetch
 (@data :fetch)          ; #x51 (I): fetch
 (@data :fetch)          ; #x52 (O): fetch
 (@data :fetch)          ; #x53 (O): fetch
 (@data :fetch)          ; #x54 (O): fetch
 (@data :fetch)          ; #x55 (I): fetch
 (@data :fetch)          ; #x56 (O): fetch
 (@data :fetch)          ; #x57 (O): fetch
 (@data :fetch)          ; #x58 (O): fetch
 (@data :fetch)          ; #x59 (I): fetch
 (@data :fetch)          ; #x5a (O): fetch
 (@data :fetch)          ; #x5b (O): fetch
 (@data :fetch)          ; #x5c (O): fetch
 (@data :fetch)          ; #x5d (I): fetch
 (@data :fetch)          ; #x5e (O): fetch
 (@data :fetch)          ; #x5f (O): fetch
 (@data :fetch)          ; #x60 (O): fetch
 (@data :fetch)          ; #x61 (I): fetch
 (@data :fetch)          ; #x62 (O): fetch
 (@data :fetch)          ; #x63 (O): fetch
 (@data :fetch)          ; #x64 (O): fetch
 (@data :fetch)          ; #x65 (I): fetch
 (@data :fetch)          ; #x66 (O): fetch
 (@data :fetch)          ; #x67 (O): fetch
 (@data :fetch)          ; #x68 (O): fetch
 (@data :fetch)          ; #x69 (I): fetch
 (@data :fetch)          ; #x6a (O): fetch
 (@data :fetch)          ; #x6b (O): fetch
 (@data :fetch)          ; #x6c (O): fetch
 (@data :fetch)          ; #x6d (I): fetch
 (@data :fetch)          ; #x6e (O): fetch
 (@data :fetch)          ; #x6f (O): fetch
 (@data :fetch)          ; #x70 (O): fetch
 (@data :fetch)          ; #x71 (I): fetch
 (@data :fetch)          ; #x72 (O): fetch
 (@data :fetch)          ; #x73 (O): fetch
 (@data :fetch)          ; #x74 (O): fetch
 (@data :fetch)          ; #x75 (I): fetch
 (@data :fetch)          ; #x76 (O): fetch
 (@data :fetch)          ; #x77 (O): fetch
 (@data :fetch)          ; #x78 (O): fetch
 (@data :fetch)          ; #x79 (I): fetch
 (@data :fetch)          ; #x7a (O): fetch
 (@data :fetch)          ; #x7b (O): fetch
 (@data :fetch)          ; #x7c (O): fetch
 (@data :decode-O)       ; #x7d (I): decode-O
 (@data :inst-halt)      ; #x7e (O): halt
 (@data :fetch)          ; #x7f (O): fetch
 )


#|
(define mnemonic-table
  '(halt
    reset
    refer-local
    refer-free
    refer-global
    indirect
    constant
    close
    box
    branch-false
    jump
    assign-local
    assign-free
    assign-global
    global-binding
    conti
    nuate
    frame
    argument
    shift
    apply
    return
    add
    sub
    mul
    eq
    cons
    car
    cdr
    set-car!
    set-cdr!))

(macro (make-inst)
  `(begin
     (mov $at 0)
     ,@(map (lambda (str)
              `(inst $at $at ,(make-keyword (string-append "inst-" str))))
            (map symbol->string mnemonic-table))))

(macro (next)
  `(begin
     (j :run-loop)
     (mread $vmpc)))

(macro (index* sp-reg i-reg)
  `(begin
     (sll $at ,i-reg 2)
     (add $at ,sp-reg $at)
     (add $at $at 4)
     (mread $at)))

(macro (index-set!* sp-reg i-reg v-reg)
  `(begin
     (sll $at ,i-reg 2)
     (add $at ,sp-reg $at)
     (add $at $at 4)
     (mwrite ,v-reg $at)))
     
(macro (index-closure* addr-reg n-reg)
  `(begin
     (sll $at ,n-reg 2)
     (add $at ,addr-reg $at)
     (add $at $at 4)
     (mread $at)))

(macro (closure-body* addr-reg)
  `(mread ,addr-reg))

(macro (vmpush reg)
  `(begin
     (vmpush* ,reg)
     (msync)))

(macro (vmpush* reg)
  `(begin
     (mwrite ,reg $vmsp)
     (sub $vmsp $vmsp 4)))

(macro (vmpop reg)
  `(begin
     (vmpop*)
     (mfetch ,reg)))

(macro (vmpop*)
  `(begin
     (add $vmsp $vmsp 4)
     (mread $vmsp)))

(macro (int->fixnum rd rs)
  `(begin
     (sll ,rd ,rs 2)
     (or ,rd ,rd #b01)))

(macro (fixnum->int rd rs)
  `(begin
     (srl $at ,rs 31)
     (beq $at $zero :positive)
     (srl ,rd ,rs 2)
     (mov $at #xc0000000)
     (or ,rd ,rs $at)
     :positive))

(macro (arg->obj rd rs)
  `(sll ,rd ,rs 2))

(macro (set-box!* addr-reg v-reg)
  `(mwrite ,v-reg ,addr-reg))

(macro (car ret-reg cons-reg)
  `(begin
     (mread ,cons-reg)
     (mfetch ,ret-reg)))
     
(macro (cdr ret-reg cons-reg)
  `(begin
     (add $at ,cons-reg 4)
     (mread $at)
     (mfetch ,ret-reg)))

(macro (set-car!* cons-reg v-reg)
  `(mwrite ,v-reg ,cons-reg))

(macro (set-cdr!* cons-reg v-reg)
  `(begin
     (add $at ,cons-reg 4)
     (mwrite ,v-reg $at)))

(macro (hex->char rd rs)
  `(begin
     (sub $at ,rs 10)
     (bltzl $zero $at :under10)
     (add ,rd ,rs #x30)
     (add ,rd ,rd 7)
     :under10))

(macro (panic msg)
  `(begin
     (msync)
     (mov $a0 ,msg)
     (j :panic)
     (nop)))

(macro (funcall func . args)
  (when (<= 4 (length args))
    (errorf "Too many args: ~s @ ~8,'0x" args func))
  `(begin
     ,@(map (lambda (i arg)
              `(mov ,(string->symbol (format "$a~d" i)) ,arg))
            (iota 4)
            args)
     (jal $ra ,func)
     (nop)))

(macro (ret . opts)
  (let-optionals* opts ((r '$ra))
    `(begin
       (j ,r)
       (nop))))

(macro (byte->word rd rs)
  `(begin
     (sub $at ,rs 1)
     (srl $at $at 2)
     (add ,rd $at 1)))

(macro (sizeof-word len-reg addr-reg)
  `(begin
     (sub $at ,addr-reg 4)
     (mread $at)
     (mfetch ,len-reg)
     (srl ,len-reg ,len-reg 2)
     (mov $at #x3fffff)
     (and ,len-reg ,len-reg $at)))

(macro (type type-reg addr-reg)
  `(begin
     (sub $at ,addr-reg 4)
     (mread $at)
     (mfetch ,type-reg)
     (srl ,type-reg ,type-reg 24)))

(asm->hex
 (@equ :obj-false     #b0000000000001011)
 (@equ :obj-true      #b0000000100001011)
 (@equ :obj-nil       #b0000001000001011)
 (@equ :obj-eof       #b0000001100001011)
 (@equ :obj-undefined #b0000010000001011)
 (@equ :obj-unbound   #b0000010100001011)
 
 (@equ :rawdata 0)
 (@equ :box 1)
 (@equ :bignum 2)
 (@equ :string 3)
 (@equ :symbol 4)
 (@equ :pair 5)
 (@equ :vector 6)
 (@equ :procedure 7)
 (@equ :record 8)
 (@equ :object 9)
 (@equ :bytearray 10)

 (with-temp-reg (%dst %src %i %len %tmp)
   (lw %len $zero :bootcode-size)
   (mov %dst 0)
   (mov %src :bootcode)
   (mov %i 0)
   :loop
   (beq %i %len :exit)
   (lw %tmp %src %i)
   (add %i %i 1)
   (mwrite %tmp %dst)
   (add %dst %dst 4)
   (msync)
   (j :loop)
   (nop)
   :exit
   )
 
 (mov $sp #xfff)
 (make-inst)
 (mov $vmpc #x0)

 ;; fetch
 (mread $vmpc)
 :run-loop
 (mfetch $s0)
 (lookup $t0 $zero $s0)
 (jad $a0 $s0 $t0)
 (add $vmpc $vmpc 4)

 :inst-halt
 (funcall :print :msg-halted #xfe00)
 (funcall :print :msg-value0 #xfe10)
 (funcall :print-hex $value0 8 #xfe18)
 (halt)

 :inst-reset
 (mread $vmpc)
 (sll $vmpc $a0 2)
 (sw $vmpc $zero :codebase)
 (mov $vmsp #xeffffc)
 (mov $vmfp $vmsp)
 (mfetch $t0)
 (fixnum->int $free $t0)
 (next)

 :inst-refer-local
 (index* $vmfp $a0)
 (mov $valnum 1)
 (mfetch $value0)
 (next)

 :inst-refer-free
 (index-closure* $closure $a0)
 (mov $valnum 1)
 (mfetch $value0)
 (next)

 :inst-refer-global
 (with-temp-reg (%false)
   (lw $a1 $zero :global-binding)
   (arg->obj $a0 $a0)
   (funcall :assq $a0 $a1)
   (mov %false :obj-false)
   (beq $v0 %false :error)
   (car $value0 $v0)
   (mov $valnum 1)
   (next)
   :error
   (panic :errmsg-global-binding-not-found))

 :inst-indirect
 (mread $value0)
 (mov $valnum 1)
 (mfetch $value0)
 (next)

 :inst-constant
 (mread $vmpc)
 (add $vmpc $vmpc 4)
 (mov $valnum 1)
 (mfetch $value0)
 (next)

 :inst-close
 (mread $vmpc)
 (add $vmpc $vmpc 4)
 (mov $valnum 1)
 (mfetch $a1) ; $a1 <- closure body
 (funcall :closure $a0 $a1)
 (mov $value0 $v0)
 (sll $t0 $a0 2)
 (add $vmsp $vmsp $t0)
 (next)

 :inst-box
 (index* $vmsp $a0)
 (mov $t6 $a0)
 (mfetch $a0)
 (funcall :box $a0)
 (index-set!* $vmsp $t6 $v0)
 (msync)
 (next)

 :inst-branch-false
 (begin
   (mov $t0 #b00000000000000000000000000001011)
   (bne $value0 $t0 :skip)
   (sll $a0 $a0 2)
   (add $vmpc $vmpc $a0)
   :skip
   )
 (next)

 :inst-jump
 (sll $a0 $a0 2)
 (add $vmpc $vmpc $a0)
 (next)

 :inst-assign-local
 (index* $vmfp $a0)
 (mfetch $t0)
 (set-box!* $t0 $value0)
 (msync)
 (next)

 :inst-assign-free
 (index* $closure $a0)
 (mfetch $t0)
 (set-box!* $t0 $value0)
 (msync)
 (next)

 :inst-assign-global
 (with-temp-reg (%false %gref)
   (lw $a1 $zero :global-binding)
   (arg->obj $a0 $a0)
   (funcall :assq $a0 $a1)
   (mov %false :obj-false)
   (beq $v0 %false :error)
   (car %gref $v0)
   (set-box!* %gref $value0)
   (mov $valnum 1)
   (msync)
   (next)
   :error
   (panic :errmsg-global-binding-not-found))

 :inst-global-binding
 (with-temp-reg (%false %gref)
   (lw $s1 $zero :global-binding)
   (arg->obj $a0 $a0)
   (funcall :assq $a0 $s1)
   (mov %false :obj-false)
   (bne $v0 %false :exit)
   ;; #<box> <- (box #<unbound>)
   (mov $s2 $a0) ; $s2 <- symbol
   (mov $a0 :obj-unbound)
   (funcall :box $a0)
   ;; #<pair> <- (cons symbol #<box>)
   (funcall :cons $s2 $v0)
   ;; (cons #<pair> global-binding)
   (funcall :cons $v0 $s1)
   (sw $v0 $zero :global-binding)
   :exit
   (next))

 :inst-conti
 ;; save stack
 (with-temp-reg (%len %stackbase %i %src %dst %data)
   (mov %stackbase #xeffffc)
   (sub %len %stackbase $vmsp)
   (byte->word %len %len)
   (funcall :malloc :bytearray %len)
   (mov %i 0)
   (mov %dst $v0)
   (add %src $vmsp 4)
   :loop
   (beq %i %len :exit)
   (add %i %i 1)
   (mread %src)
   (mfetch %data)
   (mwrite %data %dst)
   (add %src %src 4)
   (add %dst %dst 4)
   (msync)
   (j :loop)
   (nop)
   :exit)
 ;; continuation
 (with-temp-reg (%addr %code %template %data)
   (mov %addr $v0)  ; $v0 has a saved stack.
   (funcall :malloc :rawdata 3)
   (mov %template :continuation-code)

   (lw %data %template 0)
   (mov %code $v0)
   (mwrite %data %code)  ; write (refer-local 0)
   (add %code %code 4)
   (msync)

   (lw %data %template 1)
   (sll %addr %addr 8)
   (or %data %addr %data)
   (mwrite %data %code)  ; write (nuate (save-stack))
   (add %code %code 4)
   (msync)

   (lw %data %template 2)
   (nop)
   (mwrite %data %code)  ; write (return 0)
   (msync)

   (funcall :closure 0 $v0)
   (mov $valnum 1)
   (mov $value0 $v0))
 (next)

 :inst-nuate
 (with-temp-reg (%len %i %src %tmp)
   ;; restore stack
   (arg->obj %src $a0)
   (sizeof-word %len $a0)

   (sll %tmp %len 2)
   (sub %tmp %tmp 4)
   (add %src %src %tmp)

   (mov $vmsp #xeffffc)

   (mov %i 0)
   :loop
   (beq %i %len :exit)
   (add %i %i 1)
   (mread %src)
   (sub %src %src 4)
   (mfetch %tmp)
   (vmpush %tmp)
   (j :loop)
   (nop)
   :exit
   )
 (next)

 :inst-frame
 (vmpush $closure)
 (vmpush* $vmfp)
 (lw $t0 $zero :codebase)
 (msync)
 (vmpush* $t0)
 (sll $t0 $a0 2)
 (add $t0 $vmpc $t0)
 (msync)
 (vmpush $t0)
 (next)

 :inst-argument
 (vmpush $value0)
 (next)

 :inst-shift
 (with-temp-reg (%i %j %tmp)
   (srl $a1 $a0 11)
   (and $a1 $a1 #x7ff)
   (and $a0 $a0 #x7ff)

   (sub %i $a0 1)
   :loop
   (bltzl $zero %i :exit)
   (index* $vmsp %i)
   (add %j %i $a1)
   (mfetch %tmp)
   (index-set!* $vmsp %j %tmp)
   (sub %i %i 1)
   (msync)
   (j :loop)
   (nop)
   :exit
   (sll %tmp $a1 2)
   (add $vmsp $vmsp %tmp))
 (next)

 :inst-apply
 (closure-body* $value0)
 (mov $closure $value0)
 (mov $vmfp $vmsp)
 (mfetch $vmpc)
 (sw $vmpc $zero :codebase)
 (next)

 :inst-return
 (sll $t0 $a0 2)
 (add $vmsp $vmsp $t0) ;; (set! *stack-pointer* (+ *stack-pointer* (* n 4)))
 (mov $t1 0)
 (index* $vmsp $t1)
 (add $t1 $t1 1)
 (mfetch $vmpc)        ;; (set! *pc* (index *stack-pointer* 0))
 (index* $vmsp $t1)
 (add $t1 $t1 1)
 (mfetch $t0)
 (sw $t0 $zero :codebase)   ;; (set! *codebase* (index *stack-pointer* 1))
 (index* $vmsp $t1)
 (add $t1 $t1 1)
 (mfetch $vmfp)        ;; (set! *frame-pointer* (index *stack-pointer* 2))
 (index* $vmsp $t1)
 (add $vmsp $vmsp 16)  ;; (set! *stack-pointer-new* (+ *stack-pointer* (* 4 4)))
 (mfetch $closure)     ;; (set! *closure* (index *stack-pointer* 3))
 (next)

 :inst-add
 (vmpop*)
 (fixnum->int $t0 $value0)
 (mov $valnum 1)
 (mfetch $t1)
 (fixnum->int $t1 $t1)
 (add $t0 $t0 $t1)
 (int->fixnum $value0 $t0)
 (next)

 :inst-sub
 (vmpop*)
 (fixnum->int $t0 $value0)
 (mov $valnum 1)
 (mfetch $t1)
 (fixnum->int $t1 $t1)
 (sub $t0 $t0 $t1)
 (int->fixnum $value0 $t0)
 (next)

 :inst-mul
 (vmpop*)
 (fixnum->int $t0 $value0)
 (mov $valnum 1)
 (mov $value0 0)
 (mfetch $t1)
 (fixnum->int $t1 $t1)
 (begin
   :loop
   (beq $t0 $zero :exit)
   (and $t2 $t0 1)
   (beq $t2 $zero :skip)
   (srl $t0 $t0 1)
   (add $value0 $value0 $t1)
   :skip
   (j :loop)
   (sll $t1 $t1 1)
   :exit
   (int->fixnum $value0 $value0)
   )
 (next)

 :inst-eq
 (begin
   (vmpop $t0)
   (beq $value0 $t0 :true)
   (mov $valnum 1)
   (mov $value0 :obj-false)
   (next)
   :true
   (mov $value0 :obj-true)
   (next))

 :inst-cons
 (vmpop*)
 (mov $valnum 1)
 (mfetch $t0)
 (funcall :cons $value0 $t0)
 (mov $value0 $v0)
 (next)

 :inst-car
 (begin
   (type $t0 $value0)
   (mov $t1 :pair)
   (bne $t0 $t1 :error)
   (nop)
   (car $value0 $value0)
   (next)
   :error
   (panic :errmsg-not-pair))

 :inst-cdr
 (begin
   (type $t0 $value0)
   (mov $t1 :pair)
   (bne $t0 $t1 :error)
   (mov $valnum 1)
   (cdr $value0 $value0)
   (next)
   :error
   (panic :errmsg-not-pair))
 
 :inst-set-car!
 (begin
   (vmpop*)
   (mov $t1 :pair)
   (mfetch $a1)
   (type $t0 $value0)
   (bne $t0 $t1 :error)
   (nop)
   (set-car!* $value0 $a1)
   (msync)
   (next)
   :error
   (panic :errmsg-not-pair))

 :inst-set-cdr!
 (begin
   (vmpop*)
   (mov $t1 :pair)
   (mfetch $a1)
   (type $t0 $value0)
   (bne $t0 $t1 :error)
   (nop)
   (set-cdr!* $value0 $a1)
   (msync)
   (next)
   :error
   (panic :errmsg-not-pair))

 ;; subroutines

 ;; malloc
 ;; input: $a0 (type)
 ;; input: $a1 (word size)
 ;; output: $v0 (addr)
  :malloc
  (sll $t0 $a1 2)
  (sll $t1 $a0 24)
  (or $t1 $t0 $t1)
  (mwrite $t1 $free)
  (add $v0 $free 4)
  (add $free $v0 $t0)
  (msync)
  (ret)

  ;; box
  ;; input: $a0 (value)
  ;; output: $v0 (addr)
  :box
  (save-reg ($ra $a0)
    (funcall :malloc :bytearray 1))
  (mwrite $a0 $v0)
  (msync)
  (ret)

  ;; cons
  ;; input: $a0 (car)
  ;; input: $a1 (cdr)
  ;; output: $v0 (addr)
  :cons
  (save-reg ($ra $a0 $a1)
    (funcall :malloc :pair 2))
  (mwrite $a0 $v0)
  (add $t0 $v0 4)
  (msync)
  (mwrite $a1 $t0)
  (msync)
  (ret)

  ;; assq
  ;; input: $a0 (obj)
  ;; input: $a1 (list)
  ;; output: $v0 (pair or #f)
  :assq
  (with-temp-reg (%rest %nil %key)
    (mov %rest $a1)
    (mov %nil :obj-nil)
    :loop
    (beq %rest %nil :notfound)
    (nop)
    (car $v0 %rest)
    ;; TODO: pairかどうかをチェックする
    (car %key $v0)
    (beq $a0 %key $ra) ;; return $v0 if (eq? $a0 (car $v0))
    (nop)
    (cdr %rest %rest)
    ;; TODO: pairかどうかをチェックする
    (j :loop)
    (nop)
    :notfound
    (mov $v0 :obj-false)
    (ret))

  ;; closure
  ;; input: $a0 (n)
  ;; input: $a1 (body)
  ;; output: $v0 (closure)
  :closure
  (with-temp-reg (%addr %i %obj)
    (save-reg ($ra)
      (save-reg ($a0 $a1)
        (add $a1 $a0 1)
        (funcall :malloc :bytearray $a1))
      (mwrite $a1 $v0)
      (mov %i 0)
      (mov %addr $v0)
      (msync)
      :loop
      (beq %i $a0 :exit)
      (add %addr %addr 4)
      (index* $vmsp %i)
      (mfetch %obj)
      (mwrite %obj %addr)
      (add %i %i 1)
      (msync)
      (j :loop)
      (nop)
      :exit)
    (ret))

  ;; print
  ;; input: $a0 (msg addr)
  ;; input: $a1 (position addr)
  :print
  (begin
    (mov $t0 0) ; $t0: msg index
    (mov $t3 0) ; $t3: pos delta
    :loop-word
    (lw $t1 $a0 $t0)
    (add $t0 $t0 1)
    (mov $t5 24) ; $t5: shift
    :loop-byte
    (srl $t2 $t1 $t5)
    (and $t2 $t2 #xff)
    (beq $t2 $zero :exit)
    (lw $t4 $a1 $t3)
    (sub $t5 $t5 8)
    (and $t4 $t4 #xff00)
    (or $t2 $t4 $t2)
    (sw $t2 $a1 $t3)
    (bgezl $zero $t5 :loop-byte)
    (add $t3 $t3 1)
    (j :loop-word)
    (nop)
    :exit
    (ret))

  ;; print-hex
  ;; input: $a0 (value)
  ;; input: $a1 (len)
  ;; input: $a2 (position addr)
  :print-hex
  (with-temp-reg (%pos-delta %val %shift %attr-mask)
    (mov %pos-delta 0)
    (sub %shift $a1 1)
    (sll %shift %shift 2)
    :loop
    (srl %val $a0 %shift)
    (and %val %val #xf)
    (hex->char %val %val)
    (lw %attr-mask $a2 %pos-delta)
    (sub %shift %shift 4)
    (and %attr-mask %attr-mask #xff00)
    (or %val %attr-mask %val)
    (sw %val $a2 %pos-delta)
    (bgezl $zero %shift :loop)
    (add %pos-delta %pos-delta 1))
  (ret)

  ;; panic
  ;; input: $a0 (msg addr)
  :panic
  (funcall :print $a0 #xfe00)
  (funcall :print-hex $vmpc 6 #xfe10)
  (funcall :print :separator #xfe16)
  (funcall :print-hex $s0 8 #xfe18)
  (halt)

  :codebase
  (@data 0)
  :global-binding
  (@data :obj-nil)
  :value1
  (@data 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)

  :continuation-code
  (@data #x00000002)  ; (refer-local 0)
  (@data #x00000010)  ; (nuate 0)
  (@data #x00000015)  ; (return 0)
  
  :msg-halted
  (@string "Halted")

  :msg-value0
  (@string "value0: ")
  
  :separator
  (@string ": ")
  
  :errmsg-global-binding-not-found
  (@string "G-bind not found")

  :errmsg-not-pair
  (@string "Not Pair")
  
  :bootcode-size
  (@data #x00000030)
  :bootcode
  (@data #x00008801)
  (@data #x00000301)
  (@data #x04000004)
  (@data #x00000014)
  (@data #x03000008)
  (@data #x00000011)
  (@data #x00000020)
  (@data #x00000004)
  (@data #x74636166)
  (@data #x0000005c)
  (@data #x00000006)
  (@data #x00000005)
  (@data #x00000012)
  (@data #x00000002)
  (@data #x00000019)
  (@data #x00000c09)
  (@data #x00000006)
  (@data #x00000005)
  (@data #x0000340a)
  (@data #x00002411)
  (@data #x00000006)
  (@data #x00000005)
  (@data #x00000012)
  (@data #x00000002)
  (@data #x00000017)
  (@data #x00000012)
  (@data #x00000003)
  (@data #x00000005)
  (@data #x00000014)
  (@data #x00000012)
  (@data #x00000002)
  (@data #x00000018)
  (@data #x00000415)
  (@data #x00000038)
  (@data #x00000c0e)
  (@data #x00000c04)
  (@data #x00000012)
  (@data #x00000407)
  (@data #x00000028)
  (@data #x00000c0d)
  (@data #x00001811)
  (@data #x00000006)
  (@data #x00000029)
  (@data #x00000012)
  (@data #x00000c04)
  (@data #x00000005)
  (@data #x00000014)
  (@data #x00000000)

 )
|#
;; end of file
