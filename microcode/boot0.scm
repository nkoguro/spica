;; -*- coding: utf-8; mode: scheme -*-
;;
;; boot0.scm - Spica boot0
;;
;;   Copyright (c) 2010 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(macro (hex->char rd rs)
  `(begin
     (sub $at ,rs 10)
     (bltzl $zero $at :under10)
     (add ,rd ,rs #x30)
     (add ,rd ,rd 7)
     :under10))

(macro (dec->char rd rs)
  `(add ,rd ,rs #x30))

(macro (wait-data addr)
  `(begin
     :loop
     (lw $at $zero ,addr)
     (nop)
     (beq $at $zero :loop)
     (nop)))

(macro (pause-btn btn)
  (let ((v (cdr (assq btn '((north . 8) (south . 4) (east . 2) (west . 1))))))
    `(begin
       (repeat (!= $at 0)
         (lw $at $zero #xff00)
         (nop))
       (repeat (= $at 0)
         (lw $at $zero #xff00)
         (nop)
         (and $at $at ,v)))))

(macro (check-sw rt n)
  `(begin
     (lw $at $zero #xff01)
     (nop)
     (and ,rt $at ,(ash 1 n))))

(macro (serial:set-bps bps . args)
  (let-optionals* args ((addr #xfd00))
    (let ((v (cdr (assv bps
                        '((50 . #x00) (75 . #x01) (110 . #x02) (150 . #x03)
                          (300 . #x04) (600 . #x05) (1200 . #x06) (1800 . #x07)
                          (2000 . #x08) (2400 . #x09) (3600 . #x0a) (4800 . #x0b)
                          (7200 . #x0c) (9600 . #x0d) (19200 . #x0e) (38400 . #x0f)
                          (57600 . #x10) (76800 . #x11) (115200 . #x12))))))
      `(begin
         (mov $at ,v)
         (sw $at $zero ,(+ addr 1))))))

(macro (serial:read-byte reg . args)
  (let-optionals* args ((addr #xfd00))
    `(begin
       (repeat (= ,reg 0)
         (lw ,reg $zero ,(+ addr 2))
         (nop))
       (lw ,reg ,addr)
       (nop))))

(macro (serial:write-byte v . args)
  `(serial:write-byte-list (,v) ,@args))

(macro (serial:write-byte-list lst . args)
  (let-optionals* args ((addr #xfd00))
    `(begin
       (repeat (< 0 $at)
        (lw $at $zero ,(+ addr 3))
        (nop)
        (sub $at $at ,(- 15 (length lst))))
       ,@(map (lambda (v)
                (if (symbol? v)
                    `(sw ,v $zero ,addr)
                    `(begin
                       (mov $at ,v)
                       (sw $at $zero ,addr))))
              lst))))

(macro (sdcard:enable)
  `(begin
     (sw $zero $zero #xf00f)))

(macro (sdcard:disable)
 `(begin
    (mov $at 1)
    (sw $at $zero #xf00f)))

(macro (sdcard:cmd cmd arg)
  `(begin
     (mov $at ,cmd)
     (sw $at $zero #xf000)
     (debug-led $at)
     (mov $at ,arg)
     (sw $at $zero #xf001)))

(macro (sdcard:wait)
  `(repeat (!= $at 0)
     (lw $at $zero #xf010)
     (nop)
     (debug-led $at)))

(macro (sdcard:wait-finish treg us)
  `(begin
     (timeout-loop (,us ,treg 5)
       (lw $at $zero #xf010)
       (nop)
       (branch-if (= $at 0) :exit)
       (nop))
     :exit))

(main-code
 (mov $sp #xf7f)

 (:lcd:clear-all)

 (check-sw $t0 0)
 (if $t0
     (:menu)
     (:boot0))
 (halt)

 ;; :menu
 (defun :menu () (%char %len %v)
   (:lcd:print-str "Serial Console  " 0)
   (:lcd:print-str "9600bps:NP:S1   " 16)
   (serial:set-bps 9600)

   (:serial:print-str "***** boot0 menu *****\r\n")
   (loop 
    (:serial:print-str "\r\nd: dump sector\r\nb: write boot1\r\nf: write firmware\r\nr: reboot\r\nmenu> ")
    (:serial:read-line :line-buffer)
    (mov %len $v0)
    (unless (= %len 0)
      (lw %char $zero :line-buffer)
      (nop)
      (cond
       ((= %char #x62)
        (:menu:write-boot1))
       ((= %char #x64)
        (:menu:dump-sector))
       ((= %char #x66)
        (:menu:write-firmware))
       ((= %char #x72)
        (:serial:print-str "\r\nPress the south button to reboot.\r\n")
        (pause-btn south)
        (j 0)
        (nop))))))

 ;; :boot0
 (defun :boot0 () (%sector %addr)
   (:lcd:sdcard:check)
   (:lcd:print-str "boot0" 0)
   (mov %sector 1)
   (mov %addr #xc00000)
   (repeat (!= %sector 63)
     (:read-sector %sector %addr)
     (unless (= $v0 0)
       (:lcd:print-sdcard-error $v0 $v1)
       (:lcd:print-str "Press south btn." 16)
       (pause-btn south)
       (j 0)
       (nop))
     (inc %addr 512)
     (inc %sector))
   (debug-led #xff)
   (mov $a0 #xc04000)
   (j :exec-boot1)
   (nop))

 ;; :lcd:print-sdcard-error
 (defun :lcd:print-sdcard-error (resp cmd) ()
   (if cmd
       (begin
         (:lcd:print-str "Err:            " 0)
         (:lcd:print-hex cmd 8 4)
         (debug-led resp))
       (begin
         (:lcd:print-str "No SD Card      " 0))))
   
 ;; :menu:write-boot1
 (defun :menu:write-boot1 () (%sector %addr)
   (:serial:print-str "\r\n--- write boot1 ---\r\nPrepare to upload boot1, then press the south button. \r\n")
   (pause-btn south)
   (:serial:download-xmodem #x000000)
   (:serial:sdcard:check)
   (:serial:print-str "Writing SD Card...\r\n")
   ;; write boot1 code from sector 1 to sector 32
   (mov %sector 1)
   (mov %addr #x000000)
   (repeat (!= %sector 33)
     (:write-sector %sector %addr)
     (unless (= $v0 0)
       (:serial:print-sdcard-error $v0 $v1)
       (ret))
     (inc %addr 512)
     (inc %sector))
   (:serial:print-str "Done.\r\n"))

 ;; :menu:write-firmware
 (defun :menu:write-firmware () (%sector %addr)
   (:serial:print-str "\r\n--- write firmware ---\r\nPrepare to upload firmware, then press the south button. \r\n")
   (pause-btn south)
   (:serial:download-xmodem #x000000)
   (:serial:sdcard:check)
   (:serial:print-str "Writing SD Card...\r\n")
   ;; write firmware code from sector 33 to sector 62
   (mov %sector 33)
   (mov %addr #x000000)
   (repeat (!= %sector 63)
     (:write-sector %sector %addr)
     (unless (= $v0 0)
       (:serial:print-sdcard-error $v0 $v1)
       (ret))
     (inc %addr 512)
     (inc %sector))
   (:serial:print-str "Done.\r\n"))

 ;; :menu:dump-sector
 (defun :menu:dump-sector () (%v)
   (:serial:print-str "\r\n--- dump sector ---\r\n\r\nInput sector number: ")
   (:serial:read-line :line-buffer)
   (:string->integer :line-buffer $v0)
   (mov %v $v0)
   (:serial:sdcard:check)
   (:sdcard:read %v)
   (if (= $v0 0)
       (begin
         (:serial:print-str "\r\nsector: ")
         (:serial:print-dec %v)
         (:serial:print-crlf)
         (:serial:dump-byte #xf100 512))
       (begin
         (:serial:print-sdcard-error $v0 $v1)))
   (:serial:print-crlf))

 ;; :serial:print-sdcard-error
 (defun :serial:print-sdcard-error (resp cmd) ()
   (if cmd
       (begin
         (:serial:print-str "SD Card Error: Command=#x")
         (:serial:print-hex cmd 8)
         (:serial:print-str ", R1=#x") 
         (:serial:print-hex resp 2)
         (:serial:print-crlf))
       (begin
         (:serial:print-str "SD Card is not detected."))))
 
 ;; :read-sector
 (defun :read-sector (sector addr) (%mpos %pos %data)
   (:sdcard:read sector)
   (mov %pos 128)
   (repeat (!= %pos 0)
     (dec %pos)
     (lw %data %pos #xf100)
     (sll %mpos %pos 2)
     (msw %data %mpos addr)))
     
 ;; :write-sector
 (defun :write-sector (sector addr) (%mpos %pos %data)
   (mov %mpos 512)
   (repeat (!= %mpos 0)
     (sub %mpos %mpos 4)
     (mlw %data %mpos addr)
     (srl %pos %mpos 2)
     (sw %data %pos #xf180))
   (:sdcard:write sector))
     
 ;; mul
 ;; return: $v0 (mul)
 (defun :mul (m n) (%t)
   (branch-if (< n m) :dont-swap)
   (mov $v0 m)
   (mov m n)
   (mov n $v0)
   :dont-swap
   (beq n $zero :exit)
   (mov $v0 0)
   :loop
   (and %t n 1)
   (beq %t $zero :skip)
   (srl n n 1)
   (add $v0 $v0 m)
   :skip
   (bne n $zero :loop)
   (sll m m 1)
   :exit)

 ;; div
 ;; return: $v0 (quotient)
 ;; return: $v1 (remainer)
 (defun :div (m n) (%t %shift)
   (with-reg-alias ((q $v0) (r $v1))
     (mov q 0)
     (mov r m)
     (mov %shift 31)
     (srl %t r %shift)
     :loop
     (sub %t %t n)
     (bgez %t :q1)
     (sll q q 1)
     (beq %shift $zero :exit)
     (sub %shift %shift 1)
     (j :loop)
     (srl %t r %shift)
     :q1
     (sll %t n %shift)
     (sub r r %t)
     (or q q 1)
     (beq %shift $zero :exit)
     (sub %shift %shift 1)
     (j :loop)
     (srl %t r %shift)
     :exit))

 ;; :string->integer
 ;; return: $v0 (value)
 (defun :string->integer (addr len) (%c %i %v)
   (lw %c len addr)
   (mov %i 0)
   (mov %v 0)
   (when (= %c 68)
     (j :hex-str)
     (dec len))

   (while (< %i len)
     (lw %c %i addr)
     (:mul 10 %v)
     (mov %v $v0)
     (sub %c %c #x30)
     (add %v %v %c)
     (inc %i))
   (mov $v0 %v)
   (ret)

   :hex-str
   (while (< %i len)
     (lw %c %i addr)
     (sll %v %v 4)
     (cond
      ((<= #x61 %v)
       (sub %c %c #x57))
      ((<= #x41 %v)
       (sub %c %c #x37))
      (else
       (sub %c %c #x30)))
     (add %v %v %c)
     (inc %i))
   (mov $v0 %v)
   (ret))
   
 ;; :lcd:sdcard:check
 (defun :lcd:sdcard:check () (%t)
    (lw %t $zero #xf020)
    (nop)
    (and %t %t 1)
    (if (= %t 0)
        (:lcd:print-str "Insert SD Card." 0)
        (ret))

    (while (!= %t 0)
      (lw %t $zero #xf020)
      (nop)
      (and %t %t 1))
    (usleep 1000000))
     
 ;; :serial:sdcard:check
 (defun :serial:sdcard:check () (%t)
    (lw %t $zero #xf020)
    (nop)
    (and %t %t #x80)
    (unless (= %t 0)
      (:serial:print-str "Insert SD Card.\r\n"))

    (while (!= %t 0)
      (lw %t $zero #xf020)
      (nop)
      (and %t %t #x80))
    (usleep 1000000)
    (:serial:print-str "SD Card is detected.\r\n"))

 ;; sdcard:init
 ;; return: $v0 (last R1 response, or non-zero if SD Card is not detected)
 ;; return: $v1 (last command, or #x0 if SD Card is not detected.)
 (defun :sdcard:init () (%t0 %t1 %acmd41 %param %v2?)
   ;; check whether SD Card is inserted or not
   (lw %t0 $zero #xf020)
   (mov $v1 0)
   (and $v0 %t0 #x80)
   (ret-if $v0)

   ;; do nothing if the card is already initialized
   (and $v0 %t0 #x40)
   (ret-unless $v0)
   
   ;; send dummy clock
   (sdcard:disable)
   (usleep 1000)

   ;; CMD0
   (mov $v1 #x4a000100)
   (sdcard:cmd $v1 0)
   (sdcard:enable)
   (sdcard:wait-finish %t0 1000000)
   (sdcard:disable)
   (lw $v0 $zero #xf011)
   (nop)
   (ret-unless (= $v0 #x01))

   ;; CMD8
   (mov $v1 #x43000308)
   (sdcard:cmd $v1 #x000001aa)
   (sdcard:enable)
   (sdcard:wait-finish %t0 1000000)
   (sdcard:disable)
   (lw $v0 $zero #xf011)
   (nop)
   (if (= $v0 #x01)
       ;; version 2.00 or later if CMD8 is accepted
       (mov %v2? 1)
       (if (= $v0 #x05)
           ;; version 1.x if CMD8 is rejected
           (mov %v2? 0)
           (ret)))

   ;; ACMD41 for version 1.x
   (mov %acmd41 #x72000129)
   (mov %param #x0)
   (unless (= %v2? 0)
     ;; ACMD41 for version 2.00 or later
     (mov %acmd41 #x3b000129)
     (mov %param #x40000000))
   (repeat (= $v0 #x01)
     ;; CMD55
     (mov $v1 #x32000137)
     (sdcard:cmd $v1 0)
     (sdcard:enable)
     (sdcard:wait-finish %t0 1000000)
     (lw $v0 $zero #xf011)
     (nop)
     (ret-unless (= $v0 #x01))
     ;; ACMD41
     (mov $v1 %acmd41)
     (sdcard:cmd %acmd41 %param)
     (sdcard:enable)
     (sdcard:wait-finish %t0 1000000)
     (lw $v0 $zero #xf011)
     (sdcard:disable)
     (when (= $v0 #x01)
       (usleep 1000000)))
   (ret-unless (= $v0 0))

   ;; determine the sector-to-address coffecient. (x1 or x512) [-> %t0]
   (if %v2?
       (begin
         ;; CMD58 (version 2.00 or later) to check CCS bit
         (mov $v1 #x7e00033a)
         (sdcard:cmd $v1 0)
         (sdcard:enable)   
         (sdcard:wait-finish %t0 1000000)
         (sdcard:disable)
         (lw $v0 $zero #xf011)
         (lw %t0 $zero #xf013)
         (ret-unless (= $v0 0))
         (nop)
         ;; check CCS
         (srl %t0 %t0 30)
         (and %t0 %t0 1)
         (if  (= %t0 0)
           ;; address unit is byte if CCS = 0
           (mov %t0 9)
           ;; address unit is 512 bytes block if CCS = 1
           (mov %t0 0)))
       ;; address unit is byte when SDSD
       (mov %t0 9))
   (sw %t0 $zero #xf020)
   ;; CMD16
   (mov $v1 #x7f000110)
   (sdcard:cmd $v1 #x200)
   (sdcard:enable)
   (sdcard:wait-finish %t0 1000000)
   (sdcard:disable)
   (lw $v0 $zero #xf011))
 
 ;; sdcard:read
 ;; return: $v0 (last R1 response)
 ;; return: $v1 (last command)
 (defun :sdcard:read (sector) (%t0 %t1 %addr)
   (:sdcard:init)
   (lw %t0 $zero #xf020)
   (ret-unless $v0)
   ;; calc address
   (sll %addr sector %t0)
   ;; CMD17
   (mov $v1 #x7f000611)
   (sdcard:cmd $v1 %addr)
   (sdcard:enable)
   (sdcard:wait-finish %t0 1000000)
   (sdcard:disable)
   (lw $v0 $zero #xf011))

 ;; sdcard:write
 ;; return: $v0 (last R1 response)
 ;; return: $v1 (last command)
 (defun :sdcard:write (sector) (%t0 %t1 %addr)
   (:sdcard:init)
   (lw %t0 $zero #xf020)
   (ret-unless $v0)
   ;; calc address
   (sll %addr sector %t0)
   ;; CMD24
   (mov $v1 #x7f000718)
   (sdcard:cmd $v1 %addr)
   (sdcard:enable)
   (sdcard:wait-finish %t0 1000000)
   (sdcard:disable)
   (lw $v0 $zero #xf011))

 ;; :serial:print-str
 (defun :serial:print-str (msg-addr) (%bulk-char %char %shift)
   (loop 
    (lw %bulk-char $zero msg-addr)
    (inc! msg-addr)
    (mov %shift 0)
    (repeat (<= %shift 24)
      (byte-ref %char %bulk-char %shift)
      (ret-if (= %char 0))
      (serial:write-byte %char)
      (inc! %shift 8))))

 ;; :serial:print-hex
 (defun :serial:print-hex (value len) (%val %shift)
   (sub %shift len 1)
   (sll %shift %shift 2)
   (repeat (<= 0 %shift)
     (srl %val value %shift)
     (and %val %val #xf)
     (hex->char %val %val)
     (sub %shift %shift 4)
     (serial:write-byte %val)))

 ;; :serial:print-dec
 (defun :serial:print-dec (value) (%data %len)
   (mov %len 0)
   (repeat (!= value 0)
     (:div value 10)
     (mov value $v0)
     (dec->char %data $v1)
     (push %data)
     (inc %len))
   (while (< 0 %len)
     (pop %data)
     (serial:write-byte %data)
     (dec %len)))

 ;; :serial:print-crlf
 (defun :serial:print-crlf () ()
   (:serial:print-str "\r\n"))

 ;; :serial:dump-byte
 (defun :serial:dump-byte (addr len) (%val %word %pos %shift)
   (srl len len 2)
   (mov %pos 0)
   (while (!= len %pos)
     (and %val addr #x3)
     (if (= %val 0)
         (begin
           (:serial:print-crlf)
           (sll %val %pos 2)
           (:serial:print-hex %val 4)
           (:serial:print-str ": "))
         (:serial:print-str "  "))
     (lw %word $zero addr)
     (inc! addr)
     (inc! %pos)
     (mov %shift 0)
     (repeat (<= %shift 24)
       (byte-ref %val %word %shift)
       (:serial:print-hex %val 2)
       (:serial:print-str " ")
       (inc %shift 8)))
   (:serial:print-crlf)
   (ret))

 ;; :serial:read-line
 ;; NOTE: Terminal's newline must be "CR"
 ;; return: $v0 (length)
 (defun :serial:read-line (addr) (%length %char)
   (mov %length 0)
   (while (< %length 80)
     (serial:read-byte %char)
     (cond
      ((= %char #x0d)
       ;; (serial:read-byte %char) ; skip the next char (it will be "LF")
       (:serial:print-crlf)
       (j :break)
       (nop))
      ((= %char #x08)
       (unless (= %length 0)
         (serial:write-byte %char)
         (dec %length)))
      (else
       (serial:write-byte %char)
       (sw %char %length addr)
       (inc %length))))
   :break
   (mov $v0 %length))

 (@equ :SOH #x01)
 (@equ :EOT #x04)
 (@equ :ACK #x06)
 (@equ :NAK #x15)

 ;; download with XMODEM
 ;; input: $a0 (addr)
 (defun :serial:download-xmodem (addr) (%addr %nak-or-ack %i %checksum %data %tmp)
   (mov %addr addr)
   
   (mov %nak-or-ack :NAK)
   :nak-or-ack
   (:lcd:print-str "Waiting SOH..." 0)
   (serial:write-byte %nak-or-ack)
   (debug-led %nak-or-ack)
   (mov %i 50000000)
   (begin
     :wait-loop
     (beq %i $zero :nak-or-ack)
     (lw %data $zero #xfd02)
     (sub %i %i 1)
     (beq %data $zero :wait-loop)
     (debug-led %data)
     (lw %data $zero #xfd00)
     (mov %tmp :EOT)
     (beq %data %tmp :end-of-transmission)
     (mov %tmp :SOH)
     (beq %data %tmp :start-of-heading)
     (nop)
     (:lcd:print-hex %data 2 26)
     (:lcd:print-str "Bad ctrl: " 16)
     (ret))

   :start-of-heading
   (:lcd:print-str "Receiving...                    " 0)
   ;; Receive BN
   (wait-data #xfd02)
   (lw %data $zero #xfd00)
   (nop)
   (:lcd:print-hex %data 2 7)
   (:lcd:print-hex "Block: " 0)

   ;; Receive BNC
   (wait-data #xfd02)
   (lw %data $zero #xfd00)

   ;; Receive data
   (mov %checksum 0)
   (mov %i 32)
   (begin
     :loop
     (wait-data #xfd02)
     (lw %data $zero #xfd00)
     (nop)
     (add %checksum %checksum %data)
     (add %tmp $zero %data)
     (debug-led %data)

     (wait-data #xfd02)
     (lw %data $zero #xfd00)
     (nop)
     (add %checksum %checksum %data)
     (sll %data %data 8)
     (add %tmp %tmp %data)
     (debug-led %data)
     
     (wait-data #xfd02)
     (lw %data $zero #xfd00)
     (nop)
     (add %checksum %checksum %data)
     (sll %data %data 16)
     (add %tmp %tmp %data)
     (debug-led %data)
     
     (wait-data #xfd02)
     (lw %data $zero #xfd00)
     (nop)
     (add %checksum %checksum %data)
     (sll %data %data 24)
     (add %tmp %tmp %data)
     (debug-led %data)

     (mwrite %tmp %addr)
     (add %addr %addr 4)
     (sub %i %i 1)
     (msync)
     (bne %i $zero :loop)
     (nop))

   (wait-data #xfd02)
   (lw %data $zero #xfd00)
   (and %checksum %checksum #xff)
   (beq %data %checksum :nak-or-ack)
   (mov %nak-or-ack :ACK)
   
   (:lcd:print-str "Checksum Error  " 0)
   (mov %nak-or-ack :NAK)
   (sub %addr %addr 128)
   (j :nak-or-ack)
   (nop)
   
   :end-of-transmission
   (mov %nak-or-ack :ACK)
   (serial:write-byte %nak-or-ack)
   (:lcd:print-str "Completed                       " 0)
   (ret))
       
 ;; :lcd:print-str
 (defun :lcd:print-str (msg-addr pos) (%shift %bulk-char %char)
   (loop 
    (lw %bulk-char $zero msg-addr)
    (inc msg-addr)
    (mov %shift 0)
    (repeat (<= %shift 24)
      (byte-ref %char %bulk-char %shift)
      (ret-if (= %char 0))
      (sw %char pos #xfe00)
      (inc pos)
      (inc %shift 8))))

 ;; :lcd:print-hex
 (defun :lcd:print-hex (value len pos) (%val %shift)
   (sub %shift len 1)
   (sll %shift %shift 2)
   (repeat (<= 0 %shift)
     (srl %val value %shift)
     (and %val %val #xf)
     (hex->char %val %val)
     (sub %shift %shift 4)
     (sw %val pos #xfe00)
     (inc pos)))

 ;; :lcd:clear
 (defun :lcd:clear-all () ()
   (:lcd:print-str "                                " 0))

 ;; line buffer (80 words)
 :line-buffer
 (@data 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
 (@data 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
 (@data 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
 (@data 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
 (@data 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0)
  
 ;; exec boot1
 ;; input: $a0 (source address)
 (@org #xf80)
 :exec-boot1
 (begin
   ;; clear LCD
   (begin
     (mov $t0 32)
     :lcd-clear-loop
     (sub $t0 $t0 1)
     (sw $zero $t0 #xfe00)
     (bne $t0 $zero :lcd-clear-loop))

   ;; copy microcode
   (mov $t1 3840) ; boot1 (micro-spica part) size is 3840 words. (= 30 sector)
   (mov $t2 0)
   (begin
     :loop
     (mlw $t0 $zero $a0)
     (inc! $a0 4)
     (sw $t0 $t2 $zero)
     (bne $t2 $t1 :loop)
     (inc! $t2))

   ;; clear debug LEDs
   (debug-led $zero)
   (j 0)
   (mov $sp #xfff))
 )
;; end of file
