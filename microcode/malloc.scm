;; -*- coding: utf-8; mode: scheme -*-
;;
;; malloc.scm - malloc/free
;;
;;   Copyright (c) 2011 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without
;;   modification, are permitted provided that the following conditions
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright
;;      notice, this list of conditions and the following disclaimer in the
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors
;;      may be used to endorse or promote products derived from this
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(use us-test)

;; macros for debug
(defmacro show-free-small-blocks ()
  '(check-point
    (print "*** small free blocks ***")
    (let ((blocks (let loop ((result '())
                             (free-small-block
                              (ram-read-word (label :free-small-block))))
                    (if (= free-small-block 0)
                        (sort result)
                        (loop (cons free-small-block result)
                              (sdram-read-word (+ free-small-block 1)))))))
      (format #t " ~d free blocks" (length blocks))
      (fold (lambda (x i)
              (when (= (modulo i 6) 0)
                (display "\n   "))
              (format #t "#x~8,'0x, " x)
              (+ i 1))
            0
            blocks)
      (newline))))

(defmacro show-free-blocks ()
  '(check-point
    (let ((start-free-block (ram-read-word (label :start-free-block))))
      (print "*** free blocks ***")
      (let loop ((addr start-free-block))
        (let ((size (sdram-read-word addr))
              (next-addr (sdram-read-word (+ addr 1))))
          (format #t "#x~8,'0x:  size = ~d\n" addr size)
          (format #t "             next = #x~8,'0x\n" next-addr)
          (unless (= next-addr start-free-block)
            (loop next-addr)))))))

(defmacro mov-imm (reg v)
  (let ((n (cond
             ((fixnum? v)
              (logand v #x3fffffff))
             ((char? v)
              (logior (char->ucs v) #x40000000))
             ((equal? v #f) #x50000000)
             ((equal? v #t) #x58000000)
             ((equal? v '()) #x54000000)
             ((equal? v 'eof) #x5c000000)
             ((equal? v 'undefined) #x52000000)
             ((equal? v 'unbound) #x5a000000)
             (else
              (errorf "can't convert ~s to immediate value." v)))))
    `(mov ,reg ,n)))

(defmacro header->size (rt header)
  `(begin
     (sll ,rt ,header 8)
     (srl ,rt ,rt 8)))

(defmacro header->mark (rt header)
  `(srl ,rt ,header 30))

;; header must be unmarked.
(defmacro header-set-mark (rt header)
  `(adh ,rt ,header #xff40))

;; header must be marked.
(defmacro header-set-unmark (rt header)
  `(adh ,rt ,header #xffc0))

(defmacro header-clear-mark&counter (rt header)
  `(begin
     (sll ,rt ,header 8)
     (srl ,rt ,rt 8)))

;; size must be a register or lesser than #xffff.
(defmacro malloc (size)
  (if (and (integer? size)
           (<= size 3))
      '(begin*
         (call :malloc4))
      `(call :malloc ,size)))

;; addr must be a register.
(defmacro free (addr)
  `(call :free ,addr 0))

(defmacro retain (data)
  `(begin
     (when (obj? ,data)
       (let-temp (%header)
         (mlw %header ,data 0
              '()
              (adh %header %header #x3f01))
         (msw %header ,data 0)))))

;; return $v0: counter
(defmacro release (data . cont)
  `(begin
     (if (obj? ,data)
         (let-temp (%header)
           (mlw %header ,data 0
                '()
                (adh %header %header #x3fff))
           (msw %header ,data 0
                (srl $v0 %header 24))
           ,@cont)
         (mov $v0 #xff))))

;; $v0 will be broken.
(defmacro update-ref (place obj n data)
  (let ((load (case place
                ((ram) 'lw)
                ((sdram) 'mlw))))
    `(let-temp (%prev)
       (,load %prev ,obj n)
       (unless (= %prev ,data)
         (retain ,data)
         (release %prev)
         (when (= $v0 0)
           (:destruct %prev))))))

;; NOTE: This macro is used for :destruct and :full-gc.
;;       obj's counter must be 0.
(defmacro clear-obj (obj)
  `(let-temp (%i %size)
     (mlw %size ,obj 0
          (mov %i 1)
          ())
     (while (!= %i %size)
       (msw $zero ,obj %i
            (add %i %i 1)))))

(lib-code
 (defun :construct (type size) (allocator)
   (if (= size 4)
       (mov allocator :malloc4)
       (mov allocator :malloc))
   (call allocator size)

   (when (= $v0 0)
     (unless (= size 4)
       (:light-gc size)
       (branch-if (<= size $v0) :retry)
       (nop))
     ;; TODO: rewrite here!!
     (:full-gc #xfff 3)
     :retry
     (call allocator size)
     (when (= $v0 0)
       ;; TODO: exception?
       (mov-imm $v0 #f)
       (ret)))

   (or type type #xff80)
   (adh $v0 $v0 type))

 (defun :destruct (obj) (size i)
   (if (uvector? obj)
       (clear-obj obj)
       (begin
         (mlw size obj 0
              (mov i 1)
              ())
         (while (!= i size)
           (let-temp (%child)
             (mlw %child obj i)
             (release %child (msw $zero obj i))
             (add i i 1)
             (when (= $v0 0)
               (:destruct %child))))))
   (free obj))

 (defun :light-gc (size) (obj next)
   (lw obj $zero :free-small-block)
   (nop)
   (while (!= obj 0)
     (mlw next obj 1
          ()
          (sw next $zero :free-small-block))
     (:free obj 1)
     (ret-if (<= size $v0))
     (mov obj next)))

 (defun :mark (obj) (size i)
   (let-temp (%header %mark)
     (mlw %header obj 0
          (mov i 1)
          (header->mark %mark %header))
     (ret-unless (= %mark 0))
     (header-set-mark %header %header)
     (msw %header obj 0)
     (header->size size %header))
   (unless (uvector? obj)
     (while (!= i size)
       (let-temp (child)
         (mlw child obj i
              (add i i 1)
              ())
         (when (obj? child)
           (:mark child))))))

 (defun :full-gc (root root-size) (obj i end size)
   (lw end $zero :end-heap)
   ;; traverse all objects from roots and mark.
   (mov i 0)
   (while (< i root-size)
     (lw obj root i)
     (add i i 1)
     (when (obj? obj)
       (:mark obj)))

   ;; sweep unused objects.
   (mov obj #x000002)
   (while (!= obj end)
     (let-temp (%header %mark)
       (mlw %header obj 0
            ()
            (begin
              (header->size size %header)
              (header->mark %mark %header)))
       (if (= %mark 0)
           (let-temp (%counter)
             (srl %counter %header 24)
             ;; If %counter isn't 0, the object was alived.
             (unless (= %counter 0)
               ;; clear counter
               (header-clear-mark&counter %header %header)
               (msw %header obj 0)
               (clear-obj obj)
               ;; %header and %mark will be destroyed.
               (:free obj 1)))
           (begin
             ;; clear mark bit, because it is expired.
             (header-set-unmark %header %header)
             (msw %header obj 0))))
     (add obj obj size)))


  ;;
  ;; top block      000000h: 00 00 00 02
  ;;                000001h: 00 00 00 02
  ;; 1st free block 000002h: 00 3f ff fe
  ;;                000003h: 00 00 00 00
  ;; NOTE: top block is not allocated.
  (defun :minit () ()
    (let-temp (%data)
      (mov %data #x00000002)
      (msw %data $zero #x0)
      (msw %data $zero #x1)

      (mov %data #x3ffffe)
      (msw %data $zero #x2)
      (msw $zero $zero #x3)

      (sw $zero $zero :start-free-block)
      (sw $zero $zero :free-small-block)
      ))

  ;; return 4 word allocated memory. If no free space, return 0.
  :malloc4
  (let-temp (%free-block)
    (lw $v0 $zero :free-small-block)
    (nop)
    (if (= $v0 0)
        (begin
          (j :malloc)
          (add $a0 $zero 3))
        (mlw %free-block $v0 1
             ()
             (sw %free-block $zero :free-small-block))
        (msw $zero $v0 1)))
  (j $ra)
  (nop)

  ;; size: allocate memory size (word)
  ;; return an address. If no free space, return 0.
  :malloc
  (with-reg-alias ((size $a0))
    (let-temp (%free-block %free-size %prev-block %start-free-block)
      (lw %start-free-block $zero :start-free-block)
      ;; Initially, size doesn't include header size. So we must add its size.
      ;; And round up by a multiple of 2.
      (mlw %free-block %start-free-block 1
           (begin
             (add size size 2)
             (or size size #b1)
             (sub size size 1)
             (mov %prev-block %start-free-block)))

      (repeat (!= %prev-block %start-free-block)
        (mlw %free-size %free-block $zero) ;; header is equal to free block size.
        (cond
          ((= %free-block 0)
           ;; next free block
           (mlw %free-block %free-block 1
                (mov %prev-block %free-block)))
          ((< size %free-size)
           ;; allocate memory (allocate tail) and truncate free block
           (add $v0 %free-block %free-size)
           (sub $v0 $v0 size)
           (msw size $v0 0
                (sub %free-size %free-size size)
                ())
           (msw $zero $v0 1)

           (msw %free-size %free-block 0)
           (j $ra)
           (sw %prev-block $zero :start-free-block))
          ((= size %free-size)
           ;; Rewrite previous free block's next block address.
           (mlw %free-block %free-block 1
                ;; allocate memory
                (mov $v0 %free-block)
                ())
           (msw $zero $v0 1)
           (msw %free-block %prev-block 1)

           (j $ra)
           (sw %prev-block $zero :start-free-block))
          (else
           ;; next free block
           (mlw %free-block %free-block 1
                (mov %prev-block %free-block)))))
      (j $ra)
      (mov $v0 0)))

  ;; addr: allocated memory.
  ;; force-free-block: 0 or 1. If flag is 1, this block is returned to :free-block inspite of its size.
  ;; return: free block's size
  (defun :free (addr force-free-block) ()
    (with-reg-alias ((size $v0))
      (let-temp (%next-free %prev-free %t0 %t1)
        ;; Get size of addr block.
        (mlw size addr 0
             (begin
               ;; clear type
               (sll addr addr 8)
               (srl addr addr 8))
             (add size size force-free-block))

        ;; If the block is small (4 words), it is returned to :free-small-block.
        (when (= size 4)
          (lw %next-free $zero :free-small-block)
          (sw addr $zero :free-small-block)
          (msw %next-free addr 1)
          (ret))

        ;; Add free blocks.
        (lw %prev-free $zero :start-free-block)
        (sub size size force-free-block)
        (mlw %next-free %prev-free 1)

        (while (and (or (< addr %prev-free)
                        (< %next-free addr))
                    (or (!= %next-free 0)
                        (< addr %prev-free))
                    (or (!= %prev-free 0)
                        (< %next-free addr)))
          (mlw %next-free %next-free 1
               (mov %prev-free %next-free)))

        ;; join if addr is the next block of %prev-free
        (mlw %t0 %prev-free 0
             ()
             (add %t1 %prev-free %t0))
        (if (and (= %t1 addr)
                 (!= %prev-free 0))
            (begin
              (add size %t0 size)
              (msw size %prev-free 0
                   (mov addr %prev-free)))
            (begin
              (mlw %t1 %prev-free 1)
              (msw addr %prev-free 1)
              (msw %t1 addr 1)))
        (sw addr $zero :start-free-block)

        ;; join if %next-free is the next block of addr
        (add %t1 addr size)
        (when (and (= %t1 %next-free)
                   (!= %next-free 0))
          (mlw %t0 %next-free 0)
          (mlw %t1 %next-free 1
               (add size %t0 size))
          (msw size addr 0)
          (msw %t1 addr 1)))))

  :start-free-block
  (@data 0)

  :free-small-block
  (@data 0)

  :end-heap
  (@data #x400000)

  (unit-test
   "malloc"
   (setup
    (use srfi-1)

    (define (set-free-block! block-list)
      (fold (lambda (pair next)
              (let ((addr (car pair))
                    (size (cdr pair)))
                (set! (sdram addr) size)
                (set! (sdram (+ addr 1)) next)
                addr))
            0
            (append-reverse block-list '((0 . 2))))
      (set! (ram :start-free-block) 0))
    )

   (before
    (set! (reg '$sp) #xfff)
    (set! (ram :free-small-block) 0))

   ("1 word allocate"
    ((reg $v0 #x3ffffe)
     (reg $sp #xfff)
     (ram :start-free-block 0)
     (ram :free-small-block 0)
     (sdram #x0 #x00000002 #x00000002)
     (sdram #x2 #x3ffffc 0))

    (set-free-block! '((2 . #x3ffffe)))
    (call :malloc 1)
    )

   ("2 words allocate"
    ((reg $v0 #x3ffffc)
     (reg $sp #xfff)
     (ram :start-free-block 0)
     (ram :free-small-block 0)
     (sdram #x0 #x00000002 #x00000002)
     (sdram #x2 #x3ffffa 0))

    (set-free-block! '((2 . #x3ffffe)))
    (call :malloc 2)
    )

   ("3 words allocate"
    ((reg $v0 #x3ffffc)
     (reg $sp #xfff)
     (ram :start-free-block 0)
     (ram :free-small-block 0)
     (sdram #x0 #x00000002 #x00000002)
     (sdram #x2 #x3ffffa 0))

    (set-free-block! '((2 . #x3ffffe)))
    (call :malloc 3)
    )

   ("allocate in fragments"
    ((reg $v0 #x3c0004)
     (reg $sp #xfff)
     (ram :start-free-block 2)
     (ram :free-small-block 0)
     (sdram #x0 2 2)
     (sdram #x2 2 #x3c0000)
     (sdram #x3c0000 4 0))

    (set-free-block! '((2 . 2) (#x3c0000 . 8)))
    (call :malloc 3)
    )

   ("no free space"
    ((reg $v0 0)
     (reg $sp #xfff)
     (ram :start-free-block 0)
     (ram :free-small-block 0)
     (sdram #x0 #x00000002 #x00000002)
     (sdram #x2 #x3ffffe 0))

    (set-free-block! '((2 . #x3ffffe)))
    (call :malloc #x3fffff)
    )
   )

  (unit-test
   "malloc4"
   (setup
    (define (set-free-block! block-list)
      (fold (lambda (pair next)
              (let ((addr (car pair))
                    (size (cdr pair)))
                (set! (sdram addr) size)
                (set! (sdram (+ addr 1)) next)
                addr))
            0
            (append-reverse block-list '((0 . 2))))
      (set! (ram :start-free-block) 0))

    (define (set-free-small-block! addr-list)
      (set! (ram :free-small-block) (fold (lambda (addr next)
                                            (set! (sdram addr) 4)
                                            (set! (sdram (+ addr 1)) next)
                                            addr)
                                          0
                                          (reverse addr-list))))
    )

   (before
    (set! (reg '$sp) #xfff))

   ("allocate from free-small-block"
    ((reg $v0 #x3ffffc)
     (reg $sp #xfff)
     (ram :start-free-block 0)
     (ram :free-small-block 0))

    (set-free-block! '((2 . #x3ffffa)))
    (set-free-small-block! '(#x3ffffc))
    (call :malloc4)
    )

   ("allocate from free-block"
    ((reg $v0 #x3ffff8)
     (reg $sp #xfff)
     (ram :start-free-block 0)
     (ram :free-small-block 0)
     (sdram 0 2 2)
     (sdram 2 #x3ffff6 0))

    (set-free-block! '((2 . #x3ffffa)))
    (set-free-small-block! '())
    (call :malloc4)
    )
   )

  (unit-test
   "free"
   (setup
    (use srfi-1)

    (define (set-free-block! block-list)
      (fold (lambda (pair next)
              (let ((addr (car pair))
                    (size (cdr pair)))
                (set! (sdram addr) size)
                (set! (sdram (+ addr 1)) next)
                addr))
            0
            (append-reverse block-list '((0 . 2))))
      (set! (ram :start-free-block) 0)))

   (before
    (set! (reg '$sp) #xfff)
    (set! (ram :free-small-block) 0))

   ("free small block"
    ((reg $sp #xfff)
     (reg $v0 4)
     (ram :start-free-block 0)
     (ram :free-small-block #x3c0000)
     (sdram #x0 #x00000002 #x00000002)
     (sdram #x2 8 0)
     (sdram #x3c0000 4 0))

    (set-free-block! '((2 . 8)))
    (set! (sdram #x3c0000) 4)
    (call :free #x3c0000 0)
    )

   ("free small block (force-free-block is 1)"
    ((reg $sp #xfff)
     (reg $v0 4)
     (ram :start-free-block #x3c0000)
     (ram :free-small-block 0)
     (sdram #x0 #x00000002 #x00000002)
     (sdram #x2 8 #x3c0000)
     (sdram #x3c0000 4 0))

    (set-free-block! '((2 . 8)))
    (set! (sdram #x3c0000) 4)
    (call :free #x3c0000 1)
    )

   ("free large block (no merge)"
    ((reg $sp #xfff)
     (reg $v0 8)
     (ram :start-free-block #x3c0000)
     (ram :free-small-block 0)
     (sdram #x0 #x00000002 #x00000002)
     (sdram #x2 8 #x3c0000)
     (sdram #x3c0000 8 0))

    (set-free-block! '((2 . 8)))
    (set! (sdram #x3c0000) 8)
    (call :free #x3c0000 0)
    )

   ("free large block (merge the front block)"
    ((reg $sp #xfff)
     (reg $v0 16)
     (ram :start-free-block 2)
     (ram :free-small-block 0)
     (sdram #x0 #x00000002 #x00000002)
     (sdram #x2 16 #x3c0000)
     (sdram #x3c0000 8 0))

    (set-free-block! '((2 . 8) (#x3c0000 . 8)))
    (set! (sdram #xa) 8)
    (call :free #xa 0)
    )

   ("free large block (merge the rear block)"
    ((reg $sp #xfff)
     (reg $v0 16)
     (ram :start-free-block #x3bfff8)
     (ram :free-small-block 0)
     (sdram #x0 #x00000002 #x00000002)
     (sdram #x2 8 #x3bfff8)
     (sdram #x3bfff8 16 0))

    (set-free-block! '((2 . 8) (#x3c0000 . 8)))
    (set! (sdram #x3bfff8) 8)
    (call :free #x3bfff8 0)
    )

   ("free large block (merge the front and rear blocks)"
    ((reg $sp #xfff)
     (reg $v0 24)
     (ram :start-free-block 2)
     (ram :free-small-block 0)
     (sdram #x0 #x00000002 #x00000002)
     (sdram #x2 24 0))

    (set-free-block! '((2 . 8) (18 . 8)))
    (set! (sdram 10) 8)
    (call :free 10 0)
    )
   )

  (unit-test
   "destruct"
   (setup
    (use srfi-1)

    (define (imm v)
      (cond
        ((fixnum? v)
         (logand v #x3fffffff))
        ((char? v)
         (logior (char->ucs v) #x40000000))
        ((equal? v #f) #x50000000)
        ((equal? v #t) #x58000000)
        ((equal? v '()) #x54000000)
        ((equal? v 'eof) #x5c000000)
        ((equal? v 'undefined) #x52000000)
        ((equal? v 'unbound) #x5a000000)
        (else
         (errorf "can't convert immediate value: ~s" v))))

    (define (make-obj type addr counter size)
      (let ((obj (logior (ash type 24) addr #x80000000)))
        (set! (sdram obj) (logior (ash counter 24) size))
        obj))

    (define (set-obj! obj . data)
      (for-each (lambda (v i)
                  (set! (sdram (+ obj i)) v))
                data
                (iota (length data) 1)))

    (define (set-free-block! block-list)
      (fold (lambda (pair next)
              (let ((addr (car pair))
                    (size (cdr pair)))
                (set! (sdram addr) size)
                (set! (sdram (+ addr 1)) next)
                addr))
            0
            (append-reverse block-list '((0 . 2))))
      (set! (ram :start-free-block) 0)))

   (before
    (set! (reg '$sp) #xfff)
    (set! (ram :free-small-block) 0))

   ;;       +----+----+----+----+
   ;; #x02  |C:0 | *  |#f  | *  |
   ;;       +----+-|--+----+-|--+
   ;;              |         |
   ;;         +----+         v
   ;;         |             +----+----+----+----+
   ;;         |       #x06  |C:3 | *  | 1  |#\a |
   ;;         |             +----+-|--+----+----+
   ;;         |                    |
   ;;         v                    +-----+
   ;;       +----+----+----+----+        |
   ;; #x0A  |C:1 | *  | *  | *  |        |
   ;;       +----+-|--+-|--+-|--+        |
   ;;              |    |    |           |
   ;;              +----+----|----------vv
   ;;                        |         +----+----+----+----+
   ;;                        |   #x0E  |C:3F| *  |nil |undf|
   ;;                     +--+         +----+-|--+----+----+
   ;;                     |                   |
   ;;                     v v-----------------+
   ;;                    +----+----+----+----+
   ;;              #x12  |C:2 |#f  |#f  |#f  |
   ;;                    +----+----+----+----+
   ;;
   ("destruct object tree"
    ((reg $sp #xfff)
     (ram :start-free-block 0)
     (ram :free-small-block #x02)
     (sdram #x00 #x00000002 #x00000000)
     (sdram #x02 #x00000004 #x0000000a #x50000000 #x00000000)
     (sdram #x06 #x02000004 #x8100000e #x00000001 #x40000061)
     (sdram #x0a #x00000004 #x00000000 #x00000000 #x00000000)
     (sdram #x0e #x3f000004 #x81000012 #x54000000 #x52000000)
     (sdram #x12 #x01000004 #x50000000 #x50000000 #x50000000))

    (set-free-block! '())
    (let ((obj0 (make-obj 1 #x02 0 4))
          (obj1 (make-obj 1 #x06 3 4))
          (obj2 (make-obj 1 #x0a 1 4))
          (obj3 (make-obj 1 #x0e #x3f 4))
          (obj4 (make-obj 1 #x12 2 4)))
      (set-obj! obj0 obj2 (imm #f) obj1)
      (set-obj! obj1 obj3 (imm 1) (imm #\a))
      (set-obj! obj2 obj3 obj3 obj4)
      (set-obj! obj3 obj4 (imm '()) (imm 'undefined))
      (set-obj! obj4 (imm #f) (imm #f) (imm #f))
      (call :destruct obj0))
    )
   )

  (unit-test
   "light-gc"
   (setup
    (use srfi-1)

    (define (set-free-small-block! addr-list)
      (set! (ram :free-small-block) (fold (lambda (addr next)
                                            (set! (sdram addr) 4)
                                            (set! (sdram (+ addr 1)) next)
                                            addr)
                                          0
                                          (reverse addr-list))))

    (define (set-free-block! block-list)
      (fold (lambda (pair next)
              (let ((addr (car pair))
                    (size (cdr pair)))
                (set! (sdram addr) size)
                (set! (sdram (+ addr 1)) next)
                addr))
            0
            (append-reverse block-list '((0 . 2))))
      (set! (ram :start-free-block) 0)))

   (before
    (set! (reg '$sp) #xfff)
    (set! (ram :start-free-block) 0)
    (set! (ram :free-small-block) 0))

   ("collect free-small-blocks, can allocate the requested size."
    ((reg $sp #xfff)
     (reg $v0 12)
     (ram :start-free-block #x02)
     (ram :free-small-block #x0e)
     (sdram #x00 #x00000002 #x00000002)
     (sdram #x02 #x0000000c #x00000000)
     (sdram #x0e #x00000004 #x00000000))

    (set-free-block! '())
    (set-free-small-block! '(#x2 #xa #x6 #xe))
    (call :light-gc 12)
    )

   ("collect free-small-blocks, but can't allocate the requested size."
    ((reg $sp #xfff)
     (reg $v0 16)
     (ram :start-free-block #x02)
     (ram :free-small-block #x00)
     (sdram #x00 #x00000002 #x00000002)
     (sdram #x02 #x00000010 #x00000000))

    (set-free-block! '())
    (set-free-small-block! '(#x2 #xa #x6 #xe))
    (call :light-gc 24)
    )
   )

  (unit-test
   "full-gc"
   (setup
    (use srfi-1)

    (define (imm v)
      (cond
        ((fixnum? v)
         (logand v #x3fffffff))
        ((char? v)
         (logior (char->ucs v) #x40000000))
        ((equal? v #f) #x50000000)
        ((equal? v #t) #x58000000)
        ((equal? v '()) #x54000000)
        ((equal? v 'eof) #x5c000000)
        ((equal? v 'undefined) #x52000000)
        ((equal? v 'unbound) #x5a000000)
        (else
         (errorf "can't convert immediate value: ~s" v))))

    (define (make-obj type addr counter size)
      (let ((obj (logior (ash type 24) addr #x80000000)))
        (set! (sdram obj) (logior (ash counter 24) size))
        obj))

    (define (set-obj! obj . data)
      (for-each (lambda (v i)
                  (set! (sdram (+ obj i)) v))
                data
                (iota (length data) 1)))

    (define (set-free-block! block-list)
      (fold (lambda (pair next)
              (let ((addr (car pair))
                    (size (cdr pair)))
                (set! (sdram addr) size)
                (set! (sdram (+ addr 1)) next)
                addr))
            0
            (append-reverse block-list '((0 . 2))))
      (set! (ram :start-free-block) 0)))

   (before
    (set! (reg '$sp) #xffc)
    (set! (ram :start-free-block) 0)
    (set! (ram :free-small-block) 0))

   ;;           obj0=#0x02:type=1         obj1=#x06:type=2
   ;;   +----+     +----+----+----+----+     +----+----+----+----+
   ;;   | *------->|C:1 | *  |*   |  *------>|C:2 | *  |    |    |
   ;;   +----+     +----+-|--+|---+----+     +----+-|--+----+----+
   ;;   | #f |            |   v v--------------^----+
   ;;   +----+            | +----+----+----+---|+
   ;;   | *------+        > |C:3 |    |    |   *|
   ;;   +----+   |          +----+----+----+----+
   ;;    root    |       obj2=#x0a:type=3
   ;;            | obj3=#x0e:type=4         obj4=#x12:type=0
   ;;            |  +----+----+----+----+     +----+----+----+----+
   ;;            +->|C:2 | *  |    |  *------>|C:1 |    |    |    |
   ;;               +----+-|--+----+----+     +----+----+----+----+
   ;;                 ^    v
   ;;                 |  +----+----+----+----+
   ;;                 |  |C:3F|    |    | *  |obj5=#x16:type=5
   ;;                 |  +----+----+----+-|--+
   ;;                 +-------------------+
   ;;  obj6=#x1a:type=6                         v------------------------+
   ;;   +----+----+----+----+                 +----+----+----+----+      |
   ;;   |C:3F| *  |    |    |                 |C:1 | *  |    |    |      |
   ;;   +----+-|--+----+----+                 +----+-|--+----+----+      |
   ;;          v                    obj7=#x1e:type=7 | obj8=#x38:type=8  |
   ;;         +----+----+----+----+                  |  +----+----+----+-|--+
   ;;         |C:1 |    |    |    |                  |  |C:1 |    |    | *  |
   ;;         +----+----+----+----+                  |  +----+----+----+----+
   ;;       obj9=#x30:type=0                         v    ^
   ;;                                         +----+----+-|--+----+
   ;;                                         |C:1 |    | *  |    |
   ;;                                         +----+----+----+----+
   ;;                                      obj10=#x34:type=10
   ;;       #x22:free (14words)     #x3c:free (4194244words)
   ;;        +---+------||-----+     +----+-----||-----+
   ;;        |   |      ||     |     |    |     ||     |
   ;;        +---+------||-----+     +----+-----||-----+
   ;;
   ("mark&sweep"
    ((reg $sp #xffc)
     (ram :start-free-block #x1a)
     (ram :free-small-block #x0)
     (sdram #x00 #x00000002 #x0000001a)
     (sdram #x02 #x01000004 #x8300000a #x8300000a #x82000006)
     (sdram #x06 #x02000004 #x8300000a #x50000000 #x50000000)
     (sdram #x0a #x03000004 #x50000000 #x50000000 #x82000006)
     (sdram #x0e #x02000004 #x85000016 #x50000000 #x80000012)
     (sdram #x12 #x01000004 #x8600001a #x8700001e #x80000022)
     (sdram #x16 #x3f000004 #x50000000 #x50000000 #x8400000e)
     (sdram #x1a #x003fffe6 #x00000000 #x00000000 #x00000000)
     ;; these headers aren't used, but harmless.
     (sdram #x1e #x00000004 #x00000000 #x00000000 #x00000000)
     (sdram #x30 #x00000004 #x00000000 #x00000000 #x00000000)
     (sdram #x34 #x00000004 #x00000000 #x00000000 #x00000000)
     (sdram #x38 #x00000004 #x00000000 #x00000000 #x00000000))

    (set-free-block! '((#x22 . 14) (#x3c . 4194244)))
    (let ((obj0 (make-obj 1 #x02 1 4))
          (obj1 (make-obj 2 #x06 2 4))
          (obj2 (make-obj 3 #x0a 3 4))
          (obj3 (make-obj 4 #x0e 2 4))
          (obj4 (make-obj 0 #x12 1 4))
          (obj5 (make-obj 5 #x16 #x3f 4))
          (obj6 (make-obj 6 #x1a #x3f 4))
          (obj7 (make-obj 7 #x1e 1 4))
          (obj8 (make-obj 8 #x38 1 4))
          (obj9 (make-obj 0 #x30 1 4))
          (obj10 (make-obj 10 #x34 1 4)))
      (set-obj! obj0 obj2 obj2 obj1)
      (set-obj! obj1 obj2 (imm #f) (imm #f))
      (set-obj! obj2 (imm #f) (imm #f) obj1)
      (set-obj! obj3 obj5 (imm #f) obj4)
      (set-obj! obj4 obj6 obj7 #x80000022)
      (set-obj! obj5 (imm #f) (imm #f) obj3)
      (set-obj! obj6 obj9 (imm #f) (imm #f))
      (set-obj! obj7 obj10 (imm #f) (imm #f))
      (set-obj! obj8 (imm #f) (imm #f) obj7)
      (set-obj! obj9 obj0 obj6 obj7)
      (set-obj! obj10 (imm #f) obj8 (imm #f))
      (set! (ram #xffd) obj0)
      (set! (ram #xffe) (imm #f))
      (set! (ram #xfff) obj3))
    (call :full-gc #xffd 3))
   )
  )
;; end of file
