;; -*- coding: utf-8; mode: scheme -*-
;;
;; serial.scm - serial test
;;
;;   Copyright (c) 2010 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(use srfi-1)

(macro (hex->char rd rs)
  `(begin
     (sub $at ,rs 10)
     (bltzl $zero $at :under10)
     (add ,rd ,rs #x30)
     (add ,rd ,rd 7)
     :under10))

(main-code
 ;; initialize serial port (9600bps)
 (mov $t0 #xd)
 (sw $t0 $zero #xfd01)
 (sw $t0 $zero #xfd11)

 (funcall :print :start-msg #xfe00)

 (mov $s0 #xe000)
 (mov $s1 #x0)
 
 :loop
 ;; check read data
 (lw $t0 $zero #xfd02)
 (lw $t1 $zero #xfd01)
 (beq $t0 $zero :write-data)
 (sw $t1 $zero #xff80)

 ;; read data
 (lw $t0 $zero #xfd00)
 (nop)
 (sw $t0 $s1 $s0)
 (funcall :print-hex $t0 2 #xfe10)
 (add $s1 $s1 1)
 (sw $s1 $zero #xefff)

 ;; write data if a button is pressed.
 :write-data
 (lw $t0 $zero #xff00)
 (nop)
 (beq $t0 $zero :loop)
 (nop)
 (mov $t0 #x41)
 (sw $t0 $zero #xfd10)
 (funcall :print-hex $t0 2 #xfe18)
 (j :loop)
 (nop)
 
  ;; print
  ;; input: $a0 (msg addr)
  ;; input: $a1 (position addr)
  :print
  (begin
    (mov $t0 0) ; $t0: msg index
    (mov $t3 0) ; $t3: pos delta
    :loop-word
    (lw $t1 $a0 $t0)
    (add $t0 $t0 1)
    (mov $t5 24) ; $t5: shift
    :loop-byte
    (srl $t2 $t1 $t5)
    (and $t2 $t2 #xff)
    (beq $t2 $zero :exit)
    (lw $t4 $a1 $t3)
    (sub $t5 $t5 8)
    (and $t4 $t4 #xff00)
    (or $t2 $t4 $t2)
    (sw $t2 $a1 $t3)
    (bgezl $zero $t5 :loop-byte)
    (add $t3 $t3 1)
    (j :loop-word)
    (nop)
    :exit
    (ret))

  ;; print-hex
  ;; input: $a0 (value)
  ;; input: $a1 (len)
  ;; input: $a2 (position addr)
  :print-hex
  (let-temp (%pos-delta %val %shift %attr-mask)
    (mov %pos-delta 0)
    (sub %shift $a1 1)
    (sll %shift %shift 2)
    :loop
    (srl %val $a0 %shift)
    (and %val %val #xf)
    (hex->char %val %val)
    (lw %attr-mask $a2 %pos-delta)
    (sub %shift %shift 4)
    (and %attr-mask %attr-mask #xff00)
    (or %val %attr-mask %val)
    (sw %val $a2 %pos-delta)
    (bgezl $zero %shift :loop)
    (add %pos-delta %pos-delta 1))
  (ret)

  :start-msg
  (@string "Ready")
  )

;; end of file
