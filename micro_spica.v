`timescale 1ns / 1ps

module micro_spica(/*AUTOARG*/
  // Outputs
  pc, addr, write_enable, read_request, write_data, mem_read_n,
  mem_write_n, mem_addr,
  // Inouts
  mem_data,
  // Inputs
  clk, rst, program_code, read_data, mem_exec_n
  );
  input clk;
  input rst;

  output[11:0] pc;
  input [31:0] program_code;

  output reg [15:0] addr;
  output reg write_enable = 0;
  output reg read_request;
  input [31:0] read_data;
  output reg [31:0] write_data;

  output reg mem_read_n = 1;
  output reg mem_write_n = 1;
  input mem_exec_n;
  output reg [23:0] mem_addr;
  inout [31:0] mem_data;
  reg [31:0] mem_write_data;

  // memory access
  parameter
    MEMORY_ACCESS_IDLE       = 3'h0,
    MEMORY_ACCESS_READ       = 3'h1,
    MEMORY_ACCESS_READ_WAIT  = 3'h2,
    MEMORY_ACCESS_WRITE      = 3'h3,
    MEMORY_ACCESS_WRITE_WAIT = 3'h4;
  
  reg [2:0] memory_access_status = MEMORY_ACCESS_IDLE;
  reg [31:0] mdr;
  reg do_mem_read = 0;
  reg do_mem_write = 0;
  wire [6:0] msr = {
                    do_mem_read,
                    !mem_read_n,
                    do_mem_write,
                    !mem_write_n,
                    memory_access_status
                    };
  wire msr_busy = (msr != 0);
  
  parameter
    OP_FUNCT = 5'o00,
    OP_ADD   = 5'o01,
    OP_ADH   = 5'o02,
    OP_SUB   = 5'o03,
    OP_AND   = 5'o04,
    OP_OR    = 5'o05,
    OP_XOR   = 5'o06,
    OP_NOR   = 5'o07,
    OP_SLL   = 5'o14,
    OP_SRL   = 5'o15,
    OP_SLA   = 5'o16,
    OP_SRA   = 5'o17,
    OP_MLWR  = 5'o20,
    OP_LW    = 5'o21,
    OP_LMDR  = 5'o22,
    OP_BGEZL = 5'o23,
    OP_BGTZL = 5'o24,
    OP_BLEZL = 5'o25,
    OP_BLTZL = 5'o26,
    OP_INST  = 5'o27,
    OP_MSWR  = 5'o30,
    OP_SW    = 5'o31,
    OP_BTT   = 5'o34,
    OP_BNT   = 5'o35,
    OP_BEQ   = 5'o36,
    OP_BNE   = 5'o37;
  parameter
    FUNCT_NOP     = 6'o00,
    FUNCT_MSTATUS = 6'o51;
  
  (* ram_style = "distributed" *) reg [31:0] r[31:1];
  integer i;
  initial begin
    for (i = 1; i < 32; i = i + 1) r[i] = 32'h0;
  end
  
  // decode
  wire [31:0] inst     = program_code;
  wire [4:0] op        = inst[31:27];
  wire [4:0] inst_rs   = inst[21:17];
  wire [4:0] inst_rt   = inst[15:11];
  wire inst_imm_flag   = inst[16:16];
  wire [15:0] inst_imm = inst[15:0];
  wire [4:0] inst_rd   = inst[26:22];
  wire is_funct        = (op == OP_FUNCT);
  wire [5:0] funct     = inst[5:0];
  wire is_ru           = (op[4:3] == 2'b11);
  wire [4:0] inst_ru   = inst[26:22];
  
  reg exec_add           = 0;
  reg exec_adh           = 0;
  reg exec_sub           = 0;
  reg exec_and           = 0;
  reg exec_or            = 0;
  reg exec_xor           = 0;
  reg exec_nor           = 0;
  reg exec_sll           = 0;
  reg exec_srl           = 0;
  reg exec_sla           = 0;
  reg exec_sra           = 0;
  reg exec_mlwr          = 0;
  reg exec_lw            = 0;
  reg exec_lmdr          = 0;
  reg exec_mswr          = 0;
  reg exec_sw            = 0;
  reg exec_beq           = 0;
  reg exec_bne           = 0;
  reg exec_bltzl         = 0;
  reg exec_blezl         = 0;
  reg exec_bgtzl         = 0;
  reg exec_bgezl         = 0;
  reg exec_btt           = 0;
  reg exec_bnt           = 0;
  reg exec_funct_mstatus = 0;
  wire exec_call = exec_bltzl | exec_blezl | exec_bgtzl | exec_bgezl;
  wire [8:0] exec_branch_mask = {exec_btt,
                                 exec_bnt,
                                 exec_lmdr,
                                 exec_beq,
                                 exec_bne,
                                 exec_bltzl,
                                 exec_blezl,
                                 exec_bgtzl,
                                 exec_bgezl};
  wire [8:0] exec_branch_cond;
  
  reg [4:0] rs = 0;
  reg [4:0] rt = 0;
  reg [4:0] ru = 0;
  reg [15:0] imm;
  reg [4:0] rd = 0;
  reg [11:0] return_pc = 0;
  reg [11:0] next_pc = 0;
  reg [2:0] a_sel;
  reg [2:0] b_sel;
  reg [2:0] c_sel;
  
  // exec
  wire [31:0] a;
  wire [31:0] b;
  wire [31:0] bs;
  wire [31:0] c;
  
  wire [31:0] alu_add = a + b;
  wire [31:0] alu_adds = a + bs;
  wire [31:0] alu_adh = (a[31:24] == b[15:8]) ? a : {a[31:24] + b[7:0], a[23:0]};
  wire [31:0] alu_sub = a - b;
  wire [31:0] alu_and = a & b;
  wire [31:0] alu_or  = a | b;
  wire [31:0] alu_xor = a ^ b;
  wire [31:0] alu_nor = ~(a | b);
  wire [31:0] alu_sll = a << b[4:0];
  wire [31:0] alu_srl = a >> b[4:0];
  wire [31:0] alu_sla = a <<< b[4:0];
  wire [31:0] alu_sra = a >>> b[4:0];
  wire eq_a_c = (a == c);
  wire ltz_a = a[31];
  wire lez_a = (a[31] | (a == 32'h0));
  wire t_a_c = ((a[31:24] & c[15:8]) == c[7:0]);
  reg [31:0] alu_out = 0;
  reg [4:0] mem_rd = 0;
  
  // mem
  reg [4:0] wb_rd = 0;
  reg [31:0] wb_alu_out = 0;
  reg wb_mem = 0;

  // write back
  wire [31:0] wb_out = wb_mem ? read_data : wb_alu_out;
  
  always @(posedge clk) begin
    case (memory_access_status)
      MEMORY_ACCESS_IDLE: begin
        if (do_mem_read) begin
          memory_access_status <= MEMORY_ACCESS_READ;
          mem_read_n <= 0;
        end
        if (do_mem_write) begin
          memory_access_status <= MEMORY_ACCESS_WRITE;
          mem_write_n <= 0;
        end
      end
      MEMORY_ACCESS_READ: begin
        if (!mem_exec_n) begin
          memory_access_status <= MEMORY_ACCESS_READ_WAIT;
        end
      end
      MEMORY_ACCESS_READ_WAIT: begin
        if (mem_exec_n) begin
          memory_access_status <= MEMORY_ACCESS_IDLE;
          mdr <= mem_data;
          mem_read_n <= 1;
        end
      end
      MEMORY_ACCESS_WRITE: begin
        if (!mem_exec_n) begin
          memory_access_status <= MEMORY_ACCESS_WRITE_WAIT;
        end
      end
      MEMORY_ACCESS_WRITE_WAIT: begin
        if (mem_exec_n) begin
          memory_access_status <= MEMORY_ACCESS_IDLE;
          mem_write_n <= 1;
        end
      end
      default: begin
        memory_access_status <= MEMORY_ACCESS_IDLE;
        mem_read_n <= 1;
        mem_write_n <= 1;
      end
    endcase
  end
  
  // fetch
  always @(posedge clk) begin
    if (rst) begin
      next_pc <= 12'h0;
    end
    else begin
      next_pc <= pc + 1;
    end
  end

  // decode
  always @(posedge clk) begin
    if (rst) begin
      exec_lmdr   <= 0;
      exec_btt     <= 0;
      exec_bnt    <= 0;
      exec_beq    <= 0;
      exec_bne    <= 0;
      exec_bltzl  <= 0;
      exec_blezl  <= 0;
      exec_bgtzl  <= 0;
      exec_bgezl  <= 0;
    end
    else begin 
      exec_add    <= (op == OP_ADD);
      exec_adh    <= (op == OP_ADH);
      exec_sub    <= (op == OP_SUB);
      exec_and    <= (op == OP_AND);
      exec_or     <= (op == OP_OR);
      exec_xor    <= (op == OP_XOR);
      exec_nor    <= (op == OP_NOR);
      exec_sll    <= (op == OP_SLL);
      exec_srl    <= (op == OP_SRL);
      exec_sla    <= (op == OP_SLA);
      exec_sra    <= (op == OP_SRA);
      exec_mlwr   <= (op == OP_MLWR);
      exec_lw     <= (op == OP_LW);
      exec_lmdr   <= (op == OP_LMDR);
      exec_mswr   <= (op == OP_MSWR);
      exec_sw     <= (op == OP_SW);
      exec_btt    <= (op == OP_BTT);
      exec_bnt    <= (op == OP_BNT);
      exec_beq    <= (op == OP_BEQ);
      exec_bne    <= (op == OP_BNE);
      exec_bltzl  <= (op == OP_BLTZL);
      exec_blezl  <= (op == OP_BLEZL);
      exec_bgtzl  <= (op == OP_BGTZL);
      exec_bgezl  <= (op == OP_BGEZL);

      exec_funct_mstatus <= is_funct & (funct == FUNCT_MSTATUS);
      
      rs <= inst_rs;
      rt <= inst_rt;
      ru <= inst_ru;
      imm <= inst_imm_flag ? inst_imm : 16'h0;
      rd <= is_ru ? 32'h0 : inst_rd;
      
      a_sel <= {(inst_rs == 5'h0), (inst_rs == rd), (inst_rs == mem_rd)};
      b_sel <= {inst_imm_flag | (inst_rt == 5'h0), (inst_rt == rd), (inst_rt == mem_rd)};
      c_sel <= {(inst_ru == 5'h0), (inst_ru == rd), (inst_ru == mem_rd)};
      
      return_pc <= pc;
    end
  end
  
  // exec
  always @(posedge clk) begin
    if (exec_add) alu_out <= alu_add;
    if (exec_adh) alu_out <= alu_adh;
    if (exec_sub) alu_out <= alu_sub;
    if (exec_and) alu_out <= alu_and;
    if (exec_or)  alu_out <= alu_or;
    if (exec_xor) alu_out <= alu_xor;
    if (exec_nor) alu_out <= alu_nor;
    if (exec_sll) alu_out <= alu_sll;
    if (exec_srl) alu_out <= alu_srl;
    if (exec_sla) alu_out <= alu_sla;
    if (exec_sra) alu_out <= alu_sra;
    if (exec_call) alu_out <= return_pc;
    if (exec_lmdr) alu_out <= mdr;
    if (exec_funct_mstatus) alu_out <= {25'h0, msr};

    read_request <= exec_lw;
    mem_rd <= rd;

    write_enable <= exec_sw;
    write_data <= c;
    addr <= alu_adds[15:0];

    do_mem_write <= 0;
    if (exec_mswr) begin
      do_mem_write <= 1;
      mem_addr <= alu_adds[23:0];
      mem_write_data <= c;
    end

    do_mem_read <= 0;
    if (exec_mlwr) begin
      do_mem_read <= 1;
      mem_addr <= alu_adds[23:0];
    end
  end

  // mem
  always @(posedge clk) begin
    wb_rd      <= mem_rd;
    wb_alu_out <= alu_out;
    wb_mem     <= read_request;
  end
  
  // write back
  always @(posedge clk) begin
    if (wb_rd != 5'h0) begin
      r[wb_rd] <= wb_out;
    end
  end

  assign pc = ((exec_branch_mask & exec_branch_cond) == 9'h0) ? next_pc : b;

  function [31:0] sel;
    input [2:0] i;
    input [127:0] d;
    casex (i)
      3'b1xx: sel = d[127:96];
      3'b01x: sel = d[95:64];
      3'b001: sel = d[63:32];
      3'b000: sel = d[31:0];
    endcase
  endfunction
  
  assign a = sel(a_sel, {32'h0, alu_out, wb_out, r[rs]});
  assign b = sel(b_sel, {{16'h0, imm}, alu_out, wb_out, r[rt]});
  assign bs = sel(b_sel, {{{16{imm[15]}}, imm}, alu_out, wb_out, r[rt]});
  assign c = sel(c_sel, {32'h0, alu_out, wb_out, r[ru]});
  assign exec_branch_cond = {t_a_c, ~t_a_c, msr_busy, eq_a_c, ~eq_a_c, ltz_a, lez_a, ~lez_a, ~ltz_a};

  assign mem_data = (!mem_write_n && !mem_exec_n) ? mem_write_data : 32'bz;
endmodule
