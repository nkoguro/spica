`timescale 1ns/1ps

module micro_spica_test;
   // Inputs
   reg CLK_50MHz;
   reg rst;
   reg [3:0] SW;
   reg BTN_NORTH;
   reg BTN_SOUTH;
   reg BTN_EAST;
   reg BTN_WEST;
   reg ROT_A;
   reg ROT_B;
   reg ROT_CENTER;

   // Outputs
   wire [7:0] LED;

   wire [9:0] rom_addr;
   wire [31:0] rom_data;

   wire [15:0] ioaddr;
   wire ioload;
   wire [15:0] iodata_in;
   wire [15:0] iodata_out;

   // Instantiate the Unit Under Test (UUT)
   micro_spica micro_spica(.clk(CLK_50MHz), .rst(rst), 
                           .pc(rom_addr), .data_in(rom_data),  
                           .ioaddr(ioaddr), .ioload(ioload), .iodata_in(iodata_in), .iodata_out(iodata_out)
                           );
   micro_spica_testcode_inst testcode_inst(.clk(CLK_50MHz), .addr(rom_addr), .q(rom_data));
   io_controller io_controller( .clk(CLK_50MHz), 
                                .load(ioload), .addr(ioaddr), .data_in(iodata_out), .data_out(iodata_in),
                                .SW(SW), .BTN_NORTH(BTN_NORTH), .BTN_SOUTH(BTN_SOUTH), .BTN_EAST(BTN_EAST), .BTN_WEST(BTN_WEST),
                                .ROT_A(ROT_A), .ROT_B(ROT_B), .ROT_CENTER(ROT_CENTER), .LED(LED)
                                );
   

   reg [31:0] expected_r[31:1];
   
   task assert_r;
      input [4:0] reg_num;
      input [31:0] expected_val;
      integer i;
      integer success;
      begin
         success = 1;
         expected_r[reg_num] = expected_val;
         for (i = 1; i < 32; i = i + 1) begin
            if (expected_r[i] != micro_spica.r[i]) begin
               success = 0;
               $display("time = %0d ... Failed, r[%0d] is expected %h, but got %h", $time, i, expected_r[i], micro_spica.r[i]);
            end
         end
         if (success) $display("time = %0d ... OK", $time);
      end
   endtask

   task assert_notchanged;
      integer i;
      integer success;
      begin
         success = 1;
         for (i = 1; i < 32; i = i + 1) begin
            if (expected_r[i] != micro_spica.r[i]) begin
               success = 0;
               $display("time = %0d ... Failed, r[%0d] is expected %h, but got %h", $time, i, expected_r[i], micro_spica.r[i]);
            end
         end
         if (success) $display("time = %0d ... OK", $time);
      end
   endtask
   
   parameter STEPS = 20;

   integer i;
   initial begin
      // Initialize Inputs
      CLK_50MHz = 0;
      SW = 0;
      BTN_NORTH = 0;
      BTN_SOUTH = 0;
      BTN_EAST = 0;
      BTN_WEST = 0;
      ROT_A = 0;
      ROT_B = 0;
      ROT_CENTER = 0;

      $dumpfile("micro_spica_test.vcd");
      $dumpvars(0, micro_spica);
      
      for (i = 1; i < 32; i = i + 1) expected_r[i] = 0;
      
      #(STEPS/2);
      
      #(5*STEPS);
      #0       assert_r(1, 32'h0000f000);
      #(STEPS) assert_r(2, 32'h0000f000);
      #(STEPS) assert_r(3, 32'h0001e000);

      #(STEPS) assert_r(4, 32'h00000004);
      #(STEPS) assert_notchanged();
      #(STEPS) assert_notchanged();
      #(STEPS) assert_notchanged(); 
      #(STEPS) assert_notchanged();
      #(STEPS) assert_notchanged();
      #(STEPS) assert_r(5, 32'h00000009);
      #(STEPS) assert_r(6, 32'h00000003);
      #(STEPS) assert_r(7, 32'h00000006);
      #(STEPS) assert_notchanged();
      #(STEPS) assert_notchanged();
      #(STEPS) assert_notchanged();

      #(STEPS) assert_r(7, ~32'h00001234 + 1);
      #(STEPS) assert_r(8, ~32'h00001237 + 1);
      
      #0 $finish;
   end

   always #(STEPS/2) CLK_50MHz = ~CLK_50MHz;
endmodule
