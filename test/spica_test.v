`timescale 1ns/1ps

module spica_test;
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [7:4]           LCD_DB;                 // To/From spica of spica.v
   wire                 LCD_E;                  // From spica of spica.v
   wire                 LCD_RS;                 // From spica of spica.v
   wire                 LCD_RW;                 // From spica of spica.v
   wire [7:0]           LED;                    // From spica of spica.v
   wire [15:0]          SD_A;                   // From spica of spica.v
   wire [2:0]           SD_BA;                  // From spica of spica.v
   wire                 SD_CAS;                 // From spica of spica.v
   wire                 SD_CKE;                 // From spica of spica.v
   wire                 SD_CK_N;                // From spica of spica.v
   wire                 SD_CK_P;                // From spica of spica.v
   wire                 SD_CS;                  // From spica of spica.v
   wire [15:0]          SD_DQ;                  // To/From spica of spica.v, ...
   wire                 SD_LDM;                 // From spica of spica.v
   wire                 SD_LDQS_P;              // To/From spica of spica.v, ...
   wire                 SD_ODT;                 // From spica of spica.v
   wire                 SD_RAS;                 // From spica of spica.v
   wire                 SD_UDM;                 // From spica of spica.v
   wire                 SD_UDQS_P;              // To/From spica of spica.v, ...
   wire                 SD_WE;                  // From spica of spica.v
   wire [3:0]           VGA_B;                  // From spica of spica.v
   wire [3:0]           VGA_G;                  // From spica of spica.v
   wire                 VGA_HSYNC;              // From spica of spica.v
   wire [3:0]           VGA_R;                  // From spica of spica.v
   wire                 VGA_VSYNC;              // From spica of spica.v
   // End of automatics

   wire [3:0] SW;

   reg BTN_NORTH;
   reg BTN_SOUTH;
   reg BTN_EAST;
   reg BTN_WEST;
   
   reg CLK_50MHz = 0;

   always #10 CLK_50MHz = ~CLK_50MHz;
 
   spica spica(/*AUTOINST*/
               // Outputs
               .SD_CK_P                 (SD_CK_P),
               .SD_CK_N                 (SD_CK_N),
               .SD_CKE                  (SD_CKE),
               .SD_ODT                  (SD_ODT),
               .LCD_E                   (LCD_E),
               .LCD_RS                  (LCD_RS),
               .LCD_RW                  (LCD_RW),
               .LED                     (LED[7:0]),
               .SD_A                    (SD_A[15:0]),
               .SD_BA                   (SD_BA[2:0]),
               .SD_CAS                  (SD_CAS),
               .SD_CS                   (SD_CS),
               .SD_LDM                  (SD_LDM),
               .SD_RAS                  (SD_RAS),
               .SD_UDM                  (SD_UDM),
               .SD_WE                   (SD_WE),
               .VGA_B                   (VGA_B[3:0]),
               .VGA_G                   (VGA_G[3:0]),
               .VGA_HSYNC               (VGA_HSYNC),
               .VGA_R                   (VGA_R[3:0]),
               .VGA_VSYNC               (VGA_VSYNC),
               // Inouts
               .LCD_DB                  (LCD_DB[7:4]),
               .SD_DQ                   (SD_DQ[15:0]),
               .SD_LDQS_P               (SD_LDQS_P),
               .SD_UDQS_P               (SD_UDQS_P),
               // Inputs
               .CLK_50MHz               (CLK_50MHz),
               .BTN_EAST                (BTN_EAST),
               .BTN_NORTH               (BTN_NORTH),
               .BTN_SOUTH               (BTN_SOUTH),
               .BTN_WEST                (BTN_WEST),
               .ROT_A                   (ROT_A),
               .ROT_B                   (ROT_B),
               .ROT_CENTER              (ROT_CENTER),
               .SW                      (SW[3:0]));

   mock_ddr2ram mock_ddr2ram(/*AUTOINST*/
                             // Inouts
                             .SD_DQ             (SD_DQ[15:0]),
                             .SD_LDQS_P         (SD_LDQS_P),
                             .SD_UDQS_P         (SD_UDQS_P),
                             // Inputs
                             .SD_A              (SD_A[15:0]),
                             .SD_BA             (SD_BA[2:0]),
                             .SD_WE             (SD_WE),
                             .SD_RAS            (SD_RAS),
                             .SD_CAS            (SD_CAS),
                             .SD_CKE            (SD_CKE),
                             .SD_CK_N           (SD_CK_N),
                             .SD_CK_P           (SD_CK_P),
                             .SD_CS             (SD_CS),
                             .SD_LDM            (SD_LDM),
                             .SD_UDM            (SD_UDM),
                             .SD_ODT            (SD_ODT));
   
   initial begin
      BTN_NORTH <= 0;
      BTN_SOUTH <= 1;
      BTN_EAST <= 0;
      BTN_WEST <= 0;
      
      #400000 $finish;
   end
endmodule

module DCM_SP(/*AUTOARG*/
   // Outputs
   CLK0, CLK2X, CLK2X180, LOCKED,
   // Inputs
   CLKFB, CLKIN, PSCLK, PSEN, PSINCDEC, RST
   );
   output CLK0;
   // output CLK180;
   // output CLK270;
   output reg CLK2X;
   output reg CLK2X180;
   // output CLK90;
   // output CLKDV;
   // output CLKFX;
   // output CLKFX180;
   output reg LOCKED = 1;
   // output PSDONE;
   // output STATUS;
   input CLKFB;
   input CLKIN;
   input PSCLK;
   input PSEN;
   input PSINCDEC;
   input RST;

   parameter CLKDV_DIVIDE = 2.0;
   parameter CLKFX_DIVIDE = 1;
   parameter CLKFX_MULTIPLY = 4;
   parameter CLKIN_DIVIDE_BY_2 = "FALSE";
   parameter CLKIN_PERIOD= 20.0;
   parameter CLKOUT_PHASE_SHIFT= "NONE";
   parameter CLK_FEEDBACK = "2X";
   parameter DESKEW_ADJUST= "SYSTEM_SYNCHRONOUS";
   parameter DLL_FREQUENCY_MODE = "LOW";
   parameter DUTY_CYCLE_CORRECTION= "TRUE";
   parameter PHASE_SHIFT= 0;
   parameter STARTUP_WAIT= "FALSE";

   always #(CLKIN_PERIOD/4) begin
      CLK2X = ~CLK2X;
      CLK2X180 = ~CLK2X180;
   end

   always @(posedge CLKIN) begin
      CLK2X = CLKIN;
      CLK2X180 = ~CLKIN;
   end

   assign CLK0 = CLKIN;
endmodule

module IBUFG(/*AUTOARG*/
   // Outputs
   O,
   // Inputs
   I
   );
   input I;
   output O;

   assign O = I;
endmodule

module BUFG(/*AUTOARG*/
   // Outputs
   O,
   // Inputs
   I
   );
   input I;
   output O;

   assign O = I;
endmodule

module mock_ddr2ram(/*AUTOARG*/
   // Inouts
   SD_DQ, SD_LDQS_P, SD_UDQS_P,
   // Inputs
   SD_A, SD_BA, SD_WE, SD_RAS, SD_CAS, SD_CKE, SD_CK_N, SD_CK_P,
   SD_CS, SD_LDM, SD_UDM, SD_ODT
   );
   input [15:0] SD_A;
   input [2:0] SD_BA;
   input SD_WE;
   input SD_RAS;
   input SD_CAS;
   input SD_CKE;
   input SD_CK_N;
   input SD_CK_P;
   input SD_CS;
   inout [15:0] SD_DQ;
   inout SD_LDQS_P;
   inout SD_UDQS_P;
   input SD_LDM;
   input SD_UDM;
   input SD_ODT;

   parameter CL = 5;
   parameter AL = 0;
   parameter WL = AL + CL - 1;
   
   parameter
    BANK_ACTIVATE = 5'b10011,
     WRITE = 5'b10100,
     READ = 5'b10101,
     ALL_PRECHARGE = 5'b10010;
   
   reg [1:0] bank;
   reg [15:0] mem[32*1024*1024*1024-1:0];
   reg [15:0] dq0;
   reg [15:0] dq1;
   reg [9:0] write_column[15:0];
   reg [9:0] read_column[15:0];
   reg dq_read[15:0];
   reg dq_write[15:0];
   reg [3:0] queue_pointer = 0;
   reg [12:0] bank_row[1:0];

   initial begin
      bank_row[2'b00] <= 13'bx;
      bank_row[2'b01] <= 13'bx;
      bank_row[2'b10] <= 13'bx;
      bank_row[2'b11] <= 13'bx;
   end

   always @(posedge SD_CK_P) begin
      queue_pointer <= queue_pointer + 1;

      if ({SD_CKE, SD_CS, SD_RAS, SD_CAS, SD_WE} == BANK_ACTIVATE) begin
         // Bank ACTIVATE
         bank_row[SD_BA[1:0]] <= SD_A[12:0];
         bank <= SD_BA[1:0];
      end
      if ({SD_CKE, SD_CS, SD_RAS, SD_CAS, SD_WE} == WRITE) begin
         // WRITE
         write_column[(queue_pointer + WL + 1) & 4'hF] <= SD_A[9:0];
         write_column[(queue_pointer + WL + 2) & 4'hF] <= SD_A[9:0] + 10'h2;
         dq_write[(queue_pointer + WL + 1) & 4'hF] <= 1;
         dq_write[(queue_pointer + WL + 2) & 4'hF] <= 1;
      end
      if ({SD_CKE, SD_CS, SD_RAS, SD_CAS, SD_WE} == READ) begin
         // READ
         read_column[(queue_pointer + CL + 1) & 4'hF] <= SD_A[9:0];
         read_column[(queue_pointer + CL + 2) & 4'hF] <= SD_A[9:0] + 10'h2;
         dq_read[(queue_pointer + CL + 1) & 4'hF] <= 1;
         dq_read[(queue_pointer + CL + 2) & 4'hF] <= 1;
      end
      if ({SD_CKE, SD_CS, SD_RAS, SD_CAS, SD_WE} == ALL_PRECHARGE) begin
         // All banks PRECHARGE
         bank_row[2'b00] <= 13'bx;
         bank_row[2'b01] <= 13'bx;
         bank_row[2'b10] <= 13'bx;
         bank_row[2'b11] <= 13'bx;
      end
      
      dq_read[queue_pointer] <= 0;
      dq_write[queue_pointer] <= 0;
   end

   always @(posedge SD_LDQS_P) begin
      if (dq_write[queue_pointer]) begin
         mem[{bank, bank_row[bank], write_column[queue_pointer]}] <= SD_DQ;
         $display("P:mem[%h] <= %h", {bank, bank_row[bank], write_column[queue_pointer]},
                  SD_DQ);
      end
      if (dq_read[queue_pointer + 4'h1]) begin
         dq1 <= mem[{bank, bank_row[bank], read_column[queue_pointer + 4'h1]}];
         $display("P:%h <= mem[%h]", mem[{bank, bank_row[bank], read_column[queue_pointer + 4'h1]}],
                  {bank, bank_row[bank], read_column[queue_pointer + 4'h1]});
      end
   end

   always @(negedge SD_LDQS_P) begin
      if (dq_write[queue_pointer]) begin
         mem[{bank, bank_row[bank], write_column[queue_pointer] + 10'h1}] <= SD_DQ;
         $display("N:mem[%h] <= %h", {bank, bank_row[bank], write_column[queue_pointer] + 10'h1},
                  SD_DQ);
      end
      if (dq_read[queue_pointer + 4'h1]) begin
         dq0 <= mem[{bank, bank_row[bank], read_column[queue_pointer + 4'h1] + 10'h1}];
         $display("N:%h <= mem[%h]", mem[{bank, bank_row[bank], read_column[queue_pointer + 4'h1] + 10'h1}],
                  {bank, bank_row[bank], read_column[queue_pointer + 4'h1] + 10'h1});
      end
   end

   wire write_flag = dq_write[queue_pointer];
   wire read_flag = dq_read[queue_pointer];
   assign SD_DQ = dq_read[queue_pointer] ? (SD_CK_P ? dq1 : dq0) : 16'bz;
   assign SD_LDQS_P = dq_read[queue_pointer] ? SD_CK_P : 1'bz;
   assign SD_LDQS_N = dq_read[queue_pointer] ? SD_CK_N : 1'bz;
endmodule
