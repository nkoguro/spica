`timescale 1ns / 1ps

`define MICROCODE "./microcode/memcheck.hex"

module spica(/*AUTOARG*/
  // Outputs
  VGA_VSYNC, VGA_R, VGA_HSYNC, VGA_G, VGA_B, SD_WE, SD_UDM, SD_RAS,
  SD_LDM, SD_CS, SD_CAS, SD_BA, SD_A, LED, LCD_RW, LCD_RS, LCD_E,
  SD_CK_P, SD_CK_N, SD_CKE, SD_ODT, DTE_TXD, DCE_TXD,
  // Inouts
  SD_UDQS_P, SD_LDQS_P, SD_DQ, LCD_DB, J20_IO, J18_IO,
  // Inputs
  SW, ROT_CENTER, ROT_B, ROT_A, BTN_WEST, BTN_SOUTH, BTN_NORTH,
  BTN_EAST, CLK_50MHz, DTE_RXD, DCE_RXD
  );

  input CLK_50MHz;
  output SD_CK_P;
  output SD_CK_N;
  output SD_CKE;
  output SD_ODT;
  input DTE_RXD;
  output DTE_TXD;
  input DCE_RXD;
  output DCE_TXD;
  inout [4:1] J20_IO;
  inout [1:1] J18_IO;
  
  /*AUTOINPUT*/
  // Beginning of automatic inputs (from unused autoinst inputs)
  input                 BTN_EAST;               // To io_controller of io_controller.v
  input                 BTN_NORTH;              // To io_controller of io_controller.v
  input                 BTN_SOUTH;              // To io_controller of io_controller.v
  input                 BTN_WEST;               // To io_controller of io_controller.v
  input                 ROT_A;                  // To io_controller of io_controller.v
  input                 ROT_B;                  // To io_controller of io_controller.v
  input                 ROT_CENTER;             // To io_controller of io_controller.v
  input [3:0]           SW;                     // To io_controller of io_controller.v
  // End of automatics
  /*AUTOOUTPUT*/
  // Beginning of automatic outputs (from unused autoinst outputs)
  output                LCD_E;                  // From lcd of lcd.v
  output                LCD_RS;                 // From lcd of lcd.v
  output                LCD_RW;                 // From lcd of lcd.v
  output [7:0]          LED;                    // From io_controller of io_controller.v
  output [15:0]         SD_A;                   // From memory_controller of memory_controller.v
  output [2:0]          SD_BA;                  // From memory_controller of memory_controller.v
  output                SD_CAS;                 // From memory_controller of memory_controller.v
  output                SD_CS;                  // From memory_controller of memory_controller.v
  output                SD_LDM;                 // From memory_controller of memory_controller.v
  output                SD_RAS;                 // From memory_controller of memory_controller.v
  output                SD_UDM;                 // From memory_controller of memory_controller.v
  output                SD_WE;                  // From memory_controller of memory_controller.v
  output [3:0]          VGA_B;                  // From crtc of crtc.v
  output [3:0]          VGA_G;                  // From crtc of crtc.v
  output                VGA_HSYNC;              // From crtc of crtc.v
  output [3:0]          VGA_R;                  // From crtc of crtc.v
  output                VGA_VSYNC;              // From crtc of crtc.v
  // End of automatics
  /*AUTOINOUT*/
  // Beginning of automatic inouts (from unused autoinst inouts)
  inout [7:4]           LCD_DB;                 // To/From lcd of lcd.v
  inout [15:0]          SD_DQ;                  // To/From memory_controller of memory_controller.v
  inout                 SD_LDQS_P;              // To/From memory_controller of memory_controller.v
  inout                 SD_UDQS_P;              // To/From memory_controller of memory_controller.v
  // End of automatics
  /*AUTOWIRE*/
  // Beginning of automatic wires (for undeclared instantiated-module outputs)
  wire [15:0]           addr;                   // From micro_spica of micro_spica.v
  wire [31:0]           read_data;              // From selector of selector.v
  wire [31:0]           write_data;             // From micro_spica of micro_spica.v
  wire                  write_enable;           // From micro_spica of micro_spica.v
  // End of automatics

  wire [11:0] pc;
  wire [31:0] program_code;

  wire read_request;
  wire exec_n;
  wire cs_n;
  wire wr_n;
  wire rd_n;
  wire [21:0] mem_addr;
  wire [31:0] mem_data;
  
  wire main_write_enable;
  wire [11:0] main_sel_addr;
  wire [31:0] main_write_data;
  wire [31:0] main_read_data;

  wire cgram_write_enable;
  wire [11:0] cgram_sel_addr;
  wire [11:0] cgram_addr;
  wire [7:0] cgram_data;
  wire [7:0] cgram_write_data;
  wire [7:0] cgram_read_data;

  wire tvram_write_enable;
  wire [11:0] tvram_sel_addr;
  wire [11:0] tvram_addr;
  wire [15:0] tvram_data;
  wire [15:0] tvram_write_data;
  wire [15:0] tvram_read_data;

  wire io_write_enable;
  wire [7:0] io_sel_addr;
  wire [7:0] io_write_data;
  wire [7:0] io_read_data;

  wire lcd_write_enable;
  wire [4:0] lcd_sel_addr;
  wire [4:0] lcd_addr;
  wire [7:0] lcd_data;
  wire [7:0] lcd_write_data;
  wire [7:0] lcd_read_data;

  wire crtc_cs_n;
  wire crtc_rd_n;
  wire [21:0] gvram_addr;
  wire crtc_exec_n;

  wire cpu_mem_exec_n;
  wire cpu_mem_rd_n;
  wire cpu_mem_wr_n;
  wire [23:0] cpu_mem_addr;

  wire [3:0] serial0_addr;
  wire serial0_read_request;
  wire [7:0] serial0_read_data;
  wire serial0_write_enable;
  wire [7:0] serial0_write_data;

  wire [3:0] serial1_addr;
  wire serial1_read_request;
  wire [7:0] serial1_read_data;
  wire serial1_write_enable;
  wire [7:0] serial1_write_data;

  wire [8:0] sd_addr;
  wire [31:0] sd_read_data;
  wire sd_write_enable;
  wire [31:0] sd_write_data;

  wire locked;
  wire dcm_clk_in;
  wire dcm_clk0;
  wire dcm_clk100;
  wire dcm_clk100_n;
  wire clk50;
  wire clk100;
  wire ck_p;
  wire ck_n;
  
  IBUFG U_IBUFG (.I(CLK_50MHz), .O(dcm_clk_in));
  BUFG U_BUFG (.I(dcm_clk0), .O(clk50));
  BUFG bufg_clk100 (.I(dcm_clk100), .O(clk100));
  BUFG bufg_ck_p(.I(dcm_clk100), .O(ck_n));
  BUFG bufg_ck_n(.I(dcm_clk100_n), .O(ck_p));
  
  DCM_SP #(
           .CLKDV_DIVIDE(2.0), // Divide by: 1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5
           //   7.0,7.5,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0 or 16.0
           .CLKFX_DIVIDE(1),   // Can be any integer from 1 to 32
           .CLKFX_MULTIPLY(4), // Can be any integer from 2 to 32
           .CLKIN_DIVIDE_BY_2("FALSE"), // TRUE/FALSE to enable CLKIN divide by two feature
           .CLKIN_PERIOD(20.0),  // Specify period of input clock
           .CLKOUT_PHASE_SHIFT("NONE"), // Specify phase shift of NONE, FIXED or VARIABLE
           .CLK_FEEDBACK("2X"),  // Specify clock feedback of NONE, 1X or 2X
           .DESKEW_ADJUST("SYSTEM_SYNCHRONOUS"), // SOURCE_SYNCHRONOUS, SYSTEM_SYNCHRONOUS or
           //   an integer from 0 to 15
           .DLL_FREQUENCY_MODE("LOW"),  // HIGH or LOW frequency mode for DLL
           .DUTY_CYCLE_CORRECTION("TRUE"), // Duty cycle correction, TRUE or FALSE
           .PHASE_SHIFT(0),     // Amount of fixed phase shift from -255 to 255
           .STARTUP_WAIT("FALSE")   // Delay configuration DONE until DCM LOCK, TRUE/FALSE
           )
  DCM_SP_inst (
               .CLK0(dcm_clk0),     // 0 degree DCM CLK output
               // .CLK180(), // 180 degree DCM CLK output
               // .CLK270(), // 270 degree DCM CLK output
               .CLK2X(dcm_clk100),   // 2X DCM CLK output
               .CLK2X180(dcm_clk100_n), // 2X, 180 degree DCM CLK out
               // .CLK90(),   // 90 degree DCM CLK output
               // .CLKDV(),   // Divided DCM CLK out (CLKDV_DIVIDE)
               // .CLKFX(),   // DCM CLK synthesis out (M/D)
               // .CLKFX180(), // 180 degree CLK synthesis out
               .LOCKED(locked), // DCM LOCK status output
               // .PSDONE(), // Dynamic phase adjust done output
               // .STATUS(dcm_status), // 8-bit DCM status bits output
               .CLKFB(clk100),   // DCM clock feedback
               .CLKIN(dcm_clk_in),   // Clock input (from IBUFG, BUFG or DCM)
               .PSCLK(1'b0),   // Dynamic phase adjust clock input
               .PSEN(1'b0),     // Dynamic phase adjust enable input
               .PSINCDEC(1'b0), // Dynamic phase adjust increment/decrement
               .RST(1'b0)        // DCM asynchronous reset input
               );

  function cpu_mem_cs_n;
    input [1:0] addr_top;
    case (addr_top)
      2'b00:   cpu_mem_cs_n = 1'b0;
      default: cpu_mem_cs_n = 1'b1;
    endcase
  endfunction
  
  micro_spica micro_spica(.clk(clk50), .rst(1'b0),
                          .pc(pc), .program_code(program_code),
                          .read_request(read_request),
                          .mem_read_n (cpu_mem_rd_n),
                          .mem_write_n (cpu_mem_wr_n),
                          .mem_exec_n (cpu_mem_exec_n),
                          .mem_addr (cpu_mem_addr[23:0]),
                          .mem_data (mem_data[31:0]),
                          /*AUTOINST*/
                          // Outputs
                          .addr                 (addr[15:0]),
                          .write_enable         (write_enable),
                          .write_data           (write_data[31:0]),
                          // Inputs
                          .read_data            (read_data[31:0]));

  memory_controller memory_controller(.clk100(clk100),.rst(1'b0),
                                      .dqs_p(ck_p),
                                      .data            (mem_data),
                                      .exec_n          (exec_n),
                                      /*AUTOINST*/
                                      // Outputs
                                      .SD_A             (SD_A[15:0]),
                                      .SD_BA            (SD_BA[2:0]),
                                      .SD_RAS           (SD_RAS),
                                      .SD_CAS           (SD_CAS),
                                      .SD_WE            (SD_WE),
                                      .SD_CS            (SD_CS),
                                      .SD_UDM           (SD_UDM),
                                      .SD_LDM           (SD_LDM),
                                      // Inouts
                                      .SD_DQ            (SD_DQ[15:0]),
                                      .SD_LDQS_P        (SD_LDQS_P),
                                      .SD_UDQS_P        (SD_UDQS_P),
                                      // Inputs
                                      .cs_n             (cs_n),
                                      .wr_n             (wr_n),
                                      .rd_n             (rd_n),
                                      .mem_addr         (mem_addr[21:0]));
  
  selector selector(.clk(clk50),
                    .read_request(read_request),
    
                    .main_addr(main_sel_addr),
                    .main_write_enable(main_write_enable),
                    .main_write_data(main_write_data),
                    .main_read_data(main_read_data),
    
                    .cgram_addr(cgram_sel_addr),
                    .cgram_write_enable(cgram_write_enable),
                    .cgram_write_data(cgram_write_data),
                    .cgram_read_data(cgram_read_data),

                    .tvram_addr(tvram_sel_addr),
                    .tvram_write_enable(tvram_write_enable),
                    .tvram_write_data(tvram_write_data),
                    .tvram_read_data(tvram_read_data),

                    .serial0_addr(serial0_addr),
                    .serial0_write_enable(serial0_write_enable),
                    .serial0_write_data(serial0_write_data),
                    .serial0_read_request(serial0_read_request),
                    .serial0_read_data(serial0_read_data),

                    .serial1_addr(serial1_addr),
                    .serial1_write_enable(serial1_write_enable),
                    .serial1_write_data(serial1_write_data),
                    .serial1_read_request(serial1_read_request),
                    .serial1_read_data(serial1_read_data),

                    .sd_addr(sd_addr),
                    .sd_write_enable(sd_write_enable),
                    .sd_write_data(sd_write_data),
                    .sd_read_data(sd_read_data),

                    .io_addr(io_sel_addr),
                    .io_write_enable(io_write_enable),
                    .io_write_data(io_write_data),
                    .io_read_data(io_read_data),
    
                    .lcd_addr (lcd_sel_addr),
                    .lcd_write_enable(lcd_write_enable),
                    .lcd_write_data(lcd_write_data),
                    .lcd_read_data(lcd_read_data),
                    /*AUTOINST*/
                    // Outputs
                    .read_data          (read_data[31:0]),
                    // Inputs
                    .write_enable       (write_enable),
                    .addr               (addr[15:0]),
                    .write_data         (write_data[31:0]));

  dual_port_ram #(.ADDR_BITS(12), .DATA_BITS(32), .DATAFILE(`MICROCODE))
  main_ram(.clk1(clk50), .clk2(clk50),
           .addr1(main_sel_addr),
           .write_enable1(main_write_enable),
           .data_in1(main_write_data),
           .data_out1(main_read_data),
           .addr2(pc),
           .data_out2(program_code));
  
  dual_port_ram #(.ADDR_BITS(12), .DATA_BITS(16))
  tvram(.clk1(clk50),.clk2(clk50),
        .addr1(tvram_sel_addr),
        .write_enable1(tvram_write_enable),
        .data_in1(tvram_write_data),
        .data_out1(tvram_read_data),
        .addr2(tvram_addr),
        .data_out2(tvram_data));

  dual_port_ram #(.ADDR_BITS(12), .DATA_BITS(8), .DATAFILE("./7x14.hex"))
  cgram(.clk1(clk50), .clk2(clk50),
        .addr1(cgram_sel_addr),
        .write_enable1(cgram_write_enable),
        .data_in1(cgram_write_data),
        .data_out1(cgram_read_data),
        .addr2(cgram_addr),
        .data_out2(cgram_data));

  memory_arbiter memory_arbiter(.clk100(clk100),
                                .cs_n(cs_n),
                                .rd_n(rd_n),
                                .wr_n(wr_n),
                                .exec_n(exec_n),
                                .addr(mem_addr),

                                .cs0_n(crtc_cs_n),
                                .rd0_n(crtc_rd_n),
                                .wr0_n(1'b1),
                                .exec0_n(crtc_exec_n),
                                .addr0(gvram_addr),
    
                                .cs1_n(cpu_mem_cs_n(cpu_mem_addr[23:22])),
                                .rd1_n(cpu_mem_rd_n),
                                .wr1_n(cpu_mem_wr_n),
                                .exec1_n(cpu_mem_exec_n),
                                .addr1(cpu_mem_addr[21:0])
                                );

  crtc crtc(.clk50(clk50), 
            .tvram_addr(tvram_addr),
            .tvram_data(tvram_data),
            .cgram_addr(cgram_addr),
            .cgram_data(cgram_data),
            .mem_rd_n(crtc_rd_n),
            .mem_cs_n(crtc_cs_n),
            .exec_n(crtc_exec_n),
            .gvram_addr(gvram_addr),
            .graphic_enable(SW[3]),
            .gvram_data(mem_data),
            /*AUTOINST*/
            // Outputs
            .VGA_R                      (VGA_R[3:0]),
            .VGA_G                      (VGA_G[3:0]),
            .VGA_B                      (VGA_B[3:0]),
            .VGA_HSYNC                  (VGA_HSYNC),
            .VGA_VSYNC                  (VGA_VSYNC));

  io_controller io_controller(.clk(clk50),
                              .io_addr(io_sel_addr),
                              .io_write_enable(io_write_enable),
                              .io_write_data(io_write_data),
                              .io_read_data(io_read_data),
                              /*AUTOINST*/
                              // Outputs
                              .LED              (LED[7:0]),
                              // Inputs
                              .SW               (SW[3:0]),
                              .BTN_NORTH        (BTN_NORTH),
                              .BTN_SOUTH        (BTN_SOUTH),
                              .BTN_EAST         (BTN_EAST),
                              .BTN_WEST         (BTN_WEST),
                              .ROT_A            (ROT_A),
                              .ROT_B            (ROT_B),
                              .ROT_CENTER       (ROT_CENTER));

  dual_port_ram #(.ADDR_BITS(5), .DATA_BITS(8))
  lcd_ram(.clk1 (clk50), .clk2 (clk50),
          .write_enable1(lcd_write_enable),
          .addr1(lcd_sel_addr), .data_in1(lcd_write_data), .data_out1(lcd_read_data),
          .addr2(lcd_addr), .data_out2(lcd_data));

  lcd lcd(.clk(clk50), .addr(lcd_addr), .data(lcd_data),
          /*AUTOINST*/
          // Outputs
          .LCD_E                        (LCD_E),
          .LCD_RS                       (LCD_RS),
          .LCD_RW                       (LCD_RW),
          // Inouts
          .LCD_DB                       (LCD_DB[7:4]));

  serial serial0(.clk(clk50),
                 .TXD(DCE_TXD),
                 .read_data(serial0_read_data),
                 .RXD(DCE_RXD),
                 .write_enable(serial0_write_enable),
                 .read_request(serial0_read_request),
                 .addr(serial0_addr),
                 .write_data(serial0_write_data));
  
  serial serial1(.clk(clk50),
                 .TXD(DTE_TXD),
                 .read_data(serial1_read_data),
                 .RXD(DTE_RXD),
                 .write_enable(serial1_write_enable),
                 .read_request(serial1_read_request),
                 .addr(serial1_addr),
                 .write_data(serial1_write_data));

  sdcard_controller sdcard_controller(.clk50(clk50),
                                      .cd_n(J18_IO[1]),
                                      .sd_data_in(J20_IO[3]),
                                      .sd_data_out(J20_IO[1]),
                                      .sd_cs(J20_IO[4]),
                                      .sd_clk(J20_IO[2]),
                                      .read_data(sd_read_data),
                                      .write_enable(sd_write_enable),
                                      .addr(sd_addr),
                                      .write_data(sd_write_data));

  assign SD_CK_P = ck_p;
  assign SD_CK_N = ck_n;
  assign SD_CKE = locked;
  assign SD_ODT = 0;
  assign J20_IO[1] = 1'bz;
endmodule // spica
