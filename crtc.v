`timescale 1ns/1ps

module crtc(/*AUTOARG*/
  // Outputs
  tvram_addr, cgram_addr, VGA_R, VGA_G, VGA_B, VGA_HSYNC, VGA_VSYNC,
  mem_rd_n, gvram_addr, mem_cs_n,
  // Inputs
  clk50, tvram_data, cgram_data, exec_n, graphic_enable, gvram_data
  );
   input clk50;

   input [15:0] tvram_data;
   input [7:0] cgram_data;
   
   output reg [11:0] tvram_addr;
   output reg [11:0] cgram_addr;
   output [3:0] VGA_R;
   output [3:0] VGA_G;
   output [3:0] VGA_B;
   output reg VGA_HSYNC = 1;
   output reg VGA_VSYNC = 1;

   input exec_n;
   input graphic_enable;
   input [31:0] gvram_data;
   output reg mem_rd_n = 1;
   output reg [21:0] gvram_addr;
   output reg mem_cs_n = 1;
   
   // Dot Clock = 25MHz (50MHz/2)
   parameter V_DISP = 384000 * 2;
   parameter V_PULSE_WIDTH = 1600 * 2;
   parameter V_FRONT_PORCH = 8000 * 2;
   parameter V_BACK_PORCH = 23200 * 2;

   parameter H_DISP = 640 * 2;
   parameter H_PULSE_WIDTH = 96 * 2;
   parameter H_FRONT_PORCH = 16 * 2;
   parameter H_BACK_PORCH = 48 * 2;

   parameter MAX_COLUMNS = 80;
   parameter MAX_ROWS = 30;

   parameter CURSOR_POS_ADDR = 12'hFFF;
   
   reg [10:0] h_counter = H_PULSE_WIDTH + H_BACK_PORCH;
   reg [19:0] v_counter = V_PULSE_WIDTH + V_BACK_PORCH;
   reg [4:0] blink_counter = 5'h0; // blink cycle is 32/60 sec (= about 0.5 sec).
   reg blink = 0;
   reg cursor_mask = 0;
   reg h_disp_flag = 1;
   reg v_disp_flag = 1;
   
   reg [31:0] r;
   reg [31:0] g;
   reg [31:0] b;
   reg [11:0] row_addr = 12'h0;
   wire [10:0] x = h_counter - H_PULSE_WIDTH - H_BACK_PORCH + 16;
   reg [8:0] y = 9'h0;
   reg [11:0] cursor_pos = 12'h0;
   
   function [3:0] level;
      input [1:0] code;
      case (code)
        2'h0: level = 4'b0000;
        2'h1: level = 4'b0011;
        2'h2: level = 4'b1111;
        default: level = 4'bxxxx;
      endcase
   endfunction
      
   function [11:0] color2rgb;
      input [3:0] color;
      color2rgb = {level({color[3:3], color[2:2]}),
                   level({color[3:3], color[1:1]}),
                   level({color[3:3], color[0:0]})
                   };
   endfunction

   function [11:0] attr2rgb;
      input [7:0] attr;
      input dot;
      attr2rgb = color2rgb(attr[7:4]) | ({12{dot}} & color2rgb(~attr[3:0]));
   endfunction

   parameter
    GVRAM_FETCH_IDLE = 2'h0,
     GVRAM_FETCH_REQUEST_READ = 2'h1,
     GVRAM_FETCH_READ_DATA = 2'h2;
   parameter BUFFER_MAX = 8'h9f;
   parameter GVRAM_START_ADDR = 22'h3c0000;
   reg [1:0] gvram_fetch_status = GVRAM_FETCH_IDLE;
   reg [7:0] buf_in_addr;
   reg [7:0] buf_out_addr;
   reg [31:0] gdata;
   reg [31:0] gvram_buffer[0:159];

   function [3:0] level2;
      input [1:0] in;
      level2 = {in, in};
   endfunction

   function [3:0] level3;
      input [2:0] in;
      level3 = {in, in[1]};
   endfunction
   
   always @(posedge clk50) begin
      h_counter <= h_counter + 1;

      if (h_counter == H_PULSE_WIDTH - 1) begin
         VGA_HSYNC <= 1;
      end

      if (h_counter == H_PULSE_WIDTH + H_BACK_PORCH - 1) begin
         h_disp_flag <= 1;
      end

      if (h_counter == H_PULSE_WIDTH + H_BACK_PORCH + H_DISP - 1) begin
         h_disp_flag <= 0;
      end

      if (h_counter == H_PULSE_WIDTH + H_BACK_PORCH + H_DISP + H_FRONT_PORCH - 1) begin
         VGA_HSYNC <= 0;
         h_counter <= 0;
      end
   end
   
   always @(posedge clk50) begin
      v_counter <= v_counter + 1;

      if (v_counter == V_PULSE_WIDTH - 1) begin
         VGA_VSYNC <= 1;
      end

      if (v_counter == V_PULSE_WIDTH + V_BACK_PORCH - 1) begin
         v_disp_flag <= 1;
      end

      if (v_counter == V_PULSE_WIDTH + V_BACK_PORCH + V_DISP - 1) begin
         v_disp_flag <= 0;
      end

      if (v_counter == V_PULSE_WIDTH + V_BACK_PORCH + V_DISP + V_FRONT_PORCH - 1) begin
         VGA_VSYNC <= 0;
         v_counter <= 0;
      end
   end

   always @(posedge clk50) begin
      if (h_counter == H_PULSE_WIDTH + H_BACK_PORCH + H_DISP - 1) begin
         if (v_disp_flag) begin
            y <= y + 1;
         end
         else begin
            y <= 9'h0;
            row_addr <= 12'h0;
         end

         if (y[3:0] == 4'hF) row_addr <= row_addr + MAX_COLUMNS;
      end
   end

   always @(posedge clk50) begin
      if (v_counter == 0) begin
         blink_counter <= blink_counter + 1;

         if (blink_counter == 0) blink = ~blink;
      end
   end

   always @(posedge clk50) begin
      case (gvram_fetch_status)
        GVRAM_FETCH_IDLE: begin
          mem_rd_n <= 1;
           if (graphic_enable & v_disp_flag & h_counter == H_PULSE_WIDTH - 1) begin
              mem_cs_n <= 0;
              buf_in_addr <= 8'hff;
              gvram_fetch_status <= GVRAM_FETCH_REQUEST_READ;
           end
        end
        GVRAM_FETCH_REQUEST_READ: begin
          mem_rd_n <= 0;
           if (!exec_n) begin
              buf_in_addr <= buf_in_addr + 1;
              gvram_fetch_status <= GVRAM_FETCH_READ_DATA;
           end
        end
        GVRAM_FETCH_READ_DATA: begin
           if (exec_n) begin
             gvram_buffer[buf_in_addr] <= gvram_data;
             gvram_addr <= gvram_addr + 1;
             mem_rd_n <= 1;
             if (buf_in_addr == BUFFER_MAX) begin
               mem_cs_n <= 1;
               gvram_fetch_status <= GVRAM_FETCH_IDLE;
             end
             else begin
               gvram_fetch_status <= GVRAM_FETCH_REQUEST_READ;
             end
           end
        end
        default: begin
           mem_rd_n <= 1;
           mem_cs_n <= 1;
           gvram_fetch_status <= GVRAM_FETCH_IDLE;
        end
      endcase

      if (!v_disp_flag) begin
         gvram_addr <= GVRAM_START_ADDR;
      end
   end

   always @(posedge clk50) begin
      if (graphic_enable & v_disp_flag) begin
         if (h_counter == H_PULSE_WIDTH + H_BACK_PORCH - 3) begin
            buf_out_addr <= 8'h0;
         end
         if (h_counter[2:0] == 3'b111) begin
            gdata <= gvram_buffer[buf_out_addr];
            buf_out_addr <= buf_out_addr + 1;
         end
         else if (h_counter[0] == 1'b1) begin
            gdata <= {gdata[23:0], 8'h00};
         end
      end
      if (!graphic_enable | !v_disp_flag) begin
         gdata <= 32'h0;
      end
   end
   
   always @(posedge clk50) begin
      if (x[0]) begin
         r <= {4'b0000, r[31:4]};
         g <= {4'b0000, g[31:4]};
         b <= {4'b0000, b[31:4]};
      end

      if (x[3:0] == 4'h0) begin
         tvram_addr <= CURSOR_POS_ADDR;
      end

      if (x[3:0] == 4'h2) begin
         cursor_pos <= tvram_data[11:0];
      end
      
      if (x[3:0] == 4'h4) begin
         tvram_addr <= row_addr + x[10:4];
      end

      if (x[3:0] == 4'h6) begin
         cursor_mask <= blink & (tvram_addr == cursor_pos);
      end
      
      if (x[3:0] == 4'h8) begin
         cgram_addr <= {tvram_data[7:0], y[3:0]};
      end

      if (x[3:0] == 4'hF) begin
         {r[ 3: 0], g[ 3: 0], b[ 3: 0]} <= attr2rgb(tvram_data[15:8], cgram_data[7]) ^ {12{cursor_mask}};
         {r[ 7: 4], g[ 7: 4], b[ 7: 4]} <= attr2rgb(tvram_data[15:8], cgram_data[6]) ^ {12{cursor_mask}};
         {r[11: 8], g[11: 8], b[11: 8]} <= attr2rgb(tvram_data[15:8], cgram_data[5]) ^ {12{cursor_mask}};
         {r[15:12], g[15:12], b[15:12]} <= attr2rgb(tvram_data[15:8], cgram_data[4]) ^ {12{cursor_mask}};
         {r[19:16], g[19:16], b[19:16]} <= attr2rgb(tvram_data[15:8], cgram_data[3]) ^ {12{cursor_mask}};
         {r[23:20], g[23:20], b[23:20]} <= attr2rgb(tvram_data[15:8], cgram_data[2]) ^ {12{cursor_mask}};
         {r[27:24], g[27:24], b[27:24]} <= attr2rgb(tvram_data[15:8], cgram_data[1]) ^ {12{cursor_mask}};
         {r[31:28], g[31:28], b[31:28]} <= attr2rgb(tvram_data[15:8], cgram_data[0]) ^ {12{cursor_mask}};
      end
   end

   assign VGA_R = (h_disp_flag & v_disp_flag) ? r[3:0] | level3(gdata[31:29]) : 4'b0000;
   assign VGA_G = (h_disp_flag & v_disp_flag) ? g[3:0] | level3(gdata[28:26]) : 4'b0000;
   assign VGA_B = (h_disp_flag & v_disp_flag) ? b[3:0] | level2(gdata[25:24]) : 4'b0000;
endmodule
