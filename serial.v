`timescale 1ns / 1ps

module serial(/*AUTOARG*/
   // Outputs
   TXD, read_data,
   // Inputs
   RXD, clk, write_enable, read_request, addr, write_data
   );

   parameter CYCLE_BITS = 20;
   parameter CLOCK = 50;
   parameter
    CYCLE_50BPS = CLOCK * 1000 * 1000 / 50,
     CYCLE_75BPS = CLOCK * 1000 * 1000 / 75,
     CYCLE_110BPS = CLOCK * 1000 * 1000 / 110,
     CYCLE_150BPS = CLOCK * 1000 * 1000 / 150,
     CYCLE_300BPS = CLOCK * 1000 * 1000 / 300,
     CYCLE_600BPS = CLOCK * 1000 * 1000 / 600,
     CYCLE_1200BPS = CLOCK * 1000 * 1000 / 1200,
     CYCLE_1800BPS = CLOCK * 1000 * 1000 / 1800,
     CYCLE_2000BPS = CLOCK * 1000 * 1000 / 2000,
     CYCLE_2400BPS = CLOCK * 1000 * 1000 / 2400,
     CYCLE_3600BPS = CLOCK * 1000 * 1000 / 3600,
     CYCLE_4800BPS = CLOCK * 1000 * 1000 / 4800,
     CYCLE_7200BPS = CLOCK * 1000 * 1000 / 7200,
     CYCLE_9600BPS = CLOCK * 1000 * 1000 / 9600,
     CYCLE_19200BPS = CLOCK * 1000 * 1000 / 19200,
     CYCLE_38400BPS = CLOCK * 1000 * 1000 / 38400,
     CYCLE_57600BPS = CLOCK * 1000 * 1000 / 57600,
     CYCLE_76800BPS = CLOCK * 1000 * 1000 / 76800,
     CYCLE_115200BPS = CLOCK * 1000 * 1000 / 115200;
      
   input RXD;
   output reg TXD = 1;
   
   input clk;
   input write_enable;
   input read_request;
   input [3:0] addr;
   input [7:0] write_data;
   output reg [7:0] read_data;

   parameter WRITE_IDLE = 2'h0,
              WRITE_START = 2'h1,
              WRITE_DATA = 2'h2,
              WRITE_STOP = 2'h3;

   reg [1:0] rxds = 2'b0;
   reg read_active = 0;
   reg [1:0] write_status = WRITE_IDLE;
   reg [3:0] read_bits;
   reg [3:0] write_bits;
   reg [7:0] reading_data;
   reg [7:0] writing_data;
   
   reg [CYCLE_BITS-1:0] cycle = CYCLE_9600BPS;
   reg [CYCLE_BITS-1:0] read_counter = 0;
   reg [CYCLE_BITS-1:0] write_counter = 0;
   
   reg overrun_flag = 0;
   reg framing_error_flag = 0;
   
   reg [3:0] read_buf_in_addr = 0;
   reg [3:0] read_buf_out_addr = 0;
   (* ram_style = "distributed" *) reg [7:0] read_buf_data[0:15];

   reg [3:0] write_buf_in_addr = 0;
   reg [3:0] write_buf_out_addr = 0;
   (* ram_style = "distributed" *) reg [7:0] write_buf_data[0:15];

   reg rst = 0;

   always @(posedge clk) begin
      rxds <= {rxds[0], RXD};
   end
   
   always @(posedge clk) begin
      if (rst) begin
         read_active <= 0;
         read_buf_in_addr <= 0;
         overrun_flag <= 0;
         framing_error_flag <= 0;
      end
      else begin
         if (read_active == 1'b0) begin
            if (rxds == 2'b10) begin
               read_active <= 1;
               read_counter <= cycle + (cycle >> 1) - 1;
               read_bits <= 0;
            end
         end
         else begin
            if (read_counter == 0) begin
               if (read_bits == 4'h8) begin
                  read_active <= 0;
                  if (rxds[0] == 1'b1) begin
                     framing_error_flag <= 0;
                     if ((read_buf_in_addr+4'h1) == read_buf_out_addr) begin
                        overrun_flag <= 1;
                     end
                     else begin
                        overrun_flag <= 0;
                        read_buf_data[read_buf_in_addr] <= reading_data;
                        read_buf_in_addr <= read_buf_in_addr + 1;
                     end
                  end // if (rxds[0] == 1'b1)               
                  else begin
                     framing_error_flag <= 1;
                  end // else: !if(RXD == 1)
               end // if (read_bits == 3'h9)
               else begin
                  read_counter <= cycle - 1;
                  read_bits <= read_bits + 1;
                  reading_data <= {rxds[0], reading_data[7:1]};
               end // else: !if(read_bits == 4'h8)
            end // if (read_counter == 0)
            else begin
               read_counter <= read_counter - 1;
            end // else: !if(read_counter == 0)
         end // else: !if(read_active == 1'b0)
      end // else: !if(rst)
   end // always @ (posedge clk)
   
   always @(posedge clk) begin
      if (rst) begin
         write_status <= WRITE_IDLE;
         write_counter <= 0;
         write_buf_out_addr <= 0;
      end
      else begin
         if (write_counter == 0) begin
            case (write_status)
              WRITE_IDLE: begin
                 TXD <= 1;
                 if (write_buf_in_addr != write_buf_out_addr) begin
                    write_status <= WRITE_START;
                 end
              end
              WRITE_START: begin
                 TXD <= 0;
                 write_counter <= cycle - 1;

                 write_status <= WRITE_DATA;
                 write_bits <= 0;
                 write_buf_out_addr <= write_buf_out_addr + 1;
                 writing_data <= write_buf_data[write_buf_out_addr];
              end
              WRITE_DATA: begin
                 TXD <= writing_data[0];
                 write_counter <= cycle - 1;

                 writing_data <= {1'b1, writing_data[7:1]};
                 write_bits <= write_bits + 1;
                 if (write_bits == 3'h7) begin
                    write_status <= WRITE_STOP;
                 end
              end
              WRITE_STOP: begin
                 TXD <= 1;
                 write_counter <= cycle - 1;

                 write_status <= WRITE_IDLE;
              end
              default: begin
                 write_status <= WRITE_IDLE;
              end
            endcase
         end // if (write_counter == 0)
         else begin
            write_counter <= write_counter - 1;
         end // else: !if(write_counter == 0)
      end // else: !if(rst)
   end // always @ (posedge clk)

   always @(posedge clk) begin
      if (rst) begin
         read_buf_out_addr <= 0;
      end
      else begin
         if (read_request) begin
            case (addr)
              4'h0: begin
                 if (read_buf_in_addr == read_buf_out_addr) begin
                    read_data <= 8'h00;
                 end
                 else begin
                    read_data <= read_buf_data[read_buf_out_addr];
                    read_buf_out_addr <= read_buf_out_addr + 1;
                 end
              end
              4'h1: begin
                 read_data <= {6'b0, overrun_flag, framing_error_flag};
              end
              4'h2: begin
                 read_data <= read_buf_in_addr - read_buf_out_addr;
              end
              4'h3: begin
                 read_data <= write_buf_in_addr - write_buf_out_addr;
              end
              4'h4: begin
                 read_data <= {5'b0, write_status, read_active};
              end
              4'hf: begin
                 read_data <= {3'b0, TXD, 2'b0, rxds};
              end
              default: begin
                 read_data <= 8'h00;
              end
            endcase
         end // if (read_request)
      end // else: !if(rst)
   end

   always @(posedge clk) begin
      if (rst) begin
         rst <= 0;
         write_buf_in_addr <= 0;
      end
      else begin 
         if (write_enable) begin
            case (addr)
              4'h0: begin
                 write_buf_data[write_buf_in_addr] <= write_data;
                 write_buf_in_addr <= write_buf_in_addr + 1;
              end
              4'h1: begin
                 case (write_data)
                   8'h00: cycle <= CYCLE_50BPS;
                   8'h01: cycle <= CYCLE_75BPS;
                   8'h02: cycle <= CYCLE_110BPS;
                   8'h03: cycle <= CYCLE_150BPS;
                   8'h04: cycle <= CYCLE_300BPS;
                   8'h05: cycle <= CYCLE_600BPS;
                   8'h06: cycle <= CYCLE_1200BPS;
                   8'h07: cycle <= CYCLE_1800BPS;
                   8'h08: cycle <= CYCLE_2000BPS;
                   8'h09: cycle <= CYCLE_2400BPS;
                   8'h0a: cycle <= CYCLE_3600BPS;
                   8'h0b: cycle <= CYCLE_4800BPS;
                   8'h0c: cycle <= CYCLE_7200BPS;
                   8'h0d: cycle <= CYCLE_9600BPS;
                   8'h0e: cycle <= CYCLE_19200BPS;
                   8'h0f: cycle <= CYCLE_38400BPS;
                   8'h10: cycle <= CYCLE_57600BPS;
                   8'h11: cycle <= CYCLE_76800BPS;
                   8'h12: cycle <= CYCLE_115200BPS;
                 endcase // case (write_data)
                 rst <= 1;
              end // case: 4'h1
            endcase // case (addr)
         end // if (write_enable)
      end // else: !if(rst)
   end // always @ (posedge clk)
   
endmodule
 