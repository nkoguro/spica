`timescale 1ns / 1ps

module lcd(/*AUTOARG*/
           // Outputs
           addr, LCD_E, LCD_RS, LCD_RW,
           // Inouts
           LCD_DB,
           // Inputs
           clk, data
           );
  input clk;
  output reg [4:0] addr;
  input [7:0] data;
  
  inout [7:4] LCD_DB;
  output reg LCD_E;
  output reg LCD_RS;
  output LCD_RW;

  reg [5:0] pc;
  reg [3:0] ah;
  reg [3:0] al;
  reg [31:0] wait_counter;
  reg [7:4] db;
  
  parameter
    WAIT_15000MICRO = 4'h0,
    WAIT_4100MICRO = 4'h1,
    WAIT_1600MICRO = 4'h2,
    WAIT_100MICRO = 4'h3,
    WAIT_40MICRO = 4'h4,
    WAIT_1MICRO = 4'h5;
  
  // op: 4bit, imm: 4bit
  //  or
  // op: 2bit, imm: 6bit
  parameter
    OP_NOP = 8'b00000000,
    OP_WAIT = 4'b0001,
    OP_READ = 4'b0010,
    OP_RSTA = 4'b0011,
    OP_CMDH = 4'b0100,
    OP_CMDL = 4'b0101,
    OP_DATH = 4'b0110,
    OP_DATL = 4'b0111,
    OP_JMP = 2'b10,
    OP_JNZ = 2'b11;
  
  function [7:0] code;
    input [5:0] pc;
    case (pc)
      6'h00: code = {OP_CMDH, 4'b0011}; 
      6'h01: code = {OP_WAIT, WAIT_4100MICRO};
      6'h02: code = {OP_CMDH, 4'b0011}; 
      6'h03: code = {OP_WAIT, WAIT_100MICRO};
      6'h04: code = {OP_CMDL, 4'b0011};
      6'h05: code = {OP_CMDL, 4'b0010}; 
      6'h06: code = {OP_CMDH, 4'b0010}; // Function Set (4bit) [High]
      6'h07: code = {OP_CMDL, 4'b1000}; // Function Set (2 lines & 5x7 dots) [Low]
      6'h08: code = {OP_CMDH, 4'b0000}; // Entry Mode Set [High]
      6'h09: code = {OP_CMDL, 4'b0110}; // Entry Mode Set (Increment & no-shift) [Low]
      6'h0A: code = {OP_CMDH, 4'b0000}; // Display On/Off [High]
      6'h0B: code = {OP_CMDL, 4'b1100}; // Display On/Off (Display is on, no-cursor & no-blink) [Low]
      6'h0C: code = {OP_CMDH, 4'b0000}; // Clear Display [High]
      6'h0D: code = {OP_CMDL, 4'b0001}; // Clear Display [Low]
      6'h0E: code = {OP_WAIT, WAIT_1600MICRO};

      6'h0F: code = {OP_RSTA, 4'b0000}; // Reset accumulator
      6'h10: code = {OP_CMDH, 4'b0100}; // CGRAM Address Set (char:0, row:0) [High]
      6'h11: code = {OP_CMDL, 4'b0000}; // CGRAM Address Set (char:0, row:0) [Low]
      6'h12: code = {OP_DATH, 4'b0000}; // CGRAM Data Write [High]
      6'h13: code = {OP_DATL, 4'b0000}; // CGRAM Data Write [Low]
      6'h14: code = {OP_CMDH, 4'b0100}; // CGRAM Address Set (char:0, row:1) [High]
      6'h15: code = {OP_CMDL, 4'b0001}; // CGRAM Address Set (char:0, row:1) [Low]
      6'h16: code = {OP_DATH, 4'b0000}; // CGRAM Data Write [High]
      6'h17: code = {OP_DATL, 4'b0000}; // CGRAM Data Write [Low]
      6'h18: code = {OP_CMDH, 4'b0100}; // CGRAM Address Set (char:0, row:2) [High]
      6'h19: code = {OP_CMDL, 4'b0010}; // CGRAM Address Set (char:0, row:2) [Low]
      6'h1A: code = {OP_DATH, 4'b0000}; // CGRAM Data Write [High]
      6'h1B: code = {OP_DATL, 4'b0000}; // CGRAM Data Write [Low]
      6'h1C: code = {OP_CMDH, 4'b0100}; // CGRAM Address Set (char:0, row:3) [High]
      6'h1D: code = {OP_CMDL, 4'b0011}; // CGRAM Address Set (char:0, row:3) [Low]
      6'h1E: code = {OP_DATH, 4'b0000}; // CGRAM Data Write [High]
      6'h1F: code = {OP_DATL, 4'b0000}; // CGRAM Data Write [Low]
      6'h20: code = {OP_CMDH, 4'b0100}; // CGRAM Address Set (char:0, row:4) [High]
      6'h21: code = {OP_CMDL, 4'b0100}; // CGRAM Address Set (char:0, row:4) [Low]
      6'h22: code = {OP_DATH, 4'b0000}; // CGRAM Data Write [High]
      6'h23: code = {OP_DATL, 4'b0000}; // CGRAM Data Write [Low]
      6'h24: code = {OP_CMDH, 4'b0100}; // CGRAM Address Set (char:0, row:5) [High]
      6'h25: code = {OP_CMDL, 4'b0101}; // CGRAM Address Set (char:0, row:5) [Low]
      6'h26: code = {OP_DATH, 4'b0000}; // CGRAM Data Write [High]
      6'h27: code = {OP_DATL, 4'b0000}; // CGRAM Data Write [Low]
      6'h28: code = {OP_CMDH, 4'b0100}; // CGRAM Address Set (char:0, row:6) [High]
      6'h29: code = {OP_CMDL, 4'b0110}; // CGRAM Address Set (char:0, row:6) [Low]
      6'h2A: code = {OP_DATH, 4'b0000}; // CGRAM Data Write [High]
      6'h2B: code = {OP_DATL, 4'b0000}; // CGRAM Data Write [Low]
      6'h2C: code = {OP_CMDH, 4'b0100}; // CGRAM Address Set (char:0, row:7) [High]
      6'h2D: code = {OP_CMDL, 4'b0111}; // CGRAM Address Set (char:0, row:7) [Low]
      6'h2E: code = {OP_DATH, 4'b0000}; // CGRAM Data Write [High]
      6'h2F: code = {OP_DATL, 4'b0000}; // CGRAM Data Write [Low]
      
      // display line 0
      6'h30: code = {OP_CMDH, 4'b1000}; // DDRAM Address Set (x:0, y:0) [High]
      6'h31: code = {OP_CMDL, 4'b0000}; // DDRAM Address Set (x:0, y:0) [Low]
      6'h32: code = {OP_RSTA, 4'b0000}; // Reset Text-VRAM address
      6'h33: code = {OP_READ, 4'b0000}; // Read a character from Text-VRAM
      6'h34: code = {OP_DATH, 4'b0000}; // DDRAM Data Write [High]
      6'h35: code = {OP_DATL, 4'b0000}; // DDRAM Data Write [Low]
      6'h36: code = {OP_JNZ, 6'h33}; // loop until line end

      // display line 1
      6'h37: code = {OP_CMDH, 4'b1100}; // DDRAM Address Set (x:0, y:1) [High]
      6'h38: code = {OP_CMDL, 4'b0000}; // DDRAM Address Set (x:0, y:1) [Low]
      6'h39: code = {OP_READ, 4'b0000}; // Read a character from Text-VRAM
      6'h3A: code = {OP_DATH, 4'b0000}; // DDRAM Data Write [High]
      6'h3B: code = {OP_DATL, 4'b0000}; // DDRAM Data Write [Low]
      6'h3C: code = {OP_JNZ, 6'h39}; // loop until line end
      6'h3D: code = {OP_JMP, 6'h30}; // jump 'display line 0'

      default: code = {OP_NOP};
    endcase
  endfunction

  parameter
    STATE_DECODE    = 3'h0,
    STATE_EXEC      = 3'h1,
    STATE_ENABLE_H  = 3'h2,
    STATE_ENABLE_L  = 3'h3,
    STATE_DISABLE_H = 3'h4,
    STATE_DISABLE_L = 3'h5,
    STATE_WAIT      = 3'h6;
  
  reg [2:0] state = STATE_DECODE;
  
  wire [7:0] ir = code(pc);
  reg exec_wait;
  reg exec_read;
  reg exec_rsta;
  reg exec_cmdh;
  reg exec_cmdl;
  reg exec_dath;
  reg exec_datl;
  reg exec_jmp;
  reg exec_jnz;
  reg [3:0] arg;
  reg [5:0] next_pc;
  
  always @(posedge clk) begin
    if (wait_counter != 32'h0) begin
      wait_counter <= wait_counter + 1;
    end
    else begin
      case (state)
        STATE_DECODE: begin
          state <= STATE_EXEC;
          exec_wait <= (OP_WAIT == ir[7:4]);
          exec_read <= (OP_READ == ir[7:4]);
          exec_rsta <= (OP_RSTA == ir[7:4]);
          exec_cmdh <= (OP_CMDH == ir[7:4]);
          exec_cmdl <= (OP_CMDL == ir[7:4]);
          exec_dath <= (OP_DATH == ir[7:4]);
          exec_datl <= (OP_DATL == ir[7:4]);
          exec_jmp  <= (OP_JMP == ir[7:6]);
          exec_jnz  <= (OP_JNZ == ir[7:6]);
          arg       <= ir[3:0];
          next_pc   <= ir[5:0];
        end
        STATE_EXEC: begin
          pc <= pc + 1;
          if (exec_wait) begin
            state <= STATE_DECODE;
            case (arg)
              WAIT_15000MICRO: wait_counter <= -750000;
              WAIT_4100MICRO:  wait_counter <= -205000;
              WAIT_1600MICRO:  wait_counter <= -80000;
              WAIT_100MICRO:   wait_counter <= -100;
              WAIT_40MICRO:    wait_counter <= -40;
              WAIT_1MICRO:     wait_counter <= -1;
              default:         wait_counter <= 32'hx;
            endcase
          end
          if (exec_read) begin
            state <= STATE_DECODE;
            {ah, al} <= data;
            addr <= addr + 1;
          end
          if (exec_rsta) begin
            state <= STATE_DECODE;
            addr <= 5'h0;
            {ah, al} <= 8'h0;
          end
          if (exec_cmdh) begin
            state <= STATE_ENABLE_H;
            wait_counter <= -2;
            LCD_RS <= 0;
            db <= arg;
          end
          if (exec_cmdl) begin
            state <= STATE_ENABLE_L;
            wait_counter <= -2;
            LCD_RS <= 0;
            db <= arg;
          end
          if (exec_dath) begin
            state <= STATE_ENABLE_H;
            wait_counter <= -2;
            LCD_RS <= 1;
            db <= ah;
          end
          if (exec_datl) begin
            state <= STATE_ENABLE_L;
            wait_counter <= -2;
            LCD_RS <= 1;
            db <= al;
          end
          if (exec_jmp) begin
            state <= STATE_DECODE;
            pc <= next_pc;
          end
          if (exec_jnz) begin
            state <= STATE_DECODE;
            if (addr[3:0] != 4'h0) pc <= next_pc;
          end
        end
        STATE_ENABLE_H: begin
          state <= STATE_DISABLE_H;
          wait_counter <= -12;
          LCD_E <= 1;
        end
        STATE_ENABLE_L: begin
          state <= STATE_DISABLE_L;
          wait_counter <= -12;
          LCD_E <= 1;
        end
        STATE_DISABLE_H: begin
          state <= STATE_DECODE;
          wait_counter <= -50;
          LCD_E <= 0;
        end
        STATE_DISABLE_L: begin
          state <= STATE_DECODE;
          wait_counter <= -2000;
          LCD_E <= 0;
        end
      endcase
    end
  end
  
  assign LCD_RW = 0;
  assign LCD_DB = db;
endmodule
