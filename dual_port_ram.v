`timescale 1ns/1ps

module dual_port_ram(/*AUTOARG*/
   // Outputs
   data_out1, data_out2,
   // Inputs
   clk1, clk2, write_enable1, addr1, addr2, data_in1
   );
   parameter ADDR_BITS = 4;
   parameter DATA_BITS = 8;
   parameter [256*8:1] DATAFILE = 2048'b0;
   
   input clk1, clk2;
   input write_enable1;
   input [ADDR_BITS-1:0] addr1, addr2;
   input [DATA_BITS-1:0] data_in1;
   output reg [DATA_BITS-1:0] data_out1, data_out2;

   reg [DATA_BITS-1:0] mem [0:(2**ADDR_BITS)-1];

   generate
      if (DATAFILE[8:1] == 8'h0) begin
         integer i;
         initial begin
            for (i = 0; i < 2**ADDR_BITS; i = i + 1) mem[i] = 0;
         end
      end
      else begin
         initial begin
            $readmemh(DATAFILE, mem, 0, (2**ADDR_BITS)-1);
         end
      end
   endgenerate

   always @(posedge clk1) begin
      if (write_enable1) mem[addr1] <= data_in1;
      data_out1 <= mem[addr1];
   end

   always @(posedge clk2) begin
      data_out2 <= mem[addr2];
   end
endmodule
 
