
(define (spica:read-string in)
  (let loop ((c (read-char in))
             (char-list '()))
    (cond
     ((eof-object? c)
      (error "EOF encountered in a string literal."))
     ((char=? c #\")
      (apply string (reverse char-list)))
     ((char=? c #\\)
      (let ((c (spica:read-string-escape in)))
        (loop (read-char in) (cons c char-list))))
     (else
      (loop (read-char in) (cons c char-list))))))

(define (spica:read-string-escape in)
  (let ((c (read-char in)))
    (cond
     ((eof-object? c)
      (error "EOF encountered in a string literal."))
     ((char=? c #\n)
      #\newline)
     ((char=? c #\r)
      #\return)
     ((char=? c #\f)
      #\x0c)
     ((char=? c #\t)
      #\tab)
     ((char=? c #\0)
      #\null)
     ((char=? c #\x)
      (integer->char (spica:read-hex2 in)))
     (else
      c))))

(define (spica:hex1->integer c)
  (- (char->integer c)
     (cond
      ((and (char<=? #\0 c) (char<=? c #\9))
       48)
      ((and (char<=? #\A c) (char<=? c #\F))
       55)
      ((and (char<=? #\a c) (char<=? c #\f))
       87)
      (else
       (error "Bad hexadecimal char")))))

(define (spica:read-hex2 in)
  (let* ((ch (read-char in))
         (cl (read-char in)))
    (+ (* 16 (spica:hex1->integer ch))
       (spica:hex1->integer cl))))

(define (spica:read-symbol in char-list)
  (let ((c (peek-char in)))
    (cond
     ((or (eof-object? c)
          (char<=? c #\space)
          (char=? c #\") (char=? c #\') (char=? c #\() (char=? c #\)) (char=? c #\,) (char=? c #\.) (char=? c #\;)
          (char=? c #\[) (char=? c #\]) (char=? c #\`) (char=? c #\{) (char=? c #\}) (char=? c #\x7f))
      (string->symbol (apply string (reverse char-list))))
     (else
      (read-char in)
      (spica:read-symbol in (cons c char-list))))))

(define (spica:read-symbol-token in char-list)
  (let ((c (peek-char in)))
    (cond
     ((or (eof-object? c) (char=? c #\|))
      (string->symbol (apply string (reverse char-list))))
     (else
      (read-char in)
      (spica:read-symbol-token in (cons c char-list))))))

(define (spica:read-decimal-or-symbol in char-list n)
  (let ((c (peek-char in)))
    (cond
     ((or (eof-object? c)
          (char<=? c #\space)
          (char=? c #\") (char=? c #\') (char=? c #\() (char=? c #\)) (char=? c #\,) (char=? c #\.) (char=? c #\;)
          (char=? c #\[) (char=? c #\]) (char=? c #\`) (char=? c #\{) (char=? c #\}) (char=? c #\x7f))
      n)
     ((and (char<=? #\0 c) (char<=? c #\9))
      (read-char in)
      (spica:read-decimal-or-symbol in (cons c char-list) (+ (* n 10) (spica:hex1->integer c))))
     (else
      (read-char in)
      (spica:read-symbol in (cons c char-list))))))

(define (spica:read-number in radix pred?)
  (let loop ((c (peek-char in))
             (n 0))
    (cond
     ((or (eof-object? c)
          (char<=? c #\space)
          (char=? c #\") (char=? c #\') (char=? c #\() (char=? c #\)) (char=? c #\,) (char=? c #\.) (char=? c #\;)
          (char=? c #\[) (char=? c #\]) (char=? c #\`) (char=? c #\{) (char=? c #\}) (char=? c #\x7f))
      n)
     ((pred? c)
      (read-char in)
      (loop (peek-char in) (+ (* n radix) (spica:hex1->integer c))))
     (else
      (error "bad numeric format")))))

(define (spica:read-literal-char in)
  (let ((c (read-char in)))
    (cond
     ((eof-object? c)
      (error "EOF encountered in character literal"))
     ((or (char<=? c #\space)
          (char=? c #\") (char=? c #\') (char=? c #\() (char=? c #\)) (char=? c #\,) (char=? c #\.) (char=? c #\;)
          (char=? c #\[) (char=? c #\]) (char=? c #\`) (char=? c #\{) (char=? c #\}) (char=? c #\x7f))
      c)
     ((char=? c #\x)
      (integer->char (spica:read-hex2 in)))
     (else
      (let ((sym (spica:read-symbol in (list c))))
        (cond
         ((eq? sym 'space)
          (integer->char #x20))
         ((memq sym '(newline nl lf))
          (integer->char #x0a))
         ((memq sym '(return cr))
          (integer->char #x0d))
         ((eq? sym 'page)
          (integer->char #x0c))
         ((memq sym '(escape esc))
          (integer->char #x1b))
         ((memq sym '(delete del))
          (integer->char #x7f))
         ((eq? sym 'null)
          (integer->char #x00))
         (else
          (let ((str (symbol->string sym)))
            (if (= (string-length str) 1)
                (string-ref str 0)
                (error "Unknown character name"))))))))))
      
(define (spica:skip-space in)
  (let ((c (peek-char in)))
    (unless (or (eof-object? c) (and (char<=? #\! c) (char<=? c #\~)))
      (read-char in)
      (spica:skip-space in))))

(define (spica:skip-line in)
  (let ((c (read-char in)))
    (unless (or (eof-object? c) (char<=? c #\newline))
      (spica:skip-line in))))

(define (spica:read-cons-cdr in)
  (let ((v (spica:read in)))
    (spica:skip-space in)
    (let ((c (peek-char in)))
      (cond
       ((eof-object? c)
        (error "EOF inside a list"))
       ((or (char=? c #\)) (char=? c #\]) (char=? c #\}))
        (read-char in)
        v)
       (else
        (error "bad dot syntax"))))))

(define (spica:read-list in)
  (spica:skip-space in)
  (let ((c (peek-char in)))
    (cond
     ((eof-object? c)
      (error "EOF inside a list"))
     ((or (char=? c #\)) (char=? c #\]) (char=? c #\}))
      (read-char in)
      '())
     (else
      (let* ((kar (spica:read in))
             (kdr (begin
                    (spica:skip-space in)
                    (let ((c (peek-char in)))
                      (if (char=? c #\.)
                          (begin
                            (read-char in)
                            (spica:read-cons-cdr in))
                          (spica:read-list in))))))
        (cons kar kdr))))))

(define (spica:read-vector in)
  (let loop ((elts '()))
    (spica:skip-space in)
    (let ((c (peek-char in)))
      (cond
       ((eof-object? c)
        (error "EOF inside a vector"))
       ((or (char=? c #\)) (char=? c #\]) (char=? c #\}))
        (read-char in)
        (apply vector (reverse elts)))
       (else
        (loop (cons (spica:read in) elts)))))))

(define (spica:read-sharp in)
  (let ((c (read-char in)))
    (cond
     ((char=? c #\!)
      ;; hash-bang token is ignored.
      (spica:read-symbol in '())
      (spica:read in))
     ((char=? c #\()
      (spica:read-vector in))
     ((char=? c #\;)
      (spica:read in)
      (spica:read in))
     ((char=? c #\b)
      (spica:read-number in 2 (lambda (c)
                                (and (char<=? #\0 c) (char<=? c #\1)))))
     ((char=? c #\d)
      (spica:read-number in 10 (lambda (c)
                                 (and (char<=? #\0 c) (char<=? c #\9)))))
     ((char=? c #\f)
      #f)
     ((char=? c #\o)
      (spica:read-number in 8 (lambda (c)
                                (and (char<=? #\0 c) (char<=? c #\7)))))
     ((char=? c #\t)
      #t)
     ((char=? c #\x)
      (spica:read-number in 16 (lambda (c)
                                 (or (and (char<=? #\0 c) (char<=? c #\9))
                                     (and (char<=? #\A c) (char<=? c #\F))
                                     (and (char<=? #\a c) (char<=? c #\f))))))
     ((char=? c #\\)
      (spica:read-literal-char in))
     (else
      (error "Unsupported #-syntax")))))
     
     
(define (spica:read in)
  (spica:skip-space in)
  (let ((c (peek-char in)))
    (cond
     ((eof-object? c)
      c)
     ((char=? c #\")
      (read-char in)
      (spica:read-string in))
     ((char=? c #\#)
      (read-char in)
      (spica:read-sharp in))
     ((char=? c #\')
      (read-char in)
      (list 'quote (spica:read in)))
     ((or (char=? c #\() (char=? c #\[) (char=? c #\{))
      (read-char in)
      (spica:read-list in))
     ((or (char=? c #\)) (char=? c #\]) (char=? c #\}))
      (error "extra close parenthesis"))
     ((char=? c #\,)
      (read-char in)
      (let ((c (peek-char in)))
        (cond
         ((char=? c #\@)
          (read-char in)
          (list 'unquote-splicing (spica:read in)))
         (else
          (list 'unquote (spica:read in))))))
     ((char=? c #\.)
      (error "bad dot syntax"))
     ((and (char<=? #\0 c) (char<=? c #\9))
      (spica:read-decimal-or-symbol in '() 0))
     ((char=? c #\;)
      (read-char in)
      (spica:skip-line in))
     ((char=? c #\`)
      (read-char in)
      (list 'quasiquote (spica:read in)))
     ((char=? c #\|)
      (read-char in)
      (spica:read-symbol-token in '()))
     (else
      (spica:read-symbol in '())))))

(define read spica:read)

