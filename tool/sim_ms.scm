;; -*- coding: utf-8; mode: scheme -*-
;;
;; sim_ms.scm - MicroSpica simulator
;;
;;   Copyright (c) 2010 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(use gauche.parameter)
(use gauche.parseopt)
(use gauche.uvector)
(use srfi-1)
(use srfi-14)
(use util.list)
(use util.match)

(use msasm.context)

(define *main-ram* (make-u32vector (expt 2 12) 0))
(define *cg-ram* (make-u8vector (expt 2 12) 0))
(define *tv-ram* (make-u16vector (expt 2 12) 0))
(define *lcd-ram* (make-u8vector (expt 2 5) 0))
(define *io* (make-u8vector (expt 2 8) 0))

(define *sdram* (make-u32vector (expt 2 (- 24 2)) 0))

(define *dispatch-table* (make-u16vector (expt 2 10) 0))

(define *register-file* (make-u32vector 32 0))
(define *delayed* #f)

(define *mstatus* 0)
(define *maddr* 0)

(define *show-report?* #t)

(define *context* #f)
(define *hook-table* (make-hash-table))

;; functions for debugging (check-point, interactive, etc)
(define (label name)
  (if (number? name)
      name
      (context-resolve-label (context-child *context* (pc)) name)))

(define (reg name)
  (or (and-let* ((ctxt (context-child *context* (pc))))
        (~ *register-file* (reg->code ctxt name)))
      (errorf "Out of context @ pc=~4,'0x" (pc))))

(define (set-reg! name v)
  (or (and-let* ((ctxt (context-child *context* (pc))))
        (set! (~ *register-file* (reg->code ctxt name)) v))
      (errorf "Out of context @ pc=~4,'0x" (pc))))      

(set! (setter reg) set-reg!)

;;

(define pc (make-parameter 0 label))

;; memory accessor
(define (sdram-read-word addr)
  (u32vector-ref *sdram* (logand #x3fffff addr)))

(define (sdram-write-word addr data)
  (u32vector-set! *sdram* (logand #x3fffff addr) data))

(define (ram-read-word addr)
  (cond
    ((<= #x0 addr #xfff)
     (u32vector-ref *main-ram* (logand addr #xfff)))
    ((<= #xd000 addr #xdfff)
     (u8vector-ref *cg-ram* (logand addr #xfff)))
    ((<= #xe000 addr #xefff)
     (u16vector-ref *tv-ram* (logand addr #xfff)))
    ((<= #xfe00 addr #xfeff)
     (u8vector-ref *lcd-ram* (logand addr #xff)))
    ((<= #xff00 addr #xffff)
     (u8vector-ref *io* (logand addr #xff)))
    (else
     (errorf "invalid addr ~8,'0x" addr))))

(define (ram-write-word addr data)
  (cond
    ((<= #x0 addr #xfff)
     (u32vector-set! *main-ram* (logand addr #xfff) data))
    ((<= #xd000 addr #xdfff)
     (u8vector-set! *cg-ram* (logand addr #xfff) (logand data #xff)))
    ((<= #xe000 addr #xefff)
     (u16vector-set! *tv-ram* (logand addr #xfff) (logand data #xffff)))
    ((<= #xfe00 addr #xfeff)
     (u8vector-set! *lcd-ram* (logand addr #xff) (logand data #xff)))
    ((<= #xff00 addr #xffff)
     (u8vector-set! *io* (logand addr #xff) (logand data #xff)))
    (else
     (errorf "invalid addr ~8,'0x" addr))))

(define (ram addr)
  (ram-read-word (label addr)))

(define-method (setter ram) (addr val)
  (ram-write-word (label addr) (label val)))

(define (sdram addr)
  (sdram-read-word (label addr)))

(define-method (setter sdram) (addr val)
  (sdram-write-word (label addr) (label val)))

;; 
(define (reportf fmt . args)
  (when *show-report?*
    (apply format #t fmt args)
    (flush-all-ports)))

(define (funct? inst)
  (= (ash inst -27) 0))

(define (funct inst)
  (logand inst #x3f))

(define (op inst)
  (logand (ash inst -27) #b11111))

(define (asm-rd inst)
  (logand (ash inst -22) #b11111))

(define (asm-rs inst)
  (logand (ash inst -17) #b11111))

(define (asm-rt inst)
  (logand (ash inst -11) #b11111))

(define (asm-rs-val inst)
  (u32vector-ref *register-file* (asm-rs inst)))

(define (imm? inst)
  (= (logand (ash inst -16) 1) 1))

(define (imm inst)
  (logand inst #xffff))

(define (asm-rt-val inst)
  (if (imm? inst)
      (imm inst)
      (u32vector-ref *register-file*  (asm-rt inst))))

(define (asm-rt-sval inst)
  (if (imm? inst)
      (let ((v (imm inst)))
        (if (< #x8000 v)
            (logior #xffff0000 v)
            v))
      (u32vector-ref *register-file*  (asm-rt inst))))

(define (asm-rd-val inst)
  (u32vector-ref *register-file*  (asm-rd inst)))

(define (set-rd! inst val)
  (let ((rd (asm-rd inst)))
    (unless (= rd 0)
      (u32vector-set! *register-file* rd (logand val #xffffffff))
      (reportf "    $r~a <= #x~8,'0x~%" rd (logand val #xffffffff)))))

(define (set-pc! inst)
  (reportf "    pc <= #x~8,'0x~%" (asm-rt-val inst))
  (set! (pc) (asm-rt-val inst)))

(define (show-mnemonic mnemonic inst)
  (reportf (if (imm? inst)
               "~8,'0x: (~a $r~a $r~a #x~4,'0x)~%"
               "~8,'0x: (~a $r~a $r~a $r~a)~%")
           (pc)
           mnemonic
           (asm-rd inst)
           (asm-rs inst)
           ((if (imm? inst) imm asm-rt) inst)))

(define (u32->int v)
  (if (= (logand v #x80000000) 0)
      v
      (- (+ (logand (lognot v) #xffffffff) 1))))

(define (branch inst v link?)
  (reportf "    condition is ~a~%" v)
  (when v
    (when link?
      (set-rd! inst (+ (pc) 1)))
    (set! *delayed* (cut set-pc! inst))))

(define *tick* 0)
(define *tick-stack* '())

(define (tick)
  *tick*)

(define (push-tick!)
  (push! *tick-stack* *tick*))

(define (pop-tick!)
  (pop! *tick-stack*))

(define (exec-code)
  (when (<= 0 (pc) #x0fff)
    (inc! *tick*)
    (for-each (cut apply <> '()) (reverse (hash-table-get *hook-table* (pc) '())))
    (let ((inst (u32vector-ref *main-ram* (pc)))
          (delayed *delayed*)
          (halt #f))
      (set! *delayed* #f)
      (cond
        ((and (funct? inst) (= (funct inst) #o51))
         (show-mnemonic 'mstatus inst)
         (set-rd! inst *mstatus*))

        ((and (funct? inst) (= (funct inst) #o06))
         (show-mnemonic 'lookup inst)
         (let* ((vm-op (logand (asm-rt-val inst) #x3ff))
                (new-pc (u16vector-ref *dispatch-table* vm-op)))
           (reportf "    lookup jump addr for code: ~3,'0x~%" vm-op)
           (set-rd! inst new-pc)))
        ((= (op inst) #o01)
         (show-mnemonic 'add inst)
         (set-rd! inst (+ (asm-rs-val inst) (asm-rt-val inst))))
        ((= (op inst) #o02)
         (show-mnemonic 'adh inst)
         (set-rd! inst (let ((a (asm-rs-val inst))
                             (b (asm-rt-val inst)))
                         (if (= (ash a -24) (logand (ash b -8) #xff))
                             a
                             (logior (ash (logand (+ b (ash a -24)) #xff) 24)
                                     (logand a #x00ffffff))))))
        ((= (op inst) #o03)
         (show-mnemonic 'sub inst)
         (set-rd! inst (- (asm-rs-val inst) (asm-rt-val inst))))
        ((= (op inst) #o04)
         (show-mnemonic 'and inst)
         (set-rd! inst (logand (asm-rs-val inst) (asm-rt-val inst))))
        ((= (op inst) #o05)
         (show-mnemonic 'or inst)
         (set-rd! inst (logior (asm-rs-val inst) (asm-rt-val inst))))
        ((= (op inst) #o06)
         (show-mnemonic 'xor inst)
         (set-rd! inst (logxor (asm-rs-val inst) (asm-rt-val inst))))
        ((= (op inst) #o07)
         (show-mnemonic 'nor inst)
         (set-rd! inst (lognot (logior (asm-rs-val inst) (asm-rt-val inst)))))
        ((= (op inst) #o14)
         (show-mnemonic 'sll inst)
         (set-rd! inst (ash (asm-rs-val inst) (asm-rt-val inst))))
        ((= (op inst) #o15)
         (show-mnemonic 'srl inst)
         (set-rd! inst (ash (asm-rs-val inst) (- (asm-rt-val inst)))))
        ((= (op inst) #o16)
         (show-mnemonic 'sla inst)
         (let ((mask (logand (asm-rs-val inst) #x80000000)))
           (set-rd! inst (logior (ash (asm-rs-val inst) (asm-rt-val inst)) mask))))
        ((= (op inst) #o17)
         (show-mnemonic 'sra inst)
         (set-rd! inst (logand (ash (logior (asm-rs-val inst) (if (logbit? 31 (asm-rs-val inst))
                                                                  #xffffffff00000000
                                                                  #x0))
                                    (- (asm-rt-val inst)))
                         #xffffffff)))
        ((= (op inst) #o23)
         (show-mnemonic 'bgezl inst)
         (branch inst (<= 0 (u32->int (asm-rs-val inst))) #t)
         (set! halt (and (= (asm-rs inst) 0)
                         (= (asm-rt-val inst) (pc)))))
        ((= (op inst) #o24)
         (show-mnemonic 'bgtzl inst)
         (branch inst (< 0 (u32->int (asm-rs-val inst))) #t))
        ((= (op inst) #o25)
         (show-mnemonic 'blezl inst)
         (branch inst (<= (u32->int (asm-rs-val inst)) 0) #t)
         (set! halt (and (= (asm-rs inst) 0)
                         (= (asm-rt-val inst) (pc)))))
        ((= (op inst) #o26)
         (show-mnemonic 'bltzl inst)
         (branch inst (< (u32->int (asm-rs-val inst)) 0) #t))
        ((= (op inst) #o34)
         (show-mnemonic 'btt inst)
         (branch inst
                 (let ((type (ash (asm-rs-val inst) -24))
                       (mask (logand (ash (asm-rd-val inst) -8) #xff))
                       (expect (logand (asm-rd-val inst) #xff)))
                   (= (logand type mask) expect))
                 #f))
        ((= (op inst) #o35)
         (show-mnemonic 'bnt inst)
         (branch inst
                 (let ((type (ash (asm-rs-val inst) -24))
                       (mask (logand (ash (asm-rd-val inst) -8) #xff))
                       (expect (logand (asm-rd-val inst) #xff)))
                   (not (= (logand type mask) expect)))
                 #f))
        ((= (op inst) #o36)
         (show-mnemonic 'beq inst)
         (branch inst (= (asm-rd-val inst) (asm-rs-val inst)) #f)
         (set! halt (and (= (asm-rs inst) 0)
                         (= (asm-rd inst) 0)
                         (= (asm-rt-val inst) (pc)))))
        ((= (op inst) #o27)
         (show-mnemonic 'inst inst)
         (u16vector-set! *dispatch-table* (asm-rs-val inst) (asm-rt-val inst))
         (set-rd! inst (+ (asm-rs-val inst) 1)))
        ((= (op inst) #o37)
         (show-mnemonic 'bne inst)
         (branch inst (not (= (asm-rd-val inst) (asm-rs-val inst))) #f))
        ((= (op inst) #o21)
         (show-mnemonic 'lw inst)
         (let* ((addr (logand (+ (asm-rs-val inst) (asm-rt-sval inst)) #xffff))
                (v (ram-read-word addr)))
           (set! *delayed* (lambda ()
                             (set-rd! inst v)
                             (inc! (pc))))))
        ((= (op inst) #o20)
         (show-mnemonic 'mlwr inst)
         (if (= *mstatus* 0)
             (set! *mstatus* #b1000000)
             (errorf "mstatus isn't idle.(#b~7,'0b)" *mstatus*))
         (set! *maddr* (logand (+ (asm-rs-val inst) (asm-rt-sval inst)) #xffffffff)))
        ((= (op inst) #o22)
         (show-mnemonic 'lmdr inst)
         (let ((v (sdram-read-word *maddr*)))
           (reportf "    mdr <= #x~8,'0x @ #x~8,'0x~%" v *maddr*)
           (set-rd! inst v))
         (branch inst (not (= *mstatus* 0)) #f))
        ((= (op inst) #o31)
         (show-mnemonic 'sw inst)
         (let ((addr (logand (+ (asm-rs-val inst) (asm-rt-sval inst)) #xffff)))
           (ram-write-word addr (asm-rd-val inst))
           (when (= addr #xff80)
             (show-led))))
        ((= (op inst) #o30)
         (show-mnemonic 'mswr inst)
         (if (= *mstatus* 0)
             (set! *mstatus* #b1000000)
             (errorf "mstatus isn't idle.(#b~7,'0b)" *mstatus*))
         (let ((addr (+ (asm-rs-val inst) (asm-rt-sval inst))))
           (reportf "    sdram[#x~8,'0x] <= #x~8,'0x~%" (logand addr #xffffffff) (asm-rd-val inst))
           (sdram-write-word (+ (asm-rs-val inst) (asm-rt-val inst)) (asm-rd-val inst))))
        (else
         (errorf "invalid code ~8,'0x @ ~8,'0X" inst (pc))))
      (if delayed
          (delayed)
          (inc! (pc)))
    
      (set! *mstatus* (case *mstatus*
                        ((0) 0)
                        ((#b1000000) #b0100001)
                        ((#b0100001) #b0000010)
                        ((#b0000010) 0)
                        ((#b0010000) #b0001011)
                        ((#b0001011) #b0000100)
                        ((#b0000100) 0)))
      (unless halt
        (exec-code)))))


(define (load-bin filename)
  (call-with-input-file filename
    (lambda (in)
      (read-block! *main-ram* in 0 -1 'little-endian))))

(define (load-code filename)
  (call-with-input-file filename
    (lambda (in)
      (let ((addr 0))
        (port-for-each (lambda (line)
                         (rxmatch-case line
                           (#/@(.*)/ (_ addr-str)
                            (set! addr (string->number addr-str 16)))
                           (#/(.*)/ (_ data)
                            (u32vector-set! *main-ram* addr (string->number data 16))
                            (inc! addr))))
                       (cut read-line in))))))

(define (show-led)
  (let ((v (u8vector-ref *io* #x80)))
    (format #t "LED: ~a~%" (apply string-append (map (lambda (bit)
                                                       (if (= (logand v (expt 2 bit)) 0)
                                                           "_"
                                                           "*"))
                                                     (iota 8 7 -1))))))

(define (show-lcd)
  (format #t "LCD0: \"~a\"~%" (apply string-append (map (lambda (c)
                                                      (if (char-set-contains? char-set:printing c)
                                                          (x->string c)
                                                          " "))
                                                    (map (compose ucs->char (cut u8vector-ref *lcd-ram* <>))
                                                         (iota 16)))))
  (format #t "LCD1: \"~a\"~%" (apply string-append (map (lambda (c)
                                                      (if (char-set-contains? char-set:printing c)
                                                          (x->string c)
                                                          " "))
                                                    (map (compose ucs->char (cut u8vector-ref *lcd-ram* <>))
                                                         (iota 16 16))))))
  
(define (show-help progname)
  (format #t "Usage: ~a filename ...~%" progname)
  (exit 1))

(define (show-reg)
  (for-each (lambda (i name)
              (format #t "~a: #x~8,'0x    " name (u32vector-ref *register-file* i))
              (when (odd? i)
                (newline)))
            (iota 32)
            '("$r0  ($zero)"
              "$r1  ($at)  "
              "$r2  ($v0)  "
              "$r3  ($v1)  "
              "$r4  ($a0)  "
              "$r5  ($a1)  "
              "$r6  ($a2)  "
              "$r7  ($a3)  "
              "$r8  ($t0)  "
              "$r9  ($t1)  "
              "$r10 ($t2)  "
              "$r11 ($t3)  "
              "$r12 ($t4)  "
              "$r13 ($t5)  "
              "$r14 ($t6)  "
              "$r15 ($t7)  "
              "$r16 ($s0)  "
              "$r17 ($s1)  "
              "$r18 ($s2)  "
              "$r19 ($s3)  "
              "$r20 ($s4)  "
              "$r21 ($s5)  "
              "$r22 ($s6)  "
              "$r23 ($s7)  "
              "$r24 ($t8)  "
              "$r25 ($t9)  "
              "$r26 ($k0)  "
              "$r27 ($k1)  "
              "$r28 ($gp)  "
              "$r29 ($sp)  "
              "$r30 ($fp)  "
              "$r31 ($ra)  ")))

(define (show-indicator)
  (print "----------------------------------------")
  (show-led)
  (show-lcd)
  (print "----------------------------------------"))

(define (j addr)
  (set! (pc) (label addr))
  (set! *delayed* #f)
  (exec-code))

(define (call addr . args)
  (when (< 4 (length args))
    (errorf "Too many args, the maximum number of args is 4, but got %d args"
      (length args)))
  (for-each (lambda (v r)
              (set! (reg r) v))
            args
            '($r4 $r5 $r6 $r7))
  (set! (reg '$r31) #xffffffff)
  (parameterize ((pc (pc)))
    (j addr)
    (values (reg '$r2) (reg '$r3))))

(define sandbox
  (let ((module #f))
    (lambda ()
      (or module
          (begin
            (set! module (make-module #f))
            (eval '(extend user) module)
            module)))))
          
(define (read-debug-info in)
  (set! *context* (alist->context (read in)))
  (for-each (match-lambda
              ((pc . exprs)
               (hash-table-push! *hook-table*
                                 pc
                                 (eval `(lambda () ,@exprs)
                                       (sandbox)))))
            (check-point-list *context*)))


;;
(define (test-eval expr)
  (eval expr (sandbox)))

(define (exec-test result-alist desc expected-form . body)
  (define (count-up key)
    (assoc-set! result-alist key (+ (assoc-ref result-alist key 0) 1)))
  (for-each test-eval body)
  (let ((fail? #f)
        (abort? #t))
    (guard (e (else
               (format (current-error-port) "test ~a: aborted by ~s." desc e)
               (report-error e)))
      (for-each (match-lambda
                  (('reg (? symbol? reg-name) expected)
                   (let ((actual (reg reg-name)))
                     (unless (= (label expected) actual)
                       (set! fail? #t)
                       (format (current-error-port)
                           "test ~a: expects reg ~a to be ~a (~:*#x~8,'0x), but got ~a (~:*#x~8,'0x).\n"
                         desc reg-name (label expected) actual))))
                  (('ram (or (? number? addr) (? keyword? addr))
                         expected-list ...)
                   (let ((i (any (lambda (i expected)
                                   (and (not (= (ram (+ (label addr) i))
                                                (label expected)))
                                        i))
                              (iota (length expected-list))
                              expected-list)))
                     (when i
                       (set! fail? #t)
                       (format (current-error-port)
                           "test ~a: expects RAM@#x~4,'0x to be ~d (~:*#x~8,'0x), but got ~d (~:*#x~8,'0x)\n"
                         desc
                         (+ (label addr) i)
                         (label (~ expected-list i))
                         (ram (+ (label addr) i))))))
                  (('sdram (or (? number? addr) (? keyword? addr))
                           expected-list ...)
                   (let ((i (any (lambda (i expected)
                                   (and (not (= (sdram (+ (label addr) i))
                                                (label expected)))
                                        i))
                              (iota (length expected-list))
                              expected-list)))
                     (when i
                       (set! fail? #t)
                       (format (current-error-port)
                           "test ~a: expects SDRAM@#x~8,'0x to be ~d (~:*#x~8,'0x), but got ~d (~:*#x~8,'0x)\n"
                         desc
                         (+ (label addr) i)
                         (label (~ expected-list i))
                         (sdram (+ (label addr) i)))))))
                expected-form)
      (set! abort? #f))
    (cond
      (abort?
       (format (current-error-port) "test ~a: aborted.\n" desc)
       (count-up 'abort))
      (fail?
       (format (current-error-port) "test ~a: failed.\n" desc)
       (count-up 'fail))
      (else
       (format (current-error-port) "test ~a: succeed.\n" desc)
       (count-up 'success)))))

(define (show-test-result result-alist)
  (let ((success (assoc-ref result-alist 'success 0))
        (fail (assoc-ref result-alist 'fail 0))
        (abort (assoc-ref result-alist 'abort 0)))
    (format (current-error-port) "Success: ~d, Fail: ~d, Abort: ~d\n"
      success fail abort)
    (when (= fail abort 0)
      (format (current-error-port) "All tests are passed.\n")))
  result-alist)
  
(define (exec-test-cases desc test-cases)
  (format (current-error-port) "*** ~a ***\n" desc)
  (let ((setup (assoc-ref test-cases 'setup '()))
        (teardown (assoc-ref test-cases 'teardown '()))
        (before (assoc-ref test-cases 'before '()))
        (after (assoc-ref test-cases 'after '())))
    (for-each test-eval setup)
    (begin0
      (let ((result-alist (fold (lambda (test-case result-alist)
                                  (if (string? (first test-case))
                                      (begin
                                        (for-each test-eval before)
                                        (begin0
                                          (apply exec-test result-alist test-case)
                                          (for-each test-eval after)))
                                      result-alist))
                                '()
                                test-cases)))
        (show-test-result result-alist)
        result-alist)
      (for-each test-eval teardown))))

(define (add-test-result result0 result1)
  (fold (lambda (field result)
          (acons field
                 (+ (assoc-ref result0 field 0)
                    (assoc-ref result1 field 0))
                 result))
        '()
        '(success fail abort)))

(define (exec-all-tests ctxt)
  (fold (lambda (unit-test result-alist)
          (match-let1 (desc test-cases ...) unit-test
            (add-test-result result-alist
                             (exec-test-cases desc test-cases))))
        '()
        (test-case-list ctxt)))

;;

(define (main args)
  (let-args (cdr args)
      ((verbose "v|verbose" #f)
       (help "h|help" => (cut show-help (car args)))
       (load "l|load=s" => (lambda (memfile)
                             (call-with-input-file memfile
                               (lambda (in)
                                 (read-block! *sdram* in)))))
       (debug "d|debug=s" => (lambda (debug-file)
                               (call-with-input-file debug-file
                                 read-debug-info)))
       (test? "t|test" #f)
       (interactive? "i|interactive" #f)
       . filenames)
    (set! *show-report?* verbose)
    (load-bin (first filenames))
    (cond
      (interactive?
       (read-eval-print-loop read
                             eval
                             print
                             (lambda ()
                               (format #t "uSpica:#x~4,'0x> " (pc))
                               (flush (current-output-port)))))
      (test?
       (let ((result-alist (exec-all-tests *context*)))
         (format (current-error-port) "*** Overall ***\n")
         (show-test-result result-alist)))
      (else
       (exec-code)
       (show-indicator))))
  0)
                        
;; end of file
