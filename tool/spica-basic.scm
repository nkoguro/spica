;; -*- coding: utf-8; mode: scheme -*-
;;
;; spica-basic.scm - Spica basic library
;;
;;   Copyright (c) 2010 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(define (caar pair)
  (car (car pair)))

(define (cadr pair)
  (car (cdr pair)))

(define (cdar pair)
  (cdr (car pair)))

(define (cddr pair)
  (cdr (cdr pair)))

(define (destructuring-expander pat v body next)
  (if (null? pat)
      `(if (null? ,v)
           ,body
           ,next)
      (if (pair? pat)
          (let ((t (gensym)))
            `(let ((,t ,v))
               (if (pair? ,t)
                   ,(destructuring-expander (car pat) `(car ,t)
                                            (destructuring-expander (cdr pat) `(cdr ,t) body next)
                                            next)
                   ,next)))
          `(let ((,pat ,v))
             ,body))))

(define-macro (record-case expr . clauses)
  (let ((t (gensym)))
    `(let ((,t ,expr))
       ,(let loop ((clauses (reverse clauses))
                   (body '(error "invalid expression")))
          (if (null? clauses)
              body
              (loop (cdr clauses)
                    (let ((clause (car clauses))
                          (next (gensym)))
                      (let ((key (car clause))
                            (pat (cadr clause))
                            (clause-bodies (cddr clause)))
                        `(let ((,next (lambda () ,body)))
                           (if (eq? (car ,t) ',key)
                               ,(destructuring-expander pat `(cdr ,t) `(begin ,@clause-bodies) next)
                               ,next))))))))))

(define-macro (destructuring-bind pat expr . bodies)
  (let ((t (gensym)))
    `(let ((,t ,expr))
       ,(destructuring-expander pat t `(begin ,@bodies) '(error "unmatched lambda list")))))

(define (split-forms forms)
  (if (null? forms)
      (values '() '())
      (call-with-values (lambda ()
                          (split-forms (cdr forms)))
        (lambda (vars inits)
          (values (cons (caar forms) vars) (cons (cadar forms) inits))))))

(define-macro (let . args)
  (cond
   ((symbol? (car args))
    (destructuring-bind (name forms . bodies) args
      (call-with-values (lambda ()
                          (split-forms forms))
        (lambda (vars inits)
          `(let ((,name (lambda ,vars ,@bodies)))
             (,name ,@inits))))))
   (else
    (destructuring-bind (forms . bodies) args
      (call-with-values (lambda ()
                          (split-forms forms))
        (lambda (vars inits)
          `((lambda ,vars ,@bodies) ,@inits)))))))

(define-macro (let* forms . bodies)
  (if (null? forms)
      `(begin ,@bodies)
      `(let ((,(caar forms) ,(cadar forms)))
         (let* ,(cdr forms) ,@bodies))))

(define-macro (letrec forms . bodies)
  (call-with-values (lambda ()
                      (split-forms forms))
    (lambda (vars inits)
      (let loop ((xs vars)
                 (ys inits)
                 (dummies '())
                 (setters '()))
        (if (null? xs)
            `((lambda ,vars ,@setters ,@bodies) ,@dummies)
            (loop (cdr xs) (cdr ys) (cons #f dummies) (cons `(set! ,(car xs) ,(car ys)) setters)))))))


(define (pair? obj)
  (= (object-type obj) @pair))

(define (null? obj)
  (eq? obj '()))

(define (cadr x)
  (car (cdr x)))

(define (cddr x)
  (cdr (cdr x)))

(define (splice xs y)
  (if (null? xs)
      y
      (cons (car xs) (splice (cdr xs) y))))

(define-macro (quasiquote obj)
  (if (pair? obj)
      (let ((x (car obj))
            (y (cdr obj)))
        (if (and (eq? 'unquote x)
                 (pair? y)
                 (null? (cdr y)))
            (car y)
            (if (and (pair? x)
                     (eq? 'unquote-splicing (car x))
                     (pair? (cdr x))
                     (null? (cddr x)))
                (list 'splice (cadr x) (list 'quasiquote y))
                (list 'cons (list 'quasiquote x) (list 'quasiquote y)))))
      (list 'quote obj)))

(define (eqv? obj0 obj1)
  (eq? obj0 obj1))

(define (equal? obj0 obj1)
  ;; TODO: need to implement
  (eq? obj0 obj1))

(define-macro (cond . clauses)
  (if (null? clauses)
      #f
      (let* ((clause (car clauses))
             (expr (car clause)))
        (if (eq? expr 'else)
            `(begin ,@(cdr clause))
            `(if ,expr
                 (begin ,@(cdr clause))
                 (cond ,@(cdr clauses)))))))
  
;;
;;
;;

(define-macro (define-enum . consts)
  (let loop ((i 0)
             (consts consts)
             (bodies '()))
    (if (null? consts)
        `(begin ,@bodies)
        (loop (+ i 1)
              (cdr consts)
              (cons `(define ,(car consts) ,i) bodies)))))

(define-enum
  @fixnum
  @char
  @false
  @true
  @nil
  @eof
  @undefined
  
  @bytearray
  @box
  @bignum
  @string
  @symbol
  @pair
  @vector
  @procedure
  @record
  @object
  @wordarray
  @port
  )

;;
;; list
;;

(define (list? obj)
  (if (null? obj)
      #t
      (if (pair? obj)
          (list (cdr obj))
          #f)))

(define (acons obj1 obj2 obj3)
  (cons (cons obj1 obj2) obj3))

(define (make-list len . args)
  (if (eqv? len 0)
      '()
      (let ((v (if (null? args)
                   #f
                   (car args))))
        (cons v (apply make-list (- len 1) args)))))

(define (reverse lst)
  (let loop ((result '())
             (lst lst))
    (if (null? lst)
        result
        (loop (cons (car lst) result) (cdr lst)))))

(define (list* . args)
  (if (null? args)
      '()
      (let ((rev-lst (reverse args)))
        (let loop ((result (car rev-lst))
                   (lst (cdr rev-lst)))
          (if (null? lst)
              result
              (loop (cons (car lst) result) (cdr lst)))))))

(define (list-copy lst)
  (let loop ((result '())
             (lst lst))
    (if (null? lst)
        (reverse result)
        (loop (cons (car lst) result) (cdr lst)))))

(define (length lst)
  (if (null? lst)
      0
      (+ (length (cdr lst)) 1)))

(define (list-tail lst k . args)
  (let ((fallback (lambda ()
                    (if (null? args)
                        (error "argument out of range")
                        (car args)))))
    (if (= k 0)
        lst
        (if (< 0 k)
            (if (null? lst)
                (fallback)
                (apply list-tail (cdr lst) (- k 1) args))
            (fallback)))
    (fallback)))
      
(define (list-ref lst k . args)
  (let ((fallback (lambda ()
                    (if (null? args)
                        (error "argument out of range")
                        (car args)))))
    (if (pair? lst)
        (if (= k 0)
            (if (null? lst)
                (fallback)
                (car lst))
            (if (< 0 k)
                (if (null? lst)
                    (fallback)
                    (apply list-ref (cdr lst) (- k 1) args))
                (fallback)))
        (fallback))))

(define (last-pair lst)
  (if (pair? lst)
      (let ((next (cdr lst)))
        (if (pair? next)
            (last-pair next)
            lst))
      (error "pair required")))

(define (append2 lst0 lst1)
  (let loop ((rev-lst (reverse lst0))
             (lst lst1))
    (if (null? lst)
        (reverse rev-lst)
        (loop (cons (car lst1) rev-lst) (cdr lst1)))))

(define (append . lsts)
  (let ((len (length lsts))) 
    (if (eqv? len 0)
        '()
        (if (eqv? len 1)
            (car lsts)
            (apply append (append2 (car lsts) (cadr lsts)) (cddr lsts))))))

(define (append! . lsts)
  (apply append lsts))

(define (reverse! lst)
  (reverse lst))

(define (memq obj lst)
  (if (null? lst)
      #f
      (let ((v (car lst)))
        (if (eq? obj v)
            v
            (memq obj (cdr lst))))))

(define (memv obj lst)
  (if (null? lst)
      #f
      (let ((v (car lst)))
        (if (eqv? obj v)
            v
            (memv obj (cdr lst))))))

(define (member obj lst)
  (if (null? lst)
      #f
      (let ((v (car lst)))
        (if (equal? obj v)
            v
            (member obj (cdr lst))))))

(define (assq obj lst)
  (if (null? lst)
      #f
      (let ((pair (car lst)))
        (if (pair? pair)
            (if (eq? obj (car pair))
                pair
                (assq obj (cdr lst)))
            (error "pair required")))))

(define (assv obj lst)
  (if (null? lst)
      #f
      (let ((pair (car lst)))
        (if (pair? pair)
            (if (eqv? obj (car pair))
                pair
                (assv obj (cdr lst)))
            (error "pair required")))))

(define (assoc obj lst)
  (if (null? lst)
      #f
      (let ((pair (car lst)))
        (if (pair? pair)
            (if (equal? obj (car pair))
                pair
                (assoc obj (cdr lst)))
            (error "pair required")))))

;;
;; symbol
;;

(define intern
  (let ((symbol-table '()))
    (lambda (str)
      (let loop ((alist symbol-table))
        (cond
         ((null? loop)
            (let ((addr (alloc-object @symbol 3)))
              (object-set! addr 0 str)
              (object-set! addr 1 #f)
              (object-set! addr 2 #f)
              (set! symbol-table (cons (cons str addr) symbol-table))
              addr))
         ((string=? (caar alist) str)
          (cdar alist))
         (else
          (loop (cdr alist))))))))

(define (symbol? obj)
  (= (object-type obj) @symbol))

(define (symbol->string sym)
  (if (symbol? sym)
      (object-ref sym 0)
      (error "symbol required")))

(define gensym
  (let ((counter 0))
    (lambda ()
      (let ((addr (alloc-object @symbol 3))
            (str (call-with-output-string
                   (lambda (out)
                     (write 'G out)
                     (write counter out)))))
        (object-set! addr 0 str)
        (object-set! addr 1 #f)
        (object-set! addr 2 #f)
        (set! counter (+ counter 1))
        addr))))

;;
;; char
;;

(define (char? obj)
  (= (object-type obj) @char))

(define (char=? char1 char2)
  (if (and (char? char1) (char? char2))
      (= (char->fixnum char1) (char->fixnum char2))
      (error "char required")))

(define (char<=? char1 char2)
  (if (and (char? char1) (char? char2))
      (<= (char->fixnum char1) (char->fixnum char2))
      (error "char required")))

(define (char>? char1 char2)
  (if (and (char? char1) (char? char2))
      (> (char->fixnum char1) (char->fixnum char2))
      (error "char required")))

(define (char>=? char1 char2)
  (if (and (char? char1) (char? char2))
      (>= (char->fixnum char1) (char->fixnum char2))
      (error "char required")))

(define (char->integer char)
  (if (char? char)
      (char->fixnum char)
      (error "char required")))

(define (integer->char n)
  (if (fixnum? n)
      (fixnum->char n)
      (error "fixnum required")))

;;
;; string
;;

(define (string? obj)
  (= (object-type obj) @string))

(define (make-string k . opts)
  (let ((c (if (null? opts)
               #\space
               (car opts))))
    
           
;;
;; port
;;

(define (make-port direction)

;; end of file
