;; -*- coding: utf-8; mode: scheme -*-
;;
;; macros.scm - uSpica assember basic macros
;;
;;   Copyright (c) 2011 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(define-module msasm.macros
  (use gauche.collection)
  (use srfi-1)
  (use util.list)
  (use util.match)

  (use msasm)
  (use msasm.context)
  )

(select-module msasm.macros)

(defmacro mov (rd rt)
  (cond
   ((and (number? rt) (< #xffff rt))
      `(begin
         (mov ,rd ,(ash rt -16))
         (sll ,rd ,rd 16)
         ,(let ((low (logand #xffff rt)))
            (if (= low 0)
                ()
                `(add ,rd ,rd ,low)))))
   (else
    `(add ,rd $zero ,rt))))

(defmacro j (rt)
  `(blezl $zero $zero ,rt))

(defmacro jal (rd rt)
  `(blezl ,rd $zero ,rt))

(defmacro blez (rs t)
  `(blezl $zero ,rs ,t))

(defmacro bltz (rs t)
  `(bltzl $zero ,rs ,t))

(defmacro bgez (rs t)
  `(bgezl $zero ,rs ,t))

(defmacro bgtz (rs t)
  `(bgtzl $zero ,rs ,t))

(defmacro halt ()
  `(begin
     :halt
     (j :halt)
     (nop)))

(defmacro mlw (rd rs . opts)
  (let-optionals* opts ((rt '$zero)
                        (parallel-body '())
                        (delay-body '(nop)))
    `(begin
       (mlwr $zero ,rs ,rt)
       ,parallel-body
       :loop
       (lmdr ,rd $zero :loop)
       ,delay-body)))

(defmacro mlb (rd rs . opts)
  (let-optionals* opts ((rt #f))
    (if rt
        `(begin
           (mlwr $zero ,rs ,rt)
           (add $at ,rs ,rt)
           (and $at $at #b11)
           (sll $at $at 8)
           :loop
           (lmdr ,rd $zero :loop)
           (srl ,rd ,rd $at)
           (and ,rd ,rd #xff))
        `(begin
           (mlwr $zero ,rs $zero)
           (and $at ,rs #b11)
           (sll $at $at 8)
           :loop
           (lmdr ,rd $zero :loop)
           (srl ,rd ,rd $at)
           (and ,rd ,rd #xff)))))
  
     
(defmacro msw (rd rs . opts)
  (let-optionals* opts ((rt '$zero)
                        (parallel-body '()))
    `(begin
       (mswr ,rd ,rs ,rt)
       ,parallel-body
       :loop
       (lmdr $zero $zero :loop)
       (nop))))

(defmacro mread (rt)
  `(mlwr $zero $zero ,rt))

(defmacro mwrite (rd rt)
  `(mswr ,rd $zero ,rt))

(defmacro msync ()
  `(mfetch $zero))

(defmacro mfetch (rd)
  `(begin
     :loop
     (lmdr ,rd $zero :loop)
     (nop)))

(defmacro nop ()
  `(or $zero $zero $zero))

(defmacro debug-led (v)
  `(begin
     (mov $at ,v)
     (sw $at $zero #xff80)))

(defmacro push reg-list
  (if (null? reg-list)
      '()
      `(begin
         (sub $sp $sp ,(length reg-list))
         ,@(values-ref (fold2 (lambda (r body i)
                                (values (cons `(sw ,r $sp ,i) body)
                                        (+ i 1)))
                              '()
                              1
                              reg-list)
                       0))))

(defmacro pop reg-list
  (if (null? reg-list)
      '()
      `(begin
         ,@(values-ref (fold2 (lambda (r body i)
                                (values (cons `(lw ,r $sp ,i) body)
                                        (+ i 1)))
                              '()
                              1
                              reg-list)
                       0)
         (add $sp $sp ,(length reg-list)))))

(defmacro save-reg (reg-list . bodies)
  `(begin
     (push ,@reg-list)
     ,@bodies
     (pop ,@reg-list)))


;;

;; funcall is deprecated. use call instead.
;; NOTE: call doesn't preserve $a0-$a4
(defmacro funcall (func . args)
  (when (< 4 (length args))
    (errorf "Too many args: ~s @ ~s" args func))
  (let* ((data-list '())
         (arg-list (map (lambda (i arg)
                          (let ((arg-reg (reg-a i)))
                            (if (string? arg)
                                (let ((label (make-keyword (format "%%string~d" i))))
                                  (set! data-list (append data-list
                                                          `(,label
                                                            (@string ,arg))))
                                  `(mov ,arg-reg ,label))
                                `(mov ,arg-reg ,arg))))
                        (iota 4)
                        args)))
    `(begin
       (save-reg ($ra $fp ,@(map reg-a (iota (length args))))
         ,@arg-list
         (j ,func)
         (mov $ra :%%funcall-end)
         ,@data-list
         :%%funcall-end))))

;; NOTE: be careful if args contains $a0-$a3.
(defmacro call (func . args)
  (when (< 4 (length args))
    (errorf "Too many args: ~s @ ~s" args func))
  (let* ((data-list '())
         (save-regs (or (and-let* ((argc (context-info-data context 'func-argc)))
                          (append '($ra $fp) (map reg-a (iota argc))))
                        ()))
         (arg-list (map (lambda (i arg)
                          (let ((arg-reg (reg-a i)))
                            (if (string? arg)
                                (let ((label (make-keyword (format "%%string~d" i))))
                                  (set! data-list (append data-list
                                                          `(,label
                                                            (@string ,arg))))
                                  `(mov ,arg-reg ,label))
                                (if (symbol? arg)
                                    (let ((rt (reg->code context arg-reg))
                                          (rs (reg->code context arg)))
                                      (cond
                                        ((and (not (= rt rs))
                                              (memq rs '(4 5 6 7))
                                              (< rs rt))
                                         (errorf "can't use ~a as arg, it is destroyed." arg))
                                        ((= rt rs)
                                         '())
                                        (else
                                         `(mov ,arg-reg ,arg))))
                                    `(mov ,arg-reg ,arg)))))
                        (iota 4)
                        args)))
    `(begin
       (save-reg ,save-regs
         ,@arg-list
         (j ,func)
         (mov $ra :%%funcall-end)
         ,@data-list
         :%%funcall-end))))

;; unsafe version (doesn't preserve any registers and you cannot use string literals)
(defmacro call* (func . args)
  (when (< 4 (length args))
    (errorf "Too many args: ~s @ ~s" args func))
  (let ((inits (map (lambda (arg i)
                         `(mov ,(reg-a i) ,arg))
                       args
                       (iota 4))))
    `(begin
       ,@(if (null? inits)
             inits
             (cdr inits))
       (jal $ra ,func)
       ,(if (null? inits)
            '(nop)
            (car inits)))))

(defmacro tail-call* (func . args)
  (when (< 4 (length args))
    (errorf "Too many args: ~s @ ~s" args func))
  (let ((inits (cons '(mov $sp $fp)
                     (map (lambda (arg i)
                            `(mov ,(reg-a i) ,arg))
                          args
                          (iota 4)))))
    `(begin
       ,@(cdr inits)
       (j ,func)
       ,(car inits))))

(defmacro with-reg-alias (forms . body)
  `(begin
     ,(lambda (ctxt)
        (for-each (match-lambda
                    ((new-reg orig-reg)
                     (add-reg-alias! ctxt new-reg orig-reg)))
                  forms)
        '())
     ,@body))
     
(defmacro let-temp (regs . body)
  `(begin
     ,(lambda (ctxt)
        (let ((temp-regs (allocate-temp-regs! ctxt (length regs))))
          `(with-reg-alias ,(zip regs temp-regs)
             ,@body)))))

(defmacro let-stemp (reg-list . body)
  (when (< 8 (length reg-list))
    (error "reg-list must be lesser than or equal to 8 items, but got ~d items."
           (length reg-list)))
  `(save-reg ,(map reg-s (iota (length reg-list)))
     (with-reg-alias ,(alias-form reg-s reg-list)
       ,(lambda (ctxt)
          (context-info-push! ctxt 'use-stemp? #t)
          '())
       ,@body)))
  
(defmacro inc (reg . args)
  (let-optionals* args ((delta 1))
    `(add ,reg ,reg ,delta)))

(defmacro dec (reg . args)
  (let-optionals* args ((delta 1))
    `(sub ,reg ,reg ,delta)))

(defmacro inc! (reg . args)
  `(inc ,reg ,@args))

(defmacro dec! (reg . args)
  `(dec ,reg ,@args))

(define (expand-branch-if test label)
  (define (reg? v)
    (symbol? v))
  (define (imm? v)
    (not (reg? v)))
  (define (not-pred pred)
    (assoc-ref '((imm? . obj?) (obj? . imm?) (fixnum? . not-fixnum?) (chr? . not-chr?)
                 (uvector? . not-uvector?) (not-uvector? . uvector?)
                 (!= . =) (= . !=) (>= . <) (> . <=) (<= . >) (< . >=))
               pred))
  (match test
    (('not ('and tests ...))
     (expand-branch-if `(or ,@(map (lambda (test)
                                     `(not ,test))
                                   tests))
                       label))
    (('not ('or tests ...))
     (expand-branch-if `(and ,@(map (lambda (test)
                                      `(not ,test))
                                    tests))
                       label))
    (('not ('not test))
     (expand-branch-if test label))
    (('not (pred arg0 arg1))
     (expand-branch-if `(,(not-pred pred) ,arg0 ,arg1) label))
    (('not (pred arg0))
     (expand-branch-if `(,(not-pred pred) ,arg0) label))
    (('not (? reg? r))
     (expand-branch-if `(= ,r 0) label))
    (('and test)
     (expand-branch-if test label))
    (('and test rest ...)
     `(begin
        ,(expand-branch-if `(not ,test) :%%false)
        (nop)
        ,(expand-branch-if `(and ,@rest) label)
        :%%false))
    (('and)
     `(j ,test))
    (('or test)
     (expand-branch-if test label))
    (('or test rest ...)
     `(begin
        ,(expand-branch-if test label)
        (nop)
        ,(expand-branch-if `(or ,@rest) label)))
    (('or)
     `(j ,test))    
    ((pred 0 arg1)
     (expand-branch-if `(,pred $zero ,arg1) label))
    ((pred arg0 0)
     (expand-branch-if `(,pred ,arg0 $zero) label))
    ((pred arg0 (? imm? arg1))
     `(begin
        (mov $at ,arg1)
        ,(expand-branch-if `(,pred ,arg0 $at) label)))
    ((pred (? imm? arg0) arg1)
     `(begin
        (mov $at ,arg0)
        ,(expand-branch-if `(,pred $at ,arg1) label)))
    ((pred '$at '$at)
     (error "$at confilict"))
    (('= arg0 arg1)
     `(beq ,arg0 ,arg1 ,label))
    (('!= arg0 arg1)
     `(bne ,arg0 ,arg1 ,label))
    (((and (or '< '<= '> '>=) pred) arg0 arg1)
     `(begin
        (sub $at ,arg0 ,arg1)
        (,(assoc-ref '((= . beq) (!= . bne) (< . bltz) (<= . blez) (> . bgtz) (>= . bgez)) pred) $at ,label)))
    (('imm? arg0)
     `(begin
        (mov $at #x8000)
        (btt $at ,arg0 ,label)))
    (('obj? arg0)
     `(begin
        (mov $at #x8080)
        (btt $at ,arg0 ,label)))
    (('fixnum? arg0)
     `(begin
        (mov $at #xc000)
        (btt $at ,arg0 ,label)))
    (('not-fixnum? arg0)
     `(begin
        (mov $at #xc000)
        (bnt $at ,arg0 ,label)))
    (('chr? arg0)
     `(begin
        (mov $at #xc040)
        (btt $at ,arg0 ,label)))
    (('not-chr? arg0)
     `(begin
        (mov $at #xc040)
        (bnt $at ,arg0 ,label)))
    (('uvector? arg0)
     `(begin
        (mov $at #xff80)
        (btt $at ,arg0 ,label)))
    (('not-uvector? arg0)
     `(begin
        (mov $at #xff80)
        (bnt $at ,arg0 ,label)))
    ((? reg? r)
     `(begin
        (mov $at #xff50)
        (bnt $at ,r ,label)))))

(defmacro while (test . body)
  `(begin
     :%%loop
     ,(expand-branch-if `(not ,test) :%%exit)
     (nop)
     (begin ,@body)
     (j :%%loop)
     (nop)
     :%%exit))

(defmacro repeat (test . body)
  `(begin
     :%%loop
     (begin ,@body)
     ,(expand-branch-if test :%%loop)
     (nop)))

(defmacro loop body
  `(begin
     :%%loop
     ,@body
     (j :%%loop)
     (nop)))

(defmacro branch-if (test label)
  (expand-branch-if test label))

(defmacro branch-unless (test label)
  (expand-branch-if `(not ,test) label))

(defmacro defun (label args stemps . body)
  (when (< 4 (length args))
    (errorf "Too many args: ~s @ ~s" args label))
  `(begin*
    ,label
    (begin
      ,(lambda (ctxt)
         (context-info-push! ctxt 'func-argc (length args))
         ())
      (mov $fp $sp)
      (with-reg-alias ,(alias-form reg-a args)
        (let-stemp ,stemps
                   ,@body
                   :%%end-function))
      (ret))))

(defmacro ret ()
  (if (context-info-data context 'use-stemp?)
      `(begin
         (j :%%end-function)
         (nop))
      `(begin
         (j $ra)
         (mov $sp $fp))))

(defmacro ret-if (test)
  (if (context-info-data context 'use-stemp?)
      `(begin
         (branch-if ,test :%%end-function)
         (nop))
      `(when ,test
         (j $ra)
         (mov $sp $fp))))

(defmacro ret-unless (test)
  (if (context-info-data context 'use-stemp?)
      `(begin
         (branch-unless ,test :%%end-function)
         (nop))
      `(unless ,test
         (j $ra)
         (mov $sp $fp))))

(defmacro byte-ref (rt rd s)
  `(begin
     (srl ,rt ,rd ,s)
     (and ,rt ,rt #xff)))

(defmacro if (test then-body . opts)
  (let-optionals* opts ((else-body '(begin)))
    `(begin
       (branch-unless ,test :%%else-clause)
       (nop)
       ,then-body
       (j :%%exit)
       (nop)
       :%%else-clause
       ,else-body
       :%%exit)))

(defmacro when (test . body)
  `(if ,test (begin ,@body)))

(defmacro unless (test . body)
  `(if (not ,test) (begin ,@body)))

(defmacro cond clauses
  (if (null? clauses)
      '(begin)
      (match (car clauses)
        (('else body ...)
         `(begin ,@body))
        ((test body ...)
         `(if ,test (begin ,@body) (cond ,@(cdr clauses)))))))

(defmacro timeout-loop (args . body)
  (let-optionals* args ((us #f)
                        (treg '$at)
                        (num-of-insts (+ 5 (length body))))
    (let ((clock-mhz 50))
      `(begin
         (mov ,treg ,(/ (* us clock-mhz) num-of-insts))
         (while (!= ,treg 0)
           ,@body
           (dec! ,treg))))))

(defmacro usleep (us)
  `(timeout-loop (,us)))

(defmacro check-point body
  (add-check-point! context pc body)
  '())

(defmacro time body
  `(begin
     (check-point
      (push-tick!))
     ,@body
     (check-point
      (format #t "~s: ~d steps\n" ',body (- (tick) (pop-tick!))))))

;; end of file
