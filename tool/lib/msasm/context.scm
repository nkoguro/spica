;; -*- coding: utf-8; mode: scheme -*-
;;
;; context.scm - context for micro spica assembler
;;
;;   Copyright (c) 2011 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(define-module msasm.context
  (use srfi-1)
  (use srfi-11)
  (use util.list)
  (use util.match)

  (export reg-a
          reg-s
          reg-t
          alias-form
          temp-regs
          
          <context>
          alist->context
          make-context
          context-set-end!
          context-info-push!
          context-info-list
          context-info-data
          context-parent
          context-child
          context-children
          context-add-label!
          context-resolve-label
          allocate-temp-regs!
          add-reg-alias!
          reg-lookup
          reg->code
          add-check-point!
          check-point-list
          add-test-case!
          test-case-list)
  )

(select-module msasm.context)


;;

(define (reg-a i)
  (string->symbol (format "$a~d" i)))

(define (reg-s i)
  (string->symbol (format "$s~d" i)))

(define (reg-t i)
  (string->symbol (format "$t~d" i)))

(define (alias-form proc regs)
  (map (lambda (reg i)
         `(,reg ,(proc i)))
       regs
       (iota (length regs))))

(define (temp-regs)
  (map reg-t (iota 10)))

;;

(define-class <context> ()
  ((parent :init-value #f
           :init-keyword :parent)
   (children :init-value '())
   (start-pc :init-value #f
             :init-keyword :start-pc)
   (end-pc :init-value #f)
   (free-temp-regs :init-keyword :free-temp-regs)
   (reg-alist :init-value '())
   (label-alist :init-value '())
   (info-table :init-value '())
   (check-points :init-value '()
                 :allocation :class)
   (test-cases :init-value '()
               :allocation :class)))

(define-method write-object ((ctxt <context>) port)
  (write/ss (fold (lambda (slot alist)
                    (acons slot (~ ctxt slot) alist))
                  '()
                  (map slot-definition-name
                       (filter (^d (let ((alloc (slot-definition-allocation d)))
                                     (or (equal? alloc :instance)
                                         (and (not (~ ctxt'parent))
                                              (equal? alloc :class)))))
                               (class-slots <context>))))
            port))

(define (alist->context alist)
  (define (%alist->context alist dict)
    (if (hash-table-exists? dict alist)
        (hash-table-get dict alist)
        (let ((ctxt (make <context>)))
          (hash-table-put! dict alist ctxt)
          (for-each (match-lambda
                      (('parent . parent)
                       (set! (~ ctxt'parent) (and parent (%alist->context parent dict))))
                      (('children . children)
                       (set! (~ ctxt'children) (map (cut %alist->context <> dict) children)))
                      ((slot-name . val)
                       (set! (~ ctxt slot-name) val)))
                    alist)
          ctxt)))
  (%alist->context alist (make-hash-table 'eq?)))

(define (make-context pc parent)
  (let ((child (make <context>
                 :parent parent
                 :free-temp-regs (if parent
                                     (~ parent'free-temp-regs)
                                     (temp-regs))
                 :start-pc pc)))
    (if parent
        (push! (~ parent'children) child)
        (for-each (match-lambda
                    ((new-reg orig-reg)
                     (add-reg-alias! child new-reg orig-reg)))
                  '(($zero $r0) ($at $r1)
                    ($v0 $r2)   ($v1 $r3)
                    ($a0 $r4)   ($a1 $r5)  ($a2 $r6)  ($a3 $r7)
                    ($t0 $r8)   ($t1 $r9)  ($t2 $r10) ($t3 $r11) ($t4 $r12)
                    ($t5 $r13)  ($t6 $r14) ($t7 $r15) ($t8 $r24) ($t9 $r25)
                    ($s0 $r16)  ($s1 $r17) ($s2 $r18) ($s3 $r19) ($s4 $r20)
                    ($s5 $r21)  ($s6 $r22) ($s7 $r23) ($s8 $r30)
                    ($k0 $r26)  ($k1 $r27)
                    ($gp $r28)  ($sp $r29) ($fp $r30) ($ra $r31))))
    child))

(define (context-set-end! ctxt pc)
  (set! (~ ctxt'end-pc) pc))

(define (context-info-push! ctxt key . data)
  (set! (~ ctxt'info-table)
        (assoc-set! (~ ctxt'info-table)
                    key
                    (fold cons (assoc-ref (~ ctxt'info-table) key '()) data))))

(define (context-info-list ctxt key)
  (let loop ((ctxt ctxt)
             (result '()))
    (if ctxt
        (loop (context-parent ctxt)
              (append (assoc-ref (~ ctxt'info-table) key '())
                      result))
        result)))

(define (context-info-data ctxt key)
  (if ctxt
      (let ((lst (assoc-ref (~ ctxt'info-table) key '())))
        (if (null? lst)
            (context-info-data (context-parent ctxt) key)
            (car lst)))
      #f))

(define (context-parent ctxt)
  (if ctxt
      (~ ctxt'parent)
      #f))

(define (context-child ctxt pc)
  (or (and-let* ((child (find (lambda (ctxt)
                                (< (~ ctxt'start-pc) pc (+ (~ ctxt'end-pc) 1)))
                              (~ ctxt'children))))
        (context-child child pc))
      ctxt))

(define (context-children ctxt)
  (~ ctxt'children))

(define (context-label-alist ctxt)
  (~ ctxt'label-alist))

(define (context-add-label! ctxt label addr in-code?)
  (push! (~ ctxt'label-alist) (cons label addr)))

(define (context-resolve-label ctxt label)
  (let loop ((ctxt ctxt))
    (or (and-let* (ctxt
                   (p (assoc label (context-label-alist ctxt))))
          (cdr p))
        (and-let* ((parent (context-parent ctxt)))
          (loop parent))
        (errorf "label '~a' not found" label))))

(define (allocate-temp-regs! ctxt n)
  (let ((free-temp-regs (~ ctxt'free-temp-regs)))
    (when (< (length free-temp-regs) n)
      (errorf "no room to allocate temp regs (~d required, but ~d free)"
        n
        (length free-temp-regs)))
    (let-values (((allocs frees) (split-at free-temp-regs n)))
      (set! (~ ctxt'free-temp-regs) frees)
      allocs)))

(define (add-reg-alias! ctxt new-name orig-name)
  (push! (~ ctxt'reg-alist) (cons new-name orig-name)))

(define (reg-lookup ctxt reg)
  (if ctxt
      (reg-lookup (context-parent ctxt)
                  (cdr (or (assoc reg (~ ctxt'reg-alist))
                           (cons #f reg))))
      reg))

(define (reg->code ctxt reg)
  (or (and-let* ((code (assq-ref
                        '(($r0  . 0)  ($r1 . 1)   ($r2 . 2)   ($r3 . 3)
                          ($r4  . 4)  ($r5 . 5)   ($r6 . 6)   ($r7 . 7)
                          ($r8  . 8)  ($r9 . 9)   ($r10 . 10) ($r11 . 11)
                          ($r12 . 12) ($r13 . 13) ($r14 . 14) ($r15 . 15)
                          ($r16 . 16) ($r17 . 17) ($r18 . 18) ($r19 . 19)
                          ($r20 . 20) ($r21 . 21) ($r22 . 22) ($r23 . 23)
                          ($r24 . 24) ($r25 . 25) ($r26 . 26) ($r27 . 27)
                          ($r28 . 28) ($r29 . 29) ($r30 . 30) ($r31 . 31))
                        (reg-lookup ctxt reg))))
        code)
      (errorf "register ~a is not defined." reg)))

(define (add-check-point! ctxt pc expr-list)
  (push! (~ ctxt'check-points) (cons pc expr-list)))

(define (check-point-list ctxt)
  (reverse (~ ctxt'check-points)))

(define (add-test-case! ctxt desc test-cases)
  (push! (~ ctxt'test-cases) (cons desc test-cases)))

(define (test-case-list ctxt)
  (reverse (~ ctxt'test-cases)))

;; end of file
