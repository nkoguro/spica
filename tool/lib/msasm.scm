;; -*- coding: utf-8; mode: scheme -*-
;;
;; mmasm.scm - micro spica assembler
;;
;;   Copyright (c) 2010 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;

(define-module msasm
  (use srfi-1)
  (use util.match)
  (use util.list)
  (use gauche.parameter)
  (use srfi-11)
  (use gauche.collection)
  (use gauche.parseopt)
  (use file.util)

  (use msasm.context)
  
  (export defmacro macro asm->verilog asm->hex main-code lib-code))

(select-module msasm)

(define *op-table* '((add   . #b00001000000000000000000000000000)
                     (adh   . #b00010000000000000000000000000000)
                     (sub   . #b00011000000000000000000000000000)
                     (and   . #b00100000000000000000000000000000)
                     (or    . #b00101000000000000000000000000000)
                     (xor   . #b00110000000000000000000000000000)
                     (nor   . #b00111000000000000000000000000000)
                     (sll   . #b01100000000000000000000000000000)
                     (srl   . #b01101000000000000000000000000000)
                     (sla   . #b01110000000000000000000000000000)
                     (sra   . #b01111000000000000000000000000000)
                     (mlwr  . #b10000000000000000000000000000000)
                     (lw    . #b10001000000000000000000000000000)
                     (lmdr  . #b10010000000000000000000000000000)
                     (jalr  . #b10010000000000000000000000000000)
                     (bgezl . #b10011000000000000000000000000000) 
                     (bgtzl . #b10100000000000000000000000000000)
                     (blezl . #b10101000000000000000000000000000)
                     (bltzl . #b10110000000000000000000000000000)
                     (inst  . #b10111000000000000000000000000000)
                     (mswr  . #b11000000000000000000000000000000)
                     (sw    . #b11001000000000000000000000000000)
                     (btt   . #b11100000000000000000000000000000)
                     (bnt   . #b11101000000000000000000000000000)
                     (beq   . #b11110000000000000000000000000000)
                     (bne   . #b11111000000000000000000000000000)

                     (mstatus  . #b00000000000000000000000000101001)
                     ))

(define (op->code op)
  (assq-ref *op-table* op))

(define (op? sym)
  (assq sym *op-table*))

(define *asm-macro-table* '())

(define (asm-macro-set! name proc)
  (set! *asm-macro-table* (assq-set! *asm-macro-table* name proc)))

(define-macro (defmacro name args . body)
  `((with-module msasm asm-macro-set!) ',name (lambda ,(list* 'context 'pc args)
                                                ,@body)))

;; deprecated 
(define-syntax macro
  (syntax-rules ()
    ((_ (name args ...) bodies ...)
     (asm-macro-set! 'name (lambda (context pc args ...) bodies ...)))
    ((_ (name . args) bodies ...)
     (asm-macro-set! 'name (lambda (context pc . args) bodies ...)))))

(define (asm-macro-expand ctxt pc expr)
  (apply (assq-ref *asm-macro-table* (car expr)) ctxt pc (cdr expr)))

;;
(define label-reference-list (make-parameter '()))

;;

(define (code word comment)
  (list word comment))

(define (code-word code)
  (first code))

(define (code-comment code)
  (second code))

(define (code-set-word! code word)
  (set-car! code word))

(define (register-label-reference ctxt code label)
  (label-reference-list (cons (list ctxt code label)
                        (label-reference-list)))
  code)

(define (pc code-list)
  (length code-list))

(define (pass1/context ctxt code-list expr-list)
  (let-values (((child-ctxt code-list) (fold2 pass1/asm
                                              (make-context (pc code-list) ctxt)
                                              code-list
                                              expr-list)))
    (context-set-end! child-ctxt (pc code-list))
    (values ctxt code-list)))

(define (pass1 expr-list)
  (let ((ctxt (make-context 0 #f)))
    (context-set-end! ctxt #xffffffff)
    (fold2 pass1/asm ctxt '() expr-list)))

(define (pass1/asm expr ctxt code-list . opts)
  (guard (e (else
             (format (current-error-port) "Error: ~s~%" expr)
             (raise e)))
    (let-optionals* opts ((comment (format "~s" expr)))
      (match expr
        (()
         (values ctxt code-list))
        (('quote ())
         (values ctxt code-list))
        ((? procedure? proc)
         (fold2 pass1/asm ctxt code-list (list (proc ctxt))))
        ((? keyword? label)
         (context-add-label! ctxt label (pc code-list) #t)
         (values ctxt code-list))
        (((? keyword? func) args ...)
         (fold2 pass1/asm ctxt code-list `((call ,func ,@args))))
        (('begin body ...)
         (pass1/context ctxt code-list body))
        (('begin* bodies ...)
         (fold2 pass1/asm ctxt code-list bodies))
        (('@data data ...)
         (let ((data-code (lambda (v comment)
                            (if (keyword? v)
                                (register-label-reference ctxt (code 0 comment) v)
                                (code v comment)))))
           (values ctxt
                   (append code-list
                           (cons (data-code (car data) comment)
                                 (map (cut data-code <> #f) (cdr data)))))))
        (('@string str)
         (let ((word-list (reverse
                           (values-ref
                            (fold2 (lambda (c lst s)
                                     (let ((v (+ (car lst) (ash c s))))
                                       (if (= s 24)
                                           (values (cons 0 (cons v (cdr lst))) 0)
                                           (values (cons v (cdr lst)) (+ s 8)))))
                                   '(0)
                                   0
                                   (map char->ucs (append (string->list str)
                                                          '(#\null))))
                            0))))
           (values ctxt
                   (append code-list
                           (cons (code (car word-list) comment)
                                 (map (cut code <> #f) (cdr word-list)))))))
        (('@equ label v)
         (context-add-label! ctxt label v #t)
         (values ctxt code-list))
        (('@org addr)
         (let ((gap-size (- addr (length code-list))))
           (when (< gap-size 0)
             (errorf "can't set origin address to ~x" addr))
           (values ctxt (append code-list (make-list gap-size (code 0 ""))))))
        (((? op? op) rd rs (? symbol? rt))
         (values ctxt
                 (append code-list (list (code (logior (op->code op)
                                                 (ash (reg->code ctxt rd) 22)
                                                 (ash (reg->code ctxt rs) 17)
                                                 (ash (reg->code ctxt rt) 11))
                                               comment)))))
        (((? op? op) rd rs (? number? imm))
         (values ctxt
                 (append code-list (list (code (logior (op->code op)
                                                 (ash (reg->code ctxt rd) 22)
                                                 (ash (reg->code ctxt rs) 17)
                                                 (ash 1 16)
                                                 (logand #xffff imm))
                                               comment)))))
        (((? op? op) rd rs (? keyword? label))
         (values ctxt
                 (append code-list
                         (list (register-label-reference
                                ctxt
                                (code (logior (op->code op)
                                        (ash (reg->code ctxt rd) 22)
                                        (ash (reg->code ctxt rs) 17)
                                        (ash 1 16))
                                      comment)
                                label)))))
        (((? op? op) rd ('+ rs (? keyword? label)))
         (values ctxt
                 (append code-list
                         (map (cut register-label-reference ctxt <> label)
                              (pass1/asm `(,op ,rd ,rs 0) ctxt '() comment)))))
        (((? op? op) rd ('+ rs (? number? imm)))
         (pass1/asm `(,op ,rd ,rs imm) ctxt code-list comment))
        (((? op? op) rd ('- rs (? number? imm)))
         (pass1/asm `(,op ,rd ,rs (- imm)) ctxt code-list comment))
        (((? op? op) rd (? keyword? label))
         (append code-list
                 (map (cut register-label-reference ctxt <> label)
                      (pass1/asm `(,op ,rd $zero 0) ctxt '() comment))))
        (((? op? op) rd (? number? imm))
         (pass1/asm `(,op ,rd $zero ,imm) ctxt code-list comment))
        (((? op? op) rd (? symbol? rs))
         (pass1/asm `(,op ,rd ,rs 0) ctxt code-list comment))
        (else
         (pass1/asm (asm-macro-expand ctxt (pc code-list) expr)
                    ctxt
                    code-list))))))

(define (pass2! code-list)
  (pass2/resolve-label!)
  (list->vector (map code-word code-list)))

(define (pass2/resolve-label!)
  (for-each (match-lambda
              ((ctxt code label)
               (code-set-word! code
                               (logior (code-word code)
                                 (logand #xffff
                                   (context-resolve-label ctxt label))))))
            (label-reference-list)))

(define (write-hex expr-list)
  (let ((codes (pass2! (values-ref (pass1 expr-list) 1))))
    (let* ((size (vector-length codes))
           (addr-bits 12)
           (mem-size (expt 2 addr-bits)))
      (print "@0")
      (dotimes (i size)
        (format #t "~8,'0X~%" (vector-ref codes i)))
      (dotimes (i (- mem-size size))
        (print "00000000"))
      (format (current-error-port) "code size = ~d words (~d% used)~%" size (x->integer (* 100 (/ size mem-size)))))))

(define (write-disasm-list expr-list)
  (let-values (((_ codes) (pass1 expr-list)))
    (pass2/resolve-label!)
    (let* ((size (length codes))
           (addr-bits 12)
           (mem-size (expt 2 addr-bits)))
      (dotimes (i size)
        (let ((code (list-ref codes i)))
          (format #t "~4,'0X: ~8,'0X ; ~a~%" i (code-word code)
                  (or (code-comment code) ""))))
      (format (current-error-port) "code size = ~d words (~d% used)~%" size (x->integer (* 100 (/ size mem-size)))))))

(define (write-bin expr-list debug-filename)
  (let-values (((ctxt code-list) (pass1 expr-list)))
    (let ((codes (pass2! code-list)))
      (let* ((size (vector-length codes))
             (addr-bits 12)
             (mem-size (expt 2 addr-bits)))
        (dotimes (i size)
          (let ((code (vector-ref codes i)))
            (write-byte (logand (ash code 0) #xff))
            (write-byte (logand (ash code -8) #xff))          
            (write-byte (logand (ash code -16) #xff))
            (write-byte (logand (ash code -24) #xff))))
        (format (current-error-port) "code size = ~d words (~d% used)~%" size (x->integer (* 100 (/ size mem-size))))
        (when debug-filename
          (call-with-output-file debug-filename
            (lambda (out)
              (write ctxt out))))))))

(define *main-code* '())
(define *lib-code* '())

(define (set-main-code! code-list)
  (if (null? *main-code*)
      (set! *main-code* code-list)
      (error "The main code already exists.")))

(define (append-lib-code! code-list)
  (set! *lib-code* (append *lib-code* code-list)))
 
(define-syntax main-code
  (syntax-rules ()
    ((_ body ...)
     (set-main-code! '(body ...)))))

(define-syntax lib-code
  (syntax-rules ()
    ((_ body ...)
     (append-lib-code! '(body ...)))))

(define (msasm-main args)
  (let-args (cdr args) ((disasm-list? "l|list" #f)
                        (debug "d|debug=s" #f)
                        (hex? "H|hex" #f)
                        (outfile "o|output=s" #f)
                        . files)
    (for-each (^f (let ((sandbox (make-module #f)))
                    (eval '(begin
                             (extend user)
                             (use msasm)
                             (use msasm.macros)) sandbox)
                    (load f :environment sandbox)))
              files)
    (let ((code-list (with-module msasm
                       (append *main-code* *lib-code*))))
      (define (dispatch)
        (cond
          (disasm-list?
           (write-disasm-list code-list))
          (hex?
           (write-hex code-list))
          (else
           (write-bin code-list
                      (or debug
                          (and outfile
                               (path-swap-extension outfile "dbg")))))))
      (if outfile
          (begin
            (let-values (((out tmpfile) (sys-mkstemp "tmp")))
              (with-output-to-port out dispatch)
              (move-file tmpfile outfile :if-exists :supersede)))
          (dispatch))))
      0)

(with-module user
  (define main (with-module msasm msasm-main)))

;; deprecated
(define-syntax asm->hex
  (syntax-rules ()
    ((_ bodies ...)
     (define (main args)
       (let-args (cdr args) ((disasm-list? "l|list" #f)
                             (debug "d|debug=s" #f)
                             (hex? "H|hex" #f)
                             (outfile "o|output=s" #f))
         (define (dispatch)
           (cond
             (disasm-list?
              (write-disasm-list '(bodies ...)))
             (hex?
              (write-hex '(bodies ...)))
             (else
              (write-bin '(bodies ...) debug))))
         (if outfile
             (with-output-to-file outfile dispatch)
             (dispatch))
         0)))))

;; end of file
