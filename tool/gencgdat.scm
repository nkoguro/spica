;; -*- coding: utf-8; mode: scheme -*-
;;
;; gencgdat.scm
;;
;;   Copyright (c) 2007 KOGURO, Naoki (naoki@koguro.net)
;;   All rights reserved.
;;
;;   Redistribution and use in source and binary forms, with or without 
;;   modification, are permitted provided that the following conditions 
;;   are met:
;;
;;   1. Redistributions of source code must retain the above copyright 
;;      notice, this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright 
;;      notice, this list of conditions and the following disclaimer in the 
;;      documentation and/or other materials provided with the distribution.
;;   3. Neither the name of the authors nor the names of its contributors 
;;      may be used to endorse or promote products derived from this 
;;      software without specific prior written permission.
;;
;;   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
;;   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
;;   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
;;   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
;;   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
;;   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
;;   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
;;   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
;;   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
;;   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
;;   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;
;;   $Id: $
;;

(use srfi-13)

(define *character-data* (make-vector 256 (make-list 16 "00")))

(define (%read-line)
  (read-line (current-input-port) #t))

(define (start)
  (rxmatch-case (%read-line)
    (test eof-object? #f)
    (#/^ENCODING\s+(.+)/ (#f code)
     (encoding (string->number code 10)))
    (else
     (start))))
    
(define (encoding code)
  (rxmatch-case (%read-line)
    (test eof-object? #f)
    (#/^BITMAP$/ (#f)
     (bitmap code))
    (else
     (encoding code))))

(define (bitmap code)
  (when (< code (vector-length *character-data*))
    (vector-set! *character-data* code
                 (let loop ((line (%read-line))
                            (result '()))
                   (if (#/^ENDCHAR$/ line)
                       (reverse result)
                       (loop (%read-line) (cons line result))))))
  (start))
                                                                  
  
(define (generate-code filename)
  (with-input-from-file filename
    (lambda ()
      (start))))

(define (main args)
  (generate-code (list-ref args 1))
  (dotimes (i (vector-length *character-data*))
    (when (= (length (vector-ref *character-data* i)) 14) (print "00"))
    (for-each print (vector-ref *character-data* i))
    (when (= (length (vector-ref *character-data* i)) 14) (print "00"))
    )
  0)

;; end of file
